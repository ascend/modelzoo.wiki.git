# 写在前面

本经验分享由天津大学自然语言处理实验室TJUNLP Lab ([实验室主页](https://tjunlp-lab.github.io/)) 提供，欢迎各开发者一同学习交流。

# 一. 资源类算子问题

## 1.1 问题描述：

数据处理的graph中tf.contrib.lookup.index_table_from_tensor的衔接lookup算子无法打通（[算子位置](https://github.com/THUNLP-MT/THUMT/blob/3829c602f4b2b989aba0f12875532147a29e56d4/thumt/data/dataset.py#L149)）。

## 1.2 排查思路：

在不了解NPU上算子源码的情况下，根据tensorflow对lookup的过程（对应的TF源码是[仓库](https://github.com/tensorflow/tensorflow/blob/b0784e587b62eec6967196b745bba4db3a90ab0c/tensorflow/python/ops/lookup_ops.py#L37)的第543行），逐步回溯lookup的过程，包括在CPU和NPU上测试其input、HashTable/StaticHashTable初始化（隐式初始化、显式初始化）的有效性。

## 1.3 问题最终定位

NPU上关于HashTable的基类第一个子类KeyValueTensorInitializer未实现。开发人员表示HashTable涉及到io，单AICPU搞不定，需要周边组件配合，具体根据需求进行开发。                         

## 1.4 规避方法：

将资源类预处理过程放到host侧，详见 [修正脚本](https://gitee.com/HUAWEI-ASCEND/dashboard/projects/ascend/modelzoo/blob/master/contrib/Research/nlp/bigru/bigru_tf_wonderseen/thumt/data/dataset.py#L196)。

## 1.5 建议

HashTable是lookup实现的标配，lookup是自然语言处理中频繁使用的模块。它的作用是根据{String: int64_value}字典，进行 string->int_value 映射，把文字变成编码数字符号，因此是NLP中特别常见的一个feature，如果能支持该算子，可以有效节约NLP开发者的时间。



# 二. 非资源类算子问题

## MatMulV2

详见[issue](https://gitee.com/ascend/modelzoo/issues/I253AU?from=project-issue) ，注意打开NPU双精度模式：

```python
def npu_config(param=None):
    config = tf.ConfigProto()
    custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
    custom_op.name = "NpuOptimizer"
    custom_op.parameter_map["precision_mode"].s = tf.compat.as_bytes("allow_mix_precision") # necessary for supporting conv2d and matmulv2
    custom_op.parameter_map["use_off_line"].b = True
    config.graph_options.rewrite_options.remapping = RewriterConfig.OFF 
    return config
```



# 三、动态shape问题定位

## 3.1 定位

NPU算子暂时无法支持dynamic shape，前期测试graph下沉的过程中，打不通训练图。

在负责人员的帮助下进行联合的线上调试，定位出序列下标访问错误。原因是：对输入文本序列做padding时所采用的PaddingFIFOQueueV2动态shape算子被AICPU拦截，在显存访问上暂时不支持dynamic shape，因此对于graph中节点的dynamic shape属性不兼容。

预计2021年一月底的AICPU版本会增加兼容动态shape的特性，所提issue和开发人员回复详见：

1. https://gitee.com/ascend/modelzoo/issues/I2463D?from=project-issue 
2. https://gitee.com/ascend/modelzoo/issues/I24IEJ?from=project-issue 

## 3.2 需求

dynamic shape在NLP中是一个十分常见的环节，比如输入序列的padding过程、decoder环节、RNN的传播、文本抽取等，所以dynamic shape是一个比较常见的feature，如果没有修复，后期NLP算法用户在使用NPU时体验感会比较差。

## 3.3 （模型侧）RNN动态算子规避方案总结

目前解决dynamic shape暂时只能等到兼容动态shape的版本上线，因此，在项目进行中我们采用静态shape、规避动态shape的思路进行。

以RNN为基础结构的自回归NMT模型，可以参考以下方法进行规避（或见[链接](https://gitee.com/HUAWEI-ASCEND/dashboard/issues?id=I23EHA)）：

1. 在使用TF的dynamic_rnn时，把全局的seq_len设置为一样的大小，规避tf.while引入的问题；这种情况在图中还是用小算子拼接出来RNN算子。
2. scope融合，融合的部分查看：[链接](https://gitee.com/ascend/canndev/blob/development/ops/built-in/framework/tf_scope_fusion_passes/scope_dynamic_rnn_pass.cc)

在官方文档中查询的scope融合方式为c++的实现（见[链接](https://support.huaweicloud.com/devg-fr-tensorflow/atlasfrdg_25_0016.html)）（推测其类似于将RNN的多个相同的环节放在同一个scope域中重复调用）在Bi-GRU中，我们按照以上方式形成RNN模块。

但是，过多的小算子对速度损失会有大的影响吗？分析过程详见“第6节 效率对比”。

## 3.4 （工程侧）NLP中常见动态shape算子规避方案总结

1. tf.while

   将tf.while重写为while形式时，要进行scope和variable的校验。尽量避免在while循环中使用带有局部scope或者存在新的variable生成的算子。（调试过程中的一个经验是使用variable_scope，不用name_scope。如果用name_scope，不会有scope被重复定义的问题，但是参数无法直接reuse。如果用variable_scope，可以使用reuse特性。）为了可读性以及在while中始终使用同一个scope，最好将scope定义放在while之前。

2. tf.TensorArray

   根据tensorflow算子说明，可以将该算子的输入参数dynamic_shape设置为False。（该算子的静态版本在BiGRU中没有测试）

3. tf.contrib.training.bucket_by_sequence_length

   该算子默认使用dynamic shape。使用静态shape需要用户手动设置参数dynamic=False、同时给定一个预先定义的shape，即可生成静态shape输出。具体可看[链接](https://gitee.com/ascend/modelzoo/blob/master/contrib/Research/nlp/bigru/bigru_tf_wonderseen/thumt/data/dataset.py)，关键部分代码如下：

```python
with tf.name_scope("batch_examples"):
    (_, outputs) = tf.contrib.training.bucket_by_sequence_length(
        max_example_length,
        example,
        batch_sizes,
        [b + 1 for b in boundaries],
        num_threads=num_threads,
        capacity=2,  # Number of full batches to store, we don't need many.
        bucket_capacities=bucket_capacities,
        keep_input=(max_example_length <= max_length),
        dynamic_pad=using_dynamic,
        shapes=[[max_example_length], [], [tgt_length], []] if not using_dynamic else None,
    )
return outputs
```





# 四.dump所遇问题复盘

## 4.1 建议严格按照tf.Session

精度对比，需要在同样的设置下dump数据，注意控制随机操作产生的影响，分别包括dropout、数据shuffle、保证restore参数而不是随机初始化等。

在调试过程中，不同的Session子类的数据读取可能会有差异。比如在GPU模式或者CPU模式下，使用**tf.train.MonitoredTrainingSession**即使不对data进行shuffle，data的读取仍然可能和预期不一致（比如会错过数据开头的若干个sample），但是NPU模式下，**tf.train.MonitoredTrainingSession**却能按照正确的数据顺序读取。在某些情况下，使用其他Session子类可能会造成调试的困惑，比如：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164741_e139881e_8147558.jpeg "1.jpg")

在one-hot作为初始输入的情况下，一个batch中数据非0位置比较稀疏，即使数据读取发生错乱的，按照点积的计算形式，dump的cosine值也依然会很大（如index 13所指向的input:0行）；紧接着之后的操作会出现cosine值骤然降的情况（如index 13所指向的output:0行）。

lookup是参数抽取过程，那么，究竟是NPU上lookup算子存在问题、embedding由float32转换为float16存在问题、NPU dataloader存在问题还是CPU dataloader的问题。这个问题的定位又产生了不必要的调试。在改回tf.Session后，NPU/GPU/CPU形成一致的数据读取：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164812_f50ec131_8147558.jpeg "2.jpg")

所以，一方面，在batch size小的情况下，稀疏的one-hot输入的cosline值不太具备参考意义；另一方面，为了提升调试的效率，建议dump过程按照官方文档使用tf.Session而不是子类。

## 4.2 Dump模型参数进行校验

在测试模型加载由float32转float16是否产生了精度损失时，产生了些疑问：NPU上会dump计算结果，但是貌似不会dump出模型参数本身，是否可以设置成NPU下默认dump出模型参数？

如：cpu中准确命名的参数**rnnsearch/source_embedding/embedding**，是会被默认dump出来：

 ![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164826_86b566bb_8147558.jpeg "3.jpg")

在NPU的计算图上也可以看到这个节点下沉：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164851_12b29d00_8147558.jpeg "4.jpg")

但是，dump data中找不到**rnnsearch/source_embedding/embedding**，

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164900_774f8a5c_8147558.jpeg "5.jpg")

NPU dump找不到与**rnnsearch/source_embedding/embedding**同名的tensor，所以为Cosine为NaN。

为了验证参数加载是否出错，多绕了一下，通过 tf.assign 从 checkpoint 加载的参数的dump data来进行比较。不过，我们无论是否显式为tf.assign设置operator name为**rnnsearch/source_embedding/embedding**，NPU上dump数据还是被统一命名成了 *“Assign.save_Assign.+数字”* 的形式。根据文件的大小，确实可以定位出与cpu的**rnnsearch/source_embedding/embedding.npy**匹配的NPU dump文件，但是会相对麻烦。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164915_0c80bf9e_8147558.jpeg "6.jpg")

## 4.3  利用shape信息定位出某些节点的位置

根据dump的精度对比，rnnsearch/strided_slice:0节点最早出现问题，需要定位出该算子位置。但是，许多tensor操作都可能用到strided_slice，而且模型graph本身不一定是按照forward函数中从前到后依次执行，可能是根据graph多个分支并行执行。有的模型比较复杂，scope可能切分得不够细致，仅仅根据模型的forward过程，定位出这个算子位置不太方便，借助graph中查找这个算子节点会比较方便。借助它的输入输出，定位出它的位置。

```shell
grep -C 1 -rn "rnnsearch/strided_slice:0" *.pbtxt
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164930_7637ecd7_8147558.jpeg "7.jpg")

推测大概格式是：

```shell
output: "rnnsearch/strided_slice:0"
input: "rnnsearch/strided_slice:0"
```

我们查询一下***_Build.txt**中以这个节点为输出的位置：

```shell
grep -rn "output\: \"rnnsearch/strided_slice:0" *_Build.pbtxt
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164939_a21b13c5_8147558.jpeg "8.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164959_80eb8387_8147558.jpeg "9.jpg")


根据对模型的理解，以及该节点的输入输出shape属性，我们容易排查出这个算子是写于forward函数中靠近结尾处的operator。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/171822_b3620521_8147558.jpeg "10.jpg")

# 五、 精度调优过程复盘

调优环节，起到作用的几条tfdebug主要指令：

```shell
# 查看tensor的大小
pt tensor_name

# 定位到具体算子和代码，也可查看算子上下文
ni -t tensor_name
ps source_file_path

# 查看是否产生inf或者nan计算结果
run -f has_inf_or_nan

# 使用numpy正则表达式执行numpy，例如：
eval "np.max(`tensor_name`)"
```

在CPU和NPU上，根据dump结果，我们观察到fp32定义模型参数时候计算混合精度梯度，梯度有大的误差：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165014_c51b02cd_8147558.jpeg "13.jpg")

在CPU和NPU上，我们都采用fp16定义模型参数，查看是否会和混合精度产生相对一致的结果，此时前向传播的指数级算子softmax entropy有大的误差，梯度的误差较小：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165057_a8533edf_8147558.jpeg "14.jpg")

虽然fp16的单步计算结果差异较小，但是由于精度不同，fp16和fp32最终能取得的训练效果差异比较大。

我们先采用fp32训练，3k step后跑飞。使用fp16训练，1.7w step后loss跑飞，如下图。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165157_f43e9be9_8147558.jpeg "15.jpg")

我们怀疑NPU上可能会产生inf结果，inf很可能在cpu上造成严重的训练错误。

跑飞前最新的checkpoint的inf debug如下：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165209_33c093f9_8147558.jpeg "16.jpg")

对于inf梯度，从其他工程代码中发现有两种方式，一种直接置零，一种直接置一个比较大的常数（比如常数1.0）。在混合精度训练，将inf梯度置为1.0有一定效果：可以稳定训练到4w step，BLEU达到20.4，loss到达1.8左右依然会跑飞，混合精度本身有计算误差，只是简单置位为固定数值的方式依然不太理想。

综合以上，我们最后再结合loss scale将loss和梯度进行一下缩放，减小计算误差，同时置位inf为0，就可以比较稳定的训练了。见下图，同样的学习率，经过修正后的模型loss更加稳定。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165216_ff19b384_8147558.jpeg "17.jpg")

训练结果，以beam-search-size=1为基准：

- GPU效果为：BLEU-26.760

- NPU效果为：BLEU-26.7624

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165227_0cbe5060_8147558.jpeg "18.jpg")



# 六、性能

## 6.1 decoding方式对推理速度的影响

根据Bi-GRU的源代码的逻辑，在推理阶段中，每翻译下一个词（**token**）之前，需要把已翻译的词（tokens）重新输入到BiGRU中，接着从头到尾执行一次完整encoder-decoder过程。假设目标语句有m个token，以上过程就要执行m次。这个过程导致推理过程的翻译有比较大的延迟。

考虑到翻译时，encoder的输出状态不随翻译的进行而改变，只有decoder所使用的tensor是随着tokens的生成而改变，因此，decoder需要等待当前step的隐藏状态的生成后，才能去生成下一个字。

前者和后者的时间复杂度（或者说造成的翻译延迟）分别是多少？我们不妨假设要编码的源语句token数目是n，要解码的目标句token数目是m，简化的时间复杂度比较如下：

\-    在encode阶段，有两个GRU cell形成双向的GRU，时间复杂度是2n。

\-    在decode阶段，有1个GRU cell做单向GRU，时间复杂度是m。

按照源代码的形式，翻译一个m个单词的句子，要串行逐次翻译m个单词：encoder阶段被重复执行m次（时间复杂度2mn），decode阶段被重复执行(1+m)m/2次。翻译的时间复杂度是2nm+(1+m)m/2。

在工程上，同一个batch里某些sequence可以提前停止，但是这个过程经常用tf.while来构建graph，这又会造成dynamic shape，很有可能在NPU上跑不起来，所以，这意味着源代码被修正为static shape版本后，推理延迟将会更高。因为每个句子无论长短，都要按照预定义的最长的sequence length来执行编码和解码。

考虑到不需要重复调用encoder，更合理的推理方式是在一次encoder scope前向传递的过程中，我们就保存住encoder的编码输出；之后不再需要重新跑一次encoder scope 所在的计算图。在decoder前传过程中，我们记录每次的中间状态，用来直接预测下一个单词，这样每个单词的翻译延时缩短为O(1)的时间复杂度，decoder翻译所有单词的时间复杂度降低为O(m)。时间复杂度降低为O(2n+m)。

## 6.2 static shape和dynamic shape的训练速度对比

由于我们改写的static shape版本增加了许多小算子进行拼接，我们需要测试一下额外的拼接是否影响模型的训练速度。我们对比了dynamic版本和我们重写的static版本。

本地GPU下测试：

- dynamic version

  最长的处理序列是source=[128, 60]，target=[128, 60]，该序列的单步耗时为0.48s。大部分source和target的长度都小于60，单步耗时一般在0.1-0.4s之间：

- static version：文本尺寸都维持在[128, 60]，单步耗时0.46s-0.48s。

在这个同文本长度输入的情况下，static version比dynamic shape快0.01s，说明static version中小算子拼接方法不会有速度的损失。

Ascend910上：

- NPU：1.3s/step浮动

- CPU：4.3s/step浮动

相比于在GPU上一个step时间为0.4s，NPU要慢得多。潜在的原因可能是小算子对NPU不友好（根据之前的案例报告，小算子切换都可能会造成NPU上s级的时间成本）。

## 6.3 数据加载方式对比

## （1）tf.data.Dataset.from_tensor_slice

在前期小样本测试时，使用from_tensor_slice成功下沉graph。在后期训练时，逐步增加数据规模，测试模型的性能，在平行语料由200w提升到300w时，模型超出了graph serialized limit，log如下：

```python
InvalidArgumentError: Cannot serialize protocol buffer of type tensorflow.GraphDef as the serialized size (2563210554bytes) would be larger than the limit (2147483647 bytes)
```

出现这个溢出的原因是：from_tensor_slice是直接将host侧所有数据执行一次tf.constant，定义到graph中作为资源，因此它不是从数据中做iterator sample，这样定义的资源节点占用大量的serialized space(详见[链接](https://github.com/tensorflow/tensorflow/blob/mm-cherry-pick-java-fixes-on-r1.15/tensorflow/python/data/ops/dataset_ops.py)):

```python
@staticmethod
def from_tensor_slices(tensors):
   """Creates a `Dataset` whose elements are slices of the given tensors. Note that if `tensors` contains a NumPy array, and eager execution is not enabled, the values will be embedded in the graph as one or more `tf.constant` operations. For large datasets (> 2 GB), this can waste memory and run into byte limits of graph serialization. If `tensors` contains one or more large NumPy arrays, consider the alternative described
  in [this guide](https://tensorflow.org/guide/datasets#consuming_numpy_arrays).
  Args:
    tensors: A dataset element, with each component having the same size in
      the 0th dimension.
  Returns:
    Dataset: A `Dataset`.
  """
```

训练Bi-GRU需要大量的平行语料（500w~600w平行语料），不适用from_tensor_slice方式加载。解决方案可以是选择from_generator方式来采样数据。

### （2）tf.data.Dataset.from_generator

from_generator没有直接支持返回dict的模式，手动处理成dict的形式，修正如下：

### （3）采用bucket构建batch

重写为from_generator后，为了保证训练效果，我们回头研究了源代码的训练数据处理方法。

源代码根据sequence length的范围来采样不同的数据到不同的bucket中等待训练，将长短句子分开训练，有利于防止模型训练过程弱化对长句子的翻译；短句子的翻译往往要比复杂的长句子容易，如果长短句子混合在同一个batch中，在batch loss做reduce_mean时，长句子的loss容易被短句子的loss平滑。

源代码中对此的实现，是调用接口tf.contrib.training.bucket_by_sequence_length。

为此，我们重新恢复这个采样特性，探查是否对Bi-GRU的训练精度有影响。脚本详见：[get_training_input](https://gitee.com/HUAWEI-ASCEND/dashboard/projects/ascend/modelzoo/blob/master/contrib/Research/nlp/bigru/bigru_tf_wonderseen/thumt/data/dataset.py)

根据文本长短设置Bucket的方式，对于训练Bi-GRU而言有明显的帮助吗？

- 训练前期

  不使用bucket读取batch，loss会出现大的起伏。使用bucket进行训练，loss比较稳定。

- 训练后期

  我们在GPU上分别让这两种形式训练足够久的步数，前期带bucket的方式收敛更快，后期bucket只比不带bucket高约0.001 bleu。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165340_d5bace20_8147558.jpeg "11.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/165348_1725aa63_8147558.jpeg "12.jpg")

猜测造成这个现象的原因：带bucket的方式所见的文本规格更相近，所能组成的batch更少，模型前期容易重复学习这种更简单的数据组合，收敛更容易。但是，bucket同时导致batch的多样性降低，当训练步数达到一定规模，不带bucket的方式也让模型重复见到了足够多的数据组合，bucket所能带来的提升不大。

### （4）Host侧简单加速预训练平行语料处理

单线程下，560w平行语料在加载和预处理的效率比较低，导致模型启动耗时接近5分钟。为了提高效率，我们可以简单地修正为多线程加载模式。

具体参考[链接](https://github.com/uqfoundation/multiprocess)，调用multiprocess加速host侧数据读取，host侧的数据预处理从4分钟降低到2.5分钟。

## 6.4 去除不必要的reshape和临时参数

鉴于NPU上的运算速率比GPU上慢得多，我们尝试减少或者合并不必要的operator来减少运算，其中最主要的两个方面是：合并大tensor上的多余reshape操作、减少临时参数的出现（如tf.zeros）。

在GPU上时间变化不大，在NPU上的单步运行时间中有较大改善（不排除可能是当天有工作人员更新了算子使得速度提升）：2.355秒/step -> 1.355秒/step。



# 七、工具

由于Ascend910没有debug整理好的开发文档，debug过程不方便，所以这里把遇到的问题和解决方法进行总结，方便以后同学的debug进行（记于2020/12/28，具体的实现请以最新的文档为主）：

## 7.1      关闭NPU debug的log输出

\-    修改/var/log/npu/conf/slog/slog.conf 的 global_level 为3或4

\-    生效上述修改，/usr/local/Ascend/driver/tools/docker/slogd &

（注：根据开发人员提示，以tf adaptiver开头的信息无法屏蔽）

## 7.2      dump和debug的关键字

```
export PRINT_MODEL=1
export DUMP_GRAPH=2
export ENABLE_NETWORK_ANALYSIS_DEBUG=1
export SLOG_PRINT_TO_STDOUT=1
```

## 7.3      查看跑在某块卡上的进程号

暂时好像没有这个功能，只能根据运行的记录来回溯NPU卡上的进程号。

```
ps -aux | grep <该卡所对应脚本执行指令>
```

## 7.4      查看NPU内存占用情况

```
npu-smi info
```

## 7.5      Devide-os日志路径

```
/var/log/npu/slog/devide-*
```

## 7.6      DUMP与精度对比工具

- 改写NPU模式脚本并dump。如果硬盘空间不够，适当改小batch和step记录次数。                                    

- 改写CPU模式脚本，tfdebug模式下：

```shell
1. run

2. lt > tensor_name

3. 新窗口同路径下执行：timestamp=$[$(date +%s%N)/1000] ; cat tensor_name | awk '{print "pt",$4,$4}' | awk '{gsub("/", "_", $3);gsub(":", ".", $3);print($1,$2,"-n 0 -w "$3".""'$timestamp'"".npy")}' > tensor_name_cmd.txt

4. 回原窗口，粘贴tensor_name_cmd.txt的全部内容到tfdebug中执行。

5. 找到size最大的*_Build.txt文件
head -n 3 *_Build.txt | ll *_Build.txt

例如：ge_proto_00096_Build.txt
root@ubuntu:/home/train_use/tianda/wdx/# head -n 3 *_Build.txt | ll *_Build.txt
-rw------- 1 root root  1431954 Dec 19 05:26 ge_proto_00047_Build.txt
-rw------- 1 root root 75892181 Dec 19 05:28 ge_proto_00096_Build.txt
-rw------- 1 root root     9543 Dec 19 05:28 ge_proto_00145_Build.txt
root@ubuntu:/home/train_use/tianda/wdx/#

6. 开始比对，运行下方脚本
export LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/atc/lib64:${LD_LIBRARY_PATH}

NPU_ROOT=tmp/20201218212600/0/ge_default_20201218212620_31/2/0/ # NPU的dump目录
INPUT_TXT="./../ge_proto_00096_Build.txt" # cpu下的dump的graph
CPU_ROOT="./../" # INPUT_TXT所在的路径

## txt -> json
OUTPUT_JSON="${INPUT_TXT}.json"
/usr/local/Ascend/ascend-toolkit/latest/atc/bin/atc --mode=5 --om=$INPUT_TXT --json=$OUTPUT_JSON #--log debug

## compare single vector
# COMPARE_ROOT=/usr/local/Ascend/ascend-toolkit/latest/toolkit/tools/operator_cmp/compare
# python3.7 $COMPARE_ROOT/compare_vector.pyc -l $NPU_ROOT -r $CPU_ROOT -f $OUTPUT_JSON -o $OUTPUT_JSON

## compare the whole graph
python3.7 $COMPARE_ROOT/msaccucmp.pyc compare -m $NPU_ROOT -g $CPU_ROOT -f $OUTPUT_JSON
   
```

- 当精度对比出现大的偏差时，可以利用tfdebug结合tensorboard进行进一步的调试。

- 当cpu上查询不到某些节点时，也可以利用NPU的dump图来确定某个目标节点输入输出

  以rnnsearch/strided_slice为例子：

  ```shell
  grep -C 10 -rn "rnnsearch/strided_slice\"" *.pbtxt > find_rnnsearch_strided_slice.log
  ```

  