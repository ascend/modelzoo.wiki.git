### 【问题描述】
软件包版本：20.0
按照安装文档操作，安装好依赖后，执行./Ascend-Toolkit-20.0.RC1-x86_64-linux_gcc7.3.0.run --install报错
![image.png](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/11/134956sixxil1qc3hvslms.png)

### 【问题分析】
1、查看umask值以及检查环境准备和依赖是否安装成功
![image.png](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/11/1350488ivl4folc0svugkt.png)

经确认均没有问题。

2、使用命令
```
./Ascend-Toolkit-20.0.RC1-x86_64-linux_gcc7.3.0.run  --extract=./Ascend-Toolkit-20.0.RC1-x86_64-linux_gcc7.3.0 --noexec

```
将run包解压，进入查看Ascend-Toolkit-20.0.RC1-x86_64-linux_gcc7.3.0/script目录查看toolkit_install.sh安装脚本
找到错误的地方，发现是检查驱动是否已经安装这里：

![image.png](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/11/141356a3nv6dwhwpzqttqx.png)


![image.png](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/11/1402048rg5prwh2cyf7cgh.png)


**由于用户在环境上安装了200加速模块的驱动包，所以安装后软件包的安装路径、安装命令以及运行用户信息记录路径为/etc/ascend_install.info，这个和200DK开发套件包检查的位置是一样的，检查到文件后会去找驱动的配置文件，这里没有检测到，所以会报错，于是安装失败。**

### 【问题解决】
**删除/etc/ascend_install.info 文件后重新安装即可安装成功**