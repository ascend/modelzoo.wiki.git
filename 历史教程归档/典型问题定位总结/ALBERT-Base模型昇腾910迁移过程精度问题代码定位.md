### 【写在前面】

本经验分享由天津大学自然语言处理实验室TJUNLP Lab ([实验室主页](https://tjunlp-lab.github.io/)) 提供，欢迎各开发者一同学习交流。

### 【问题描述】

由于ALBERT是属于BERT的衍生种类，参考ModelZoo中的Officials目录下的Bert模型修改代码完成迁移；但是，在NPU上跑通后发现精度达不到论文的要求，F1 score仅能达到50，达不到论文的80结果。

### 【问题分析】

查阅[精度对比手册](https://support.huaweicloud.com/ti-maa-A800_9000_9010/altasprecision_16_001.html)，让模型在gpu和npu上跑的模型输入一条相同的数据

得到了精度对比结果:

![输入图片说明](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/23/192630f5jn2csu7pimdxim.png "在这里输入图片标题")

按照网络结构从上往下找，定位到bert/encoder/transformer/group_0/layer_0/inner_group_0/attention_1/self/Cast这个算子是第一个出现较大偏差的算子（cos距离为0.5）,但是光知道这个算子出了问题，只能知道是在这个范围内出了问题，不知道具体哪一行出了问题，因此使用tensorflow自带的调试工具来定位代码问题，由于在精度对比之前就已经在gpu的模型代码上加上了调试模式，因此只要再运行一遍gpu的代码进入调试模式：

![输入图片说明](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/23/193508oyaifbddphgg9yrj.png "在这里输入图片标题")

在命令界面打run，运行一次,一页一页往下翻，找到之前出错的算子bert/encoder/transformer/group_0/layer_0/inner_group_0/attention_1/self/Cast

![输入图片说明](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/23/193935kzx7tlwbspjsavjp.png "在这里输入图片标题")

 点击进去后，在点node_info就可以看到这个算子是在具体哪一行怎么被运行的

![输入图片说明](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/23/194136jxvnwjdlrqjl5wcu.png "在这里输入图片标题")

### 【问题解决】

问题出在了在模型内部对bias进行强制类型转换为tf.float32

![输入图片说明](https://bbs-img-cbc-cn.obs.cn-north-1.myhuaweicloud.com/data/attachment/forum/202011/23/194331kzn0ibyps1go9dnq.png "在这里输入图片标题")

由于使用的是混合精度模式，在该模式下，会把将部分算子转化为float16的形式进行训练，在模型内部强制转换成float32导致在训练的时候出现精度损失。

将模型中对该算子输出转类型的tf.cast算子去掉，把数据类型转换移到模型的外部后，再训练，得到的F1 score为79.4，和论文的结果80很接近了。