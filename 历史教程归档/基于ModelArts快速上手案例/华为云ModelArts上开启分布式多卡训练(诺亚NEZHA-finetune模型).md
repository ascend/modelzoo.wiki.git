### 准备工作

1.ModelArts使用准备

首先需要完成账号注册、ModelArts配置和创建桶的准备工作。
>![](public_sys-resources/icon-note.gif) **说明：** 
   >ModelArts教程链接：[https://support.huaweicloud.com/wtsnew-modelarts/index.html](https://support.huaweicloud.com/wtsnew-modelarts/index.html)。页面提供了较丰富的ModelArts教程，参考“准备工作”部分完成ModelArts准备工作。

2.拥有云上Ascend 910多卡资源

需要确保您的华为云账号已拥有Ascend 910多卡资源(白名单方式管控)，Region建议选择北京四(公共资源池中提供昇腾训练资源)

3.准备数据和训练脚本
   ModelArts使用对象存储服务（Object Storage Service，简称OBS）进行数据存储，因此，在开始训练任务之前，需要将数据和训练脚本上传至OBS。

   1. 单击[下载链接](https://dumps.wikimedia.org/zhwiki/)，下载数据集解压至本地。

   2. 单击[下载链接](https://git.huawei.com/w00520830/nezha-910)[当前只能由高校对接的项目经理访问，后面会同步到github上]，下载训练脚本至本地。该脚本对可以在ModelArts采用8 * Ascend 910 CPU规格进行训练任务。

   3. 将数据集和训练脚本按照如下目录结构上传至已创建好的OBS桶（例如：test）。同时为了方便后续创建训练作业，需要创建训练输出目录和日志输出目录。

      ```
        test
          ├dataset                                     //用于存放数据集
          ├──train
          ├──val
          ├config                                      //用于存放配置文件
          ├train                                       //用于存放训练脚本
          ├──run_pretraining_npu_nezha_mx_efs.py
          ├──run_classifier.py
          ├──modeling_nezha.py
          ├──tokenization.py
          ├──optimization.py
          ├output                                      //用于存放输出文件
          ├log                                         //用于存放日志文件
      ```

### 通过简单适配将TensorFlow脚本运行在ModelArts

在我们将TensorFlow的训练脚本迁移到NPU上跑通后，还需要对训练脚本进行一定的适配工作后才能最终在ModelArts上运行起来，以下就是介绍如何对TensorFlow代码进行简单的适配工作。

1.在训练启动脚本中加上以下环境变量，如开启静态内存和设置HCCL连接超时时间


   ```
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    os.environ.pop('http_proxy', None)
    os.environ['WHICH_OP'] = 'GEOP'
    os.environ['NEW_GE_FE_ID'] = '1'
    os.environ['GE_AICPU_FLAG'] = '1'
    os.environ['HCCL_CONNECT_TIMEOUT'] = '600'
    os.environ['GE_USE_STATIC_MEMORY'] = '1'

   ```
2.导入ModelArts上的自研组件 MoXing API，在Run函数中开启分布式多卡训练

导入moxing api
`import moxing.tensorflow as mox`

导入hccl模块

`from hccl.split.api import set_split_strategy_by_size`

设置分布式数据并行拆分策略

set_split_strategy_by_size([10, 10, 10, 10, 15, 15, 15, 15])

```
        mox.run(input_fn=input_fn,
                model_fn=model_fn,
                optimizer_fn=optimizer_fn,
                checkpoint_path=FLAGS.init_checkpoint,
                run_mode=mox.ModeKeys.TRAIN,
                batch_size=FLAGS.train_batch_size,
                log_dir=FLAGS.nas_train_url if FLAGS.nas_train_url else FLAGS.train_url,
                max_number_of_steps=max_number_of_steps,
                log_every_n_steps=FLAGS.log_every_n_steps,
                save_model_steps=FLAGS.save_interval_steps,
                save_summary_steps=FLAGS.save_summaries_steps,
                auto_batch=False)
```

3.在ModelArts运行的脚本必须配置data\_url和train\_url，分别对应数据存储路径\(OBS路径\)和训练输出路径\(OBS路径\)，训练启动脚本中解析命令行参数中需要加上这两参数

   ```
   flags.DEFINE_string(
       "train_url", "./output/",
       "The output directory where the model checkpoints will be written.")
   flags.DEFINE_string("data_url", "../dataset/",
                       "SQuAD json for training. E.g., train-v1.1.json")
   ...
   data = Data(batch_size=20, data_path=os.path.join(FLAGS.data_url, "train"),
                   val_data=os.path.join(FLAGS.data_url, "val"),
                   test_data=os.path.join(FLAGS.data_url, "val"))
   ...
   saver.save(sess=sess, save_path=os.path.join(FLAGS.train_url, "model"))
   ```

4. ModelArts界面支持向脚本中其他参数传值，在下一章节“创建训练作业”中将会详细介绍。

   ```
   flags.DEFINE_float(
       "train_batch_size", 64,
       "Total batch size for training.")
   flags.DEFINE_float(
       "learning_rate", 5e-4,
       "The initial learning rate for Adam.")
   ...
   optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate).minimize(cost)
   ```

### 创建训练作业

准备好数据和执行脚本并上传至OBS存储后，就需要在ModelArts训练平台上创建训练作业将训练任务真正执行起来，ModelArts提供多种交互界面来创建训练作业，而网页端是最快捷方便的，我们的教程就以网页端为例来介绍一下。

1.进入ModelArts控制台。

  打开华为云ModleArts主页 https://www.huaweicloud.com/product/modelarts.html，点击该页面的“进入控制台”。

2.创建基于常用框架的训练作业

  ModelArts教程https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0231.html展示了如何创建训练任务。
  
  创建训练作业的步骤如下：
  
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/212831_1e4ef50b_1869725.png "屏幕截图.png")

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/164203_6066e3c3_1869725.png "屏幕截图.png")

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/164317_e740f09e_1869725.png "屏幕截图.png")

3.配置超参
  
  算法来源：选择算法管理。

  算法名称：选择上一步创建好的算法。

  训练输入：选择数据存储位置，并填入OBS中数据集的位置。

  训练输出：选择相应路径用来上传训练输出文件。

  作业日志路径：选择相应路径用来上传日志。

  资源池：选择公共资源池>Ascend。规格选择Ascend:8*Ascend 910 CPU：192核 768GiB，表示单机单卡规格。

  计算节点个数：根据实际需要选择，例如1表示单卡训练。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/213606_f551005e_1869725.png "屏幕截图.png")

其中 `using_data_split=True`
     `hcom_parallel=True`
这两个超参在开启分布式训练时需要设置，不能取默认值

### 查看运行结果

1.在训练作业界面可以查看运行日志。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/210214_0f46e0fc_1869725.png "屏幕截图.png")

2.点击资源占用情况页签，可以看到NPU使用率及磁盘读写速率

![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/210412_c3676b72_1869725.png "屏幕截图.png")