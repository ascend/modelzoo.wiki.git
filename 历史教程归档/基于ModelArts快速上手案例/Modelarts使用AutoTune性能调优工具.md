## Modelarts使用AutoTune工具性能调优(基于CANN20.2)

本案例针对Modelarts云上训练环境，如果是裸机或者依瞳环境，请忽略本wiki，参考[AutoTune资料](https://support.huaweicloud.com/Development-tg-cann202training1/atlasautotune_16_0003.html)操作

### 1.概述
Auto Tune工具的作用就是充分利用硬件资源进行算子的自动调优。用户在生成网络模型时，仅需要打开auto_tune_mode开关使能Auto Tune工具，就可以在算子编译时自动调用Auto Tune工具进行算子调优，调优后的结果会放在自定义知识库中，在后续调用相关算子时便可直接享受到调优后的算子性能。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/165703_f1bc8401_7866623.png "屏幕截图.png")
本案例针对使用ModelArts，介绍如何使用AutoTune工具进行性能调优。


### 2.修改脚本
#### 2.1 如果训练脚本中使用了initialize_system接口，请按照如下配置使能Auto Tune：
```
npu_init = npu_ops.initialize_system()
npu_shutdown = npu_ops.shutdown_system()
config = tf.ConfigProto()
...
custom_op.parameter_map["auto_tune_mode"].s = tf.compat.as_bytes("RL,GA")
...
with tf.Session(config=config) as sess:

    sess.run(npu_init)
    #调用HCCL接口...
    #执行训练...
    sess.run(npu_shutdown)

```
#### 2.2 如果训练脚本中未使用initialize_system接口，请按照如下配置使能Auto Tune。
##### 2.2.1 通过session配置项中的auto_tune_mode参数设置：
```
session_config = tf.config(…)
custom_op = session_config.graph_options.rewrite_options.custom_optimizers.add()
…
custom_op.parameter_map["auto_tune_mode"].s = tf.compat.as_bytes("RL,GA")
```
##### 2.2.2 通过NPURunConfig中的auto_tune_mode参数设置：
```
session_config=tf.ConfigProto()
config = NPURunConfig(auto_tune_mode="RL,GA", session_config=session_config)
```

#### 3.设置并创建调优后算子信息库的路径
在训练脚本中增加：
```
import os
os.environ["TUNE_BANK_PATH"]='/cache/AutoTune_Bank'
os.mkdir('/cache/AutoTune_Bank')
```

#### 4.开始调优
使用修改后的训练脚本，创建训练任务：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/170934_db9714f2_7866623.png "屏幕截图.png")
开始调优：
![](https://images.gitee.com/uploads/images/2021/0301/171117_383c95f4_7866623.png "屏幕截图.png")
喝杯咖啡，耐心等待

#### 5.将容器内调优后的知识库传输至OBS桶
在训练脚本中，调用moxing接口：
```
import moxing as mox
mox.file.copy_parallel("/cache/AutoTune_Bank", 's3://xxxxx/')
```

脚本运行结束后就可以在我们的OBS桶里看到调优后的知识库。

#### 6.在重新训练时使用调优的知识库
**注意：再次启动训练，只需要设置TUNE_BANK_PATH环境变量即可**

##### 6.1 从obs桶拷贝知识库到ModelArts
训练脚本中增加：
```
import moxing as mox
mox.file.copy_parallel("s3://xxxxx/AutoTune_Bank", '/cache/')
```
##### 6.2 设置TUNE_BANK_PATH环境变量
在训练脚本中增加：
```
import os
os.environ["TUNE_BANK_PATH"]='/cache/AutoTune_Bank'
```
这样就会优先使用调优后的知识库启动训练，可以对比观察，训练性能得到提升。

#### 7.知识库合并（可选）
调优后的知识库，Auto Tune工具提供了自定义知识库合并的功能，支持将不同路径下的自定义知识库进行合并，并将其中优于内置知识库中的调优策略存入目标路径中。
需要将调优后的知识库拷贝至开发环境，参考下面的操作步骤来合并：
[知识库合并](https://support.huaweicloud.com/Development-tg-cann202training1/atlasautotune_16_0017.html)