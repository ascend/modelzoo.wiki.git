## 概述<a name="section10628144818142"></a>

ModelArts是华为云提供的面向开发者的一站式AI开发平台，集成了昇腾AI处理器资源池，用户可以在该平台下体验如何执行TensorFlow训练任务。

本教程以一个简单的已迁移好的alexnet网络训练脚本为例，简要介绍如何在ModelArts执行TensorFlow训练任务。

## 准备工作<a name="section09342092166"></a>

1. ModelArts使用准备。

   参考ModelArts教程“准备工作”一栏，完成账号注册、ModelArts配置和创建桶的准备工作。

   >![](public_sys-resources/icon-note.gif) **说明：** 
   >ModelArts教程链接：[https://support.huaweicloud.com/wtsnew-modelarts/index.html](https://support.huaweicloud.com/wtsnew-modelarts/index.html)。页面提供了较丰富的ModelArts教程，参考“准备工作”部分完成ModelArts准备工作。

2. 拥有云上昇腾AI处理器资源。

   确保你的账号已拥有ModelArts华为云昇腾集群服务公测资格，可在[这里](https://console.huaweicloud.com/modelarts/?region=cn-north-4#/dashboard/applyModelArtsAscend910Beta)提交申请。

3. 准备数据和训练脚本。

   ModelArts使用对象存储服务（Object Storage Service，简称OBS）进行数据存储，因此，在开始训练任务之前，需要将数据和训练脚本上传至OBS。

   1. 单击[下载链接](https://modelarts-samples.obs.cn-north-4.myhuaweicloud.com:443/alexnet.zip?AccessKeyId=APWPYQJZOXDROK0SPPNG&Expires=1636180781&Signature=Opetw0j9BtFSMgS%2BawTqqhSeQIY%3D)，下载数据集和训练脚本并解压至本地。该脚本可以在ModelArts采用1\*Ascend规格进行训练任务。

   2. 将数据集和训练脚本按照如下目录结构上传至已创建好的OBS桶（例如：test）。同时为了方便后续创建训练作业，需要创建训练输出目录和日志输出目录。

      ```
        test
          ├dataset          //用于存放数据集
          ├──train
          ├──val
          ├train            //用于存放训练脚本
          ├──data.py
          ├──model.py
          ├──train_npu.py
          ├output            //用于存放输出文件
          ├log               //用于存放日志文件
      ```



## 通过简单适配将TensorFlow脚本运行在ModelArts<a name="section621144618246"></a>

本样例提供的脚本可以直接运行在ModelArts，想要快速体验本样例可以跳过本章节。

如果需要将自定义TensorFlow脚本或更多TensorFlow示例代码在ModelArts运行起来，需要参考本章节对TensorFlow代码进行简单适配。

1. 在ModelArts运行的脚本必须配置data\_url和train\_url，分别对应数据存储路径\(OBS路径\)和训练输出路径\(OBS路径\)。

   ```
   flags.DEFINE_string(
       "train_url", "./output/",
       "The output directory where the model checkpoints will be written.")
   flags.DEFINE_string("data_url", "../dataset/",
                       "SQuAD json for training. E.g., train-v1.1.json")
   ...
   data = Data(batch_size=20, data_path=os.path.join(FLAGS.data_url, "train"),
                   val_data=os.path.join(FLAGS.data_url, "val"),
                   test_data=os.path.join(FLAGS.data_url, "val"))
   ...
   saver.save(sess=sess, save_path=os.path.join(FLAGS.train_url, "model"))
   ```

2. ModelArts界面支持向脚本中其他参数传值，在下一章节“创建训练作业”中将会详细介绍。

   ```
   flags.DEFINE_float(
       "learning_rate", 5e-4,
       "The initial learning rate for Adam.")
   ...
   optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate).minimize(cost)
   ```


## 创建训练作业<a name="section3731123143511"></a>

准备好数据和执行脚本以后，需要创建训练任务将TensorFlow脚本真正运行起来。首次使用ModelArts的用户可以根据本章节了解ModelArts创建训练作业的流程。

1. 进入ModelArts控制台。

   打开华为云ModelArts主页[https://www.huaweicloud.com/product/modelarts.html](https://www.huaweicloud.com/product/modelarts.html)，点击该页面的“进入控制台”。

2. 创建算法。

   ModelArts教程[https://support.huaweicloud.com/engineers-modelarts/modelarts\_23\_0231.html](https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0231.html)展示了如何创建算法。

   以本教程的sample为例，详细列出在创建算子页面如何配置：

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/150358_ec333fa4_8267113.png "zh-cn_image_0290796682.png")
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/150410_6229afbc_8267113.png "zh-cn_image_0290796815.png")

   单击“下一步”，在“超参规范”页面中，您可以根据实际情况定义此算法的超级参数。超级参数的设置是可选的，可以不填写执行下一步操作。

   单击“下一步”，在“使用约束”页面中，您可以根据实际情况定义此算法的输入约束和训练约束，可以不填写直接提交。

3. 使用已有算法创建训练作业。

   ModelArts教程[https://support.huaweicloud.com/engineers-modelarts/modelarts\_23\_0236.html](https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0236.html)展示了如何使用常用框架创建训练作业。

   以本教程的sample，详细列出在创建训练作业界面如何进行配置：

   - 算法来源：选择算法管理。

   - 算法名称：选择上一步创建好的算法。

   - 训练输入：选择数据存储位置，并填入OBS中数据集的位置。

   - 训练输出：选择相应路径用来上传训练输出文件。

   - 作业日志路径：选择相应路径用来上传日志。

   - 资源池：选择公共资源池\>Ascend。规格选择Ascend:1\*Ascend910CPU：24核96GiB，表示单机单卡规格。

   - 计算节点个数：根据实际需要选择，例如1表示单卡训练。

   ![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/150437_acc2442e_8267113.png "zh-cn_image_0290906559.png")

## 查看运行结果<a name="section177791934185318"></a>

1. 在训练作业界面可以查看运行日志。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1111/150458_1d7f1770_8267113.png "zh-cn_image_0290924026.png")

2. 如果创建训练作业时指定了日志路径，可以从OBS下载日志文件并查看。

