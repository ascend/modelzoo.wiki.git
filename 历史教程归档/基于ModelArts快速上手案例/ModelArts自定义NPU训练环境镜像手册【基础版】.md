:exclamation:目前所有公开的镜像SWR_URL地址在[**这篇Wiki**](https://gitee.com/ascend/modelzoo/wikis/Modelarts%E5%85%AC%E5%BC%80%E9%95%9C%E5%83%8F%E5%88%97%E8%A1%A8%E4%BF%A1%E6%81%AF?sort_id=3413562)中已经列出，请根据自身当前遇到的问题选择合适的镜像使用。
- 如果列表中的镜像已经解决了您当前的问题，那么不需要开发者进行二次制作了，直接跳转到[**Pycharm上使用自定义镜像**](#Pycharm上使用自定义镜像)中直接使用该镜像即可
- 如果列表中的镜像无法解决您当前的问题，且需要通过替换文件才能解决，可参看[**自定义镜像的制作**](#自定义镜像的制作)小节
- 如果您需要基于全新的run包制作一个公共镜像，那需要从[**准备工作**](#准备工作)开始看起

---
本篇Wiki涉及的内容比较多，包括安装包(文件)的获取，环境的安装，公共镜像的制作，自定义镜像的制作以及Pycharm的使用，下面给出一个目录方便大家跳转到对应小节。
-  [**准备工作**](#准备工作) 
-  [**安装包/文件的获取**](#安装软件包的获取) 
-  [**申请ECS服务器**](#申请ECS服务器) 
-  [**公共镜像的制作**](#公共镜像的制作) 
-  [**自定义镜像的制作**](#自定义镜像的制作) 
-  [**Pycharm的使用**](#Pycharm上使用自定义镜像) 
-  [**其他**](#其他) 


## <span id="准备工作">准备工作</span>
- 下载软件包或者需要替换的文件
- 申请华为公有云arm架构的ECS服务器
- 预备知识[Dockerfile](https://docs.docker.com/engine/reference/builder/)


## <span id="安装软件包的获取">安装软件包/文件的获取</span>

1. 软件包
   :exclamation:  _如果需要重新安装软件包才涉及，如果是替换某些 py 或者 so 文件不涉及，可忽略本小节。_  
   - 固件和驱动件包获取[链接](https://www.huaweicloud.com/ascend/resource/Software)。

     ![驱动包截图图片](https://images.gitee.com/uploads/images/2020/1207/130906_2cb2962a_1482256.jpeg "驱动包截图图片.jpg")

   - 训练软件包获取[链接](https://www.huaweicloud.com/ascend/cann-download)。

     ![训练软件包](https://images.gitee.com/uploads/images/2020/1223/112146_58463312_1482256.png "屏幕截图.png")


2. 获取需要替换的文件
   
   这个要针对具体模型，如果issue中committer回复需要替换py文件或者so文件，找项目经理获取对应的文件。

## <span id="申请ECS服务器">申请ECS服务器</span>
由于自定义镜像需要，我们需要购买华为公有云arm架构的ECS服务器。虽然用的频率不是很高，但还是需要购买，购买ECS服务器的[链接](https://console.huaweicloud.com/ecm/?region=cn-north-4#/dashboard)。

![购买ecs服务器](https://images.gitee.com/uploads/images/2020/1207/144250_81844292_1482256.jpeg "购买ecs服务器.jpg")

我们选择**按需计费**，如下图所示。区域，我们选择**华北-北京四**，CPU架构选择**鲲鹏计算**，规格按我截图的即可（8vCPU 16GB），不用申请高配的，只是用来制作镜像用。我们不能使用x86架构的系统进行制作，因为基础镜像是Arm架构的。

![购买ecs服务器1](https://images.gitee.com/uploads/images/2020/1207/144342_64dce49b_1482256.jpeg "购买ecs服务器1.jpg")

![购买ecs服务器2](https://images.gitee.com/uploads/images/2020/1207/144400_5d22e07d_1482256.jpeg "购买ecs服务器2.jpg")

下一步，按红色框里的配置网络及公网IP，其中公网宽带选择 **按流量计费** ，其他配置可能会造成更多代金券的浪费。（ _这里会分配公网IP和虚拟私有云VPC，这两项都会收费的哦，可使用代金券Modelarts抵扣_ ）

![配置公网ip](https://images.gitee.com/uploads/images/2021/0114/195447_71bc369b_1482256.png "配置公网ip.png")

继续下一步，**云服务器名称**自己随意取想要的关系不大，比如此处我取的是 ecs-zhonglin。下面的密码必须填写且必须记住（**重点**），因为后面登入ECS服务器需要用到这个密码。 最后流程选择购买，如下图我选的配置价格是1.238/hours，这个一样可以使用代金券购买抵扣。

![购买ecs服务器](https://images.gitee.com/uploads/images/2021/0114/194903_7d9b9471_1482256.png "购买ecs服务器.png")

然后，继续进入[ECS网页](https://console.huaweicloud.com/ecm/?agencyId=0703e2ce96800ff21f35c01d3901ff44&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)界面中的 **云服务器控制台**--->**弹性云服务器**，可以看到刚刚购买的ECS服务器，可以对其开机并远程登入进行操作。

![购买ECS后的界面](https://images.gitee.com/uploads/images/2020/1207/144805_ed5cfa4a_1482256.jpeg "购买ECS后的界面.jpg")

![ECS开机界面](https://images.gitee.com/uploads/images/2020/1207/144855_6e8d16f8_1482256.jpeg "ECS开机界面.jpg")

![远程登入ECS界面](https://images.gitee.com/uploads/images/2020/1207/144836_436d2399_1482256.jpeg "远程登入ECS界面.jpg")

开机后，F5刷新后点击远程登入，进入下面的界面中，输入上述自己设定的密码，进入的ECS服务器中。（这里也可以使用其他ssh软件，通过分配的ip登入）

![cshell登入界面](https://images.gitee.com/uploads/images/2020/1207/144916_f49c5811_1482256.jpeg "cshell登入界面.jpg")

![cshell登入后的界面](https://images.gitee.com/uploads/images/2020/1207/144931_b67ff459_1482256.jpeg "cshell登入后的界面.jpg")

注意，你可以可以使用其他的ssh软件登入，因为我们知道了大网IP、用户名以及自己设定的密码，端口号port默认为22。如下图，使用[MobaXterm](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/software/MobaXterm.rar)登入到了ECS服务器中。

![mobaxterm登入的界面](https://images.gitee.com/uploads/images/2020/1207/144959_d92822cd_1482256.jpeg "mobaxterm登入的界面.jpg")

登入进入后，在ECS上面通过下面的命令安装docker环境，方便后面制作镜像使用。【如果你本地或者ECS服务器中安装过了，这一步可以跳过】
```
卸载旧的版本

sudo apt-get remove docker docker-engine docker-ce docker.io

更新apt包索引

sudo apt-get update

安装以下包以使apt可以通过HTTPS使用存储库repository

sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

添加Docker官方的GPG密钥

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

使用下面的命令来设置stable存储库

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

再更新一下apt包索引

sudo apt-get update

安装最新版本的Docker

sudo apt-get install -y docker-ce 或 sudo apt-get install -y docker.io
```

## <span id="公共镜像的制作">公共镜像的制作</span>
  :exclamation: _这一节，如果需要基于新的软件包制作公共镜像才涉及；如果基于提供的公共的镜像，只是希望替换某些 py 或者 so 文件不涉及，可忽略本小结。_ 

为什么拿到了modelarts的基础镜像，我们还要制作新公共镜像呢？因为Modelarts已安装的软件包跟社区上的可能略有滞后，我们希望用社区上提供的最新的软件包制作公共镜像。社区上最新的包如何获取，可以参看准备工作小结。

1. 登入到[容器镜像服务SWR](https://console.huaweicloud.com/swr/?region=cn-north-4#/app/dashboard)，基于基础镜像制作新的镜像作为公共镜像。

   选择界面上的**北京4 区域**，获取登入指令并登入到SWR中。

   ![获取swr登入指令](https://images.gitee.com/uploads/images/2020/1207/145117_0f378780_1482256.jpeg "获取swr登入指令.jpg")

   登入进入后，就可以进行docker image的pull和push操作了。值得注意的是，由于swr中存在组织的概念，手机号注册的账号（非IAM子账号），目前只能通过别人共享私有镜像的方式pull别人的镜像。我们获取别人共享的镜像地址后，可以通过docker下面的指令拉取共享的镜像。

   使用modelarts共享的基础镜像地址:

   `swr.cn-north-4.myhuaweicloud.com/modelarts-job-dev-image/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8`

   使用下面的docker指令拉取共享的镜像：

    `docker pull swr.cn-north-4.myhuaweicloud.com/modelarts-job-dev-image/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8`

    通过docker pull命令拉取共享的镜像到ECS服务器上，然后基于该镜像进行修改后commit保存，并再push到swr容器服务中（这种方法没有充分利用Dockerfile的分层结构，再次不详述和不推荐）。我们还可以通过Dockerfile的方式，使用该基础镜像进行修改。Dockerfile的介绍该文档不详细阐述，可以通过[Docker官方链接](https://docs.docker.com/engine/reference/builder/)进行学习。
    
    [Gitee链接](https://gitee.com/echo_lin/docker_images)中给出了一个示例Dockerfile文件，截图如下。在基础镜像中(`swr.cn-north-4.myhuaweicloud.com/modelarts-job-dev-image/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8`)，基本的环境都安装了，但里面的Ascend包和驱动包可能不是最新的，我们对里面已安装的进行了卸载，使用准备工作中取的软件包(*.run)进行了再安装。  **（注意Dockerfile最后一行的内容，默认目录必须切换到/home/work中）** 

    ![基础镜像Dockerfile截图](https://images.gitee.com/uploads/images/2020/1207/145222_6aac26a2_1482256.jpeg "基础镜像Dockerfile截图.jpg")

    在ECS服务器中，我们首先可以写一个需要的Dockerfile文件，然后将它制作镜像文件，最后将已经生成好的docker 镜像push到容器镜像服务swr中。该如何操作呢？下面我们具体讲解一下。

    - 首先，我将需要用到的软件包或者py文件通过ftp方式上传到ECS服务器中，如下截图是我当前需要的包和Dockerfile文件，里面的包是在准备工作中下载得到的。如果不需要卸载已有的基础镜像包，我们是不需要这些新的run包，只需要Dockerfile和需要替换的py或so文件。

      ![制作镜像需要的文件](https://images.gitee.com/uploads/images/2020/1207/145313_2af42fc9_1482256.jpeg "制作镜像需要的文件.jpg")

    - 然后，基于上面截图的Dockerfile文件可以进行镜像文件的制作。注意，为了后续方便将制作好的镜像文件push到事先在[容器镜像服务SWR](https://console.huaweicloud.com/swr/?region=cn-north-4#/app/dashboard)创建组织ascend-modelzoo中，在这假设镜像名字及tag名字为`swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_base`。这个镜像名字不是随意取得，它有一定的规则，参照[链接](https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0084.html)。*[镜像仓库地址]/[组织名称]/[镜像名称:版本名称]*
     
       如果你也是在北京4，那镜像仓库地址为swr.cn-north-4.myhuaweicloud.com；组织名称需要在[镜像仓库地址](https://console.huaweicloud.com/swr/?region=cn-north-4#/app/dashboard)中事先建立好，比如这里创建的为ascend-modelzoo。镜像名称:版本名称自己随意取，能够清楚回忆起该image的用途即可。接下来直接执行`docker build -f Dokcerfile -t [镜像仓库地址]/[组织名称]/[镜像名称:版本名称] .` 构建镜像。下面是我基于上面的Dockerfile文件构建镜像的指令：
    
      `docker build –f Dockerfile –t swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_base .`

      ![swr中创建组织](https://images.gitee.com/uploads/images/2020/1207/145416_204aa3a6_1482256.jpeg "swr中创建组织.jpg")

      ![制作镜像build命令](https://images.gitee.com/uploads/images/2020/1207/145522_2f9e8e66_1482256.jpeg "制作镜像build命令.jpg")

    - 最后，制作完成后可以push到[容器镜像服务SWR](https://console.huaweicloud.com/swr/?region=cn-north-4#/app/dashboard)中，但在push之前，我们需要获取容器登入指令，截图如下:

      ![获取swr登入指令2](https://images.gitee.com/uploads/images/2020/1207/145655_bdc65722_1482256.jpeg "获取swr登入指令2.jpg")

      ![登入到swr](https://images.gitee.com/uploads/images/2020/1207/145729_c65ea263_1482256.jpeg "登入到swr.jpg")

      向swr中推送刚刚build构建的镜像，指令参看如下：

      `docker push swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_base`

      由于当前镜像之前的Layer层已经在容器镜像服务SWR中存在了，所以下图看到很多打印"Layer already exists"。

      ![dockerbuild打印](https://images.gitee.com/uploads/images/2020/1207/145811_c6bd82fd_1482256.jpeg "dockerbuild打印.jpg")

      另外，SWR中的镜像可以设置为公开共享，别人就可以docker pull这个镜像。

      ![共享镜像1](https://images.gitee.com/uploads/images/2020/1207/145910_824b1510_1482256.jpeg "共享镜像1.jpg")

      ![共享镜像2](https://images.gitee.com/uploads/images/2020/1207/145919_992fc182_1482256.jpeg "共享镜像2.jpg")

      ![共享镜像3](https://images.gitee.com/uploads/images/2020/1207/150000_c1490e5e_1482256.jpeg "共享镜像3.jpg")

      OK，万里长征，我们完了主要的部分！前面主要描述了如何基于最开始的基础镜像，通过卸载里面已有老的软件包，再安装新的软包，并发布共享出去给大家使用的全过程。

## <span id="自定义镜像的制作">自定义镜像的制作</span>
  :exclamation: _如果我们希望在公共镜像中替换某些 py 或者 so 文件，其实我们不需要下载 run 包，再次重新制作一份。我们可以基于提供的公共镜像在上面自定义制作_。 目前公开的公共镜像SWR_URL地址在[ **这篇Wiki** ](https://gitee.com/ascend/modelzoo/wikis/Modelarts%E5%85%AC%E5%BC%80%E9%95%9C%E5%83%8F%E5%88%97%E8%A1%A8%E4%BF%A1%E6%81%AF?sort_id=3413562)中已经给出。

如果社区里提供的软件包里面修复了某些bug问题，比如某个py文件修复了某个bug，或者某个动态库so文件做了修复。我们如何基于公开的镜像再次重新定制化的制作我们需要的自定义镜像呢。同样的，首先，我们需要[**申请ECS服务器**](#申请ECS服务器)并在上面制作，如果你之前申请过可直接跳过；然后拿到Modelarts环境上的公共镜像（这个主要是华为方提供，如果社区人员自己制作的话，需要参看准备工作内容，下载所有需要的*.run文件），基于提供的公共镜像进行二次制作，这里同样涉及到写新的Dockerfile文件，但是都是比较简单的替换某些py或so文件操作。下面举例的这个Dockerfile，它的功能是基于某个公共镜像，在路径/usr/local/Ascend/nnae/latest/fwkacllib/python/site-packages/te/lang/cce/te_compute下替换conv3d_backprop_filter_compute.py文件。**（注意Dockerfile最后一行的内容，默认目录必须切换到/home/work中）**

![输入图片说明](https://images.gitee.com/uploads/images/2021/0118/171708_ba32453d_1482256.png "屏幕截图.png")

写完需要的Dockerfile文件后，我们同样可以像公共镜像一样，使用命令`docker build -f Dockerfile -t [镜像仓库地址]/[组织名称]/[镜像名称:版本名称] .`（注意命令最后有点号），来构建Build自己的镜像。如果你也是在北京4，那镜像仓库地址为 swr.cn-north-4.myhuaweicloud.com；组织名称，需要在[容器镜像服务SWR](https://console.huaweicloud.com/swr/?region=cn-north-4#/app/dashboard)中创建 _（这步很重要，不然下一步push会失败哦）_，然后push到自己的刚刚创建的组织中使用。此示例中假设自定义的镜像名字为

`swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_econet`

构建此自定义镜像的命令为

`docker build -f Dockerfile -t swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_econet .`

向swr中推送刚刚build构建的镜像，指令参看如下：

`docker push swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_econet`

好！上面我们已经通过手写Dockerfile，基于最新的基础镜像和修复文件构建需要的自定义镜像。

 **知识Tips** 
1.  **如何确定待替换文件所在路径** 
    这里可能有人会问我，拿到一个committer给的文件，比如上面提到的 conv3d_backprop_filter_compute.py 文件，我怎么知道它要新增到哪个路径下，或者需要去哪个路径下将旧的文件进行替换。
    - 首先，我们可以基于提供的公共镜像，也就是我们一直提到的 [**公共镜像列表**](https://gitee.com/ascend/modelzoo/wikis/Modelarts%E5%85%AC%E5%BC%80%E9%95%9C%E5%83%8F%E5%88%97%E8%A1%A8%E4%BF%A1%E6%81%AF?sort_id=3413562) ，假设这里我使用的是"swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_base", 创建一个容器container，这里假设该容器的名字为"nn"，执行命令如下：

    `docker run -itd --name nn swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_base /bin/bash`

    - 然后，start进入刚刚创建的容器 nn 中，命令如下：

      `docker start nn`

    - 最后，attach到刚刚创建的容器 nn 中，命令如下：
  
      `docker attach nn`

    现在，我们进入到了容器 nn 中，通过命令查看环境变量PYTHONPATH设置的情况 `env | grep PYTHONPATH`，可以看到环境变量指定在了 /usr/local/Ascend/。
    ![PYTHONPATH路径](https://images.gitee.com/uploads/images/2020/1211/170558_491ed373_1482256.png "屏幕截图.png")
    自然的，我们通过命令`cd /usr/local/Ascend/`切换到路径 /usr/local/Ascend/ 下。在当前路径下，我们通过命令`find . -name conv3d_backprop_filter_compute.py` 查找该文件所在位置，如下图。这样我们就获取得到了需要替换文件在镜像中的绝对路径。
    ![查找文件位置](https://images.gitee.com/uploads/images/2020/1211/164459_b072f704_1482256.png "屏幕截图.png")

2.  **验证自定义镜像的完整性** 
     _我们如何来确定该自定义镜像的完整性，或者通俗来讲，文件是否替换成功了呢？_ 
    解决办法就是：在ECS上我们可以基于刚创建的自定义镜像创建一个容器container，然后进入容器到对应文件路径下看看所替换的文件时间是否跟其他文件不一样。正常来说，被替换的文件或者新增的文件，它的时间跟其他文件是不一样的，而且是更新的。比如上面示例，被替换的文件conv3d_backprop_filter_compute.py，它在路径`/usr/local/Ascend/nnae/latest/fwkacllib/python/site-packages/te/lang/cce/te_compute`下。

    - 首先，我们基于刚刚build创建的镜像`swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_econet`，创建一个container，取名为econet，命令如下：
  
      `docker run -itd --name econet swr.cn-north-4.myhuaweicloud.com/ascend-modelzoo/roma-tensorflow-ascend910-c75-cp37-euleros2.8-aarch64-training:1.15.0-2.0.8_econet /bin/bash`

    - 然后，start进入刚刚创建的容器econet中，命令如下：

      `docker start econet`

    - 最后，attach到刚刚创建的容器econet中，命令如下：
  
      `docker attach econet`

    现在，我们进入到了容器econet中，到对应路径下看看被替换的文件信息。

    ```
    cd /usr/local/Ascend/nnae/latest/fwkacllib/python/site-packages/te/lang/cce/te_compute
    ls -al
    ```

    ![验证dockerfile](https://images.gitee.com/uploads/images/2020/1207/173250_fb995ca4_1482256.png "屏幕截图.png")

    从上面截图可以看到，我们通过Dockerfile自定义镜像方法来替换的算子py文件的确是替换成功了，可以安心的使用它解决问题。
好，到此为止，自定义镜像的工作就完了。接下来我们谈谈在Modelarts上借助Pycharm IDE如何使用自定义镜像。

## <span id="Pycharm上使用自定义镜像">Pycharm上使用自定义镜像</span>
我们还是回到Pycharm的Modelarts菜单栏配置界面中，使用自定义镜像时选择 **Custom** 而非 **Frequently-used**，如下示意图。

![pycharm配置界面](https://images.gitee.com/uploads/images/2020/1207/150150_e0aea2f5_1482256.jpeg "pycharm配置界面.jpg")

- 首先，我们对配置界面上的参数做详细介绍：

    1. Image path，填自己push上去的自定义镜像URL地址，如下图中把截图中的”docker pull”去掉，保留后面的字符串即是，或者别人公开的自定义镜像URL地址。留意一下，这里的镜像必须在自己权限可访问的组织里或者当前镜像是公开的，否则创建任务时会失败报"permission deny"错误。你可以在SWR中可查看，当前你是否在这个组织中。

          ![获取镜像地址](https://images.gitee.com/uploads/images/2020/1207/150242_195ce264_1482256.jpeg "获取镜像地址.jpg")

    2. Boot Command，我们就需要**额外注意**了。它的命令要求比较苛刻，输入命令的模板为：

         **/bin/bash run_train.sh 训练代码在obs上的s3协议字符串路径 训练启动文件  '/tmp/log/demo.log'  --超参** 

        下面我以一个具体示例阐述。*（针对这么苛刻的命令，目前已经给Modelarts提工单需求了，后面如果有修复会及时在这个wiki上更新）*

        比如，截图示例中，Boot Command我输入了`/bin/bash run_train.sh 's3://zhonglin-demo/econet/' 'econet/run_modelarts.py'  '/tmp/log/demo.log'`。
        - `/bin/bash run_train.sh`是固定的不需要修改，照搬过来
        - `'s3://zhonglin-demo/econet/'`换成你需要调试代码在OBS上的代码目录(注意这里是OBS代码目录，不是物理机上pycharm工程下的代码目录，所以每次更新本地代码，都需要将代码更新到obs上)。仔细的你可能留意到了，输入命令中的"训练代码在obs上的s3协议字符串路径"，就是 _**训练代码在obs上的文件夹路径**_。 _注意，这里有一个小细节，该路径是s3:开头，不是obs:开头_ 。
        - `'econet/run_modelarts.py'`为启动文件，注意，这里需要填写OBS路径下启动文件的父文件夹和启动文件，比如 *父文件夹/启动文件* 这种格式。下面是我在obs上存放econet的代码路径截图，其中run_modelarts.py是启动文件。大家可以参照下设置下自己训练代码对应的参数。 仔细的你可能又留意到了，输入命令中的"训练启动文件"格式，就是  **_训练代码所在文件夹/启动文件_**  

          ![bootcommand上的obs路径](https://images.gitee.com/uploads/images/2020/1207/150334_a8b32184_1482256.jpeg "bootcommand上的obs路径.jpg")

        - '/tmp/log/demo.log'这个需要设置，目前可以固定为/tmp/log/xxx.log，它会把脚本的日志输出到里面。 
        - 超参 这里可选，你可以不放在这里，而是放到里面的启动文件中配置。
    3. Code Directory：如果是使用自定义镜像，这里不需要设置。空着即可。 
    4. Obs Path:填写OBS上的路径，用于存放输出数据和日志，但不需要"obs:/"抬头字段
    5. Data Path in OBS：填写训练数据在OBS上的路径，但不需要"obs:/"抬头字段
    6. Specifications: 选择 **CPU:24vCPUs96GiB** 
    7. Compute Nodes: 选择 1。 目前一般账户只能支持单节点训练任务

- 其次，配置完参数后，代码中的启动文件的路径也要做少部分修改，不然会出现找不到启动文件。如何修改路径呢？
 **先说结论** ：如果run.sh或者py文件中涉及代码全路径的，都按格式  _/home/work/user-job-dir/TrainCode/xxx_  设置。其中TrainCode是训练代码在obs的归属文件夹名字，xxx是训练代码中的某个文件。
     - **原理** ：在Modelarts上，使用自定义镜像运行时，首先Modelarts会根据你 Boot command 配置的参数 "训练代码在obs上的s3协议字符串路径" ，将整个文件夹上传到 **/home/work/user-job-dir/** 这个路径下。比如如果我的模型训练代码在obs上的路径为 _s3://zhonglin-demo/econet/_ ，那么econet整个文件夹将会上传到 _/home/work/user-job-dir/_ 路径下，最后我们的训练代码在Modelarts上的路径为 _/home/work/user-job-dir/econet_ 

- 最后，说到这里，大家应该都清楚了吧。比如如果我想在启动文件为 run_modelarts.py 中调用训练脚本中的run.sh文件，那么run_modelarts.py代码中调用该shell文件的方法就应该为 `os.system('sh /home/work/user-job-dir/econet/run.sh')`
其他在pycharm上使用自定义镜像的方法跟使用内置镜像模板的方法一样，可以参看[Modelarts上使用NPU环境训练示例](https://gitee.com/ascend/modelzoo/wikis/ModelArts%E4%B8%8A%E4%BD%BF%E7%94%A8NPU%E7%8E%AF%E5%A2%83%E8%AE%AD%E7%BB%83%E7%A4%BA%E4%BE%8B?sort_id=3155028)，链接里面有描述如何使用。

## <span id="其他">其他</span>
1. 日志问题，参看[链接](https://gitee.com/ascend/modelzoo/wikis/%E6%97%A5%E5%BF%97%EF%BC%8C%E6%97%A5%E5%BF%97%EF%BC%81?sort_id=3157963)。
2. Pycharm上如何使用内置镜像进行模型训练，请参看[链接](https://gitee.com/ascend/modelzoo/wikis/ModelArts%E4%B8%8A%E4%BD%BF%E7%94%A8NPU%E7%8E%AF%E5%A2%83%E8%AE%AD%E7%BB%83%E7%A4%BA%E4%BE%8B?sort_id=3155028)。
3. 如果在提供的共享镜像中发现缺少依赖包，可以在Dockerfile里先进行安装好，把它们直接打入镜像文件中。
比如你可以在Dockerfile文件里执行pip和apt-get安装命令，安装你需要的boto3/sklearn/progress和build-essential，apt-utils
   ```
   RUN pip --no-cache-dir install boto3==1.7.29 sklearn progress
   RUN apt-get install build-essential apt-utils
   ```
    如果pip安装时发现找不到对应的package，你可以试着使用提供的备选pip代理/root/.pip/pip.conf~，将原来的/root/.pip/pip.conf替换掉。

    `RUN cp /root/.pip/pip.conf~  /root/.pip/pip.conf`

    或者将自己上传到ECS服务器路径上的pip.conf文件拷贝到镜像/root/.pip/路径下

    `COPY pip.conf  /root/.pip/`









