## Modelarts采集Profiling性能数据(基于CANN20.2)

本案例针对Modelarts云上训练环境，如果是裸机或者依瞳环境，请忽略本wiki，参考[Profiling资料](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0003.html)操作


### 1.准备工作
开始采集前，训练脚本应该按照指导修改，可以在NPU上正常运行训练，找到需要采集性能数据的前向和反向节点名称（即fp_point和bp_point）。
方法可以参考老谭的指导：
[Apulis环境采集Profiling性能](https://gitee.com/ascend/modelzoo/wikis/%E8%AE%AD%E7%BB%83%E6%80%A7%E8%83%BD%E5%A4%AA%E5%B7%AE%E4%BA%86%EF%BC%8C%E6%9C%89%E4%BB%80%E4%B9%88%E5%A5%BD%E5%8A%9E%E6%B3%95%E5%90%97%EF%BC%9F%EF%BC%88Apulis%E7%89%88%EF%BC%89?sort_id=3148795)


### 2.修改脚本
#### 2.1 SessionRun方式
通过session配置项profiling_mode、profiling_options开启Profiling数据采集，代码示例如下：
```
custom_op.parameter_map["profiling_mode"].b = True
custom_op.parameter_map["profiling_options"].s = tf.compat.as_bytes('{"output":"/cache/profiling","task_trace":"on"}')
custom_op.parameter_map["fp_point"].s = tf.compat.as_bytes("gnn/graphlayer_1_vars/random_uniform/RandomUniform")
custom_op.parameter_map["bp_point"].s = tf.compat.as_bytes("gradients/Fill")
```
#### 2.2 Estimator方式
Estimator模式下，通过NPURunConfig中的profiling_config开启Profiling数据采集，代码示例如下：
```
from npu_bridge.estimator.npu.npu_config import NPURunConfig
from npu_bridge.estimator.npu.npu_config import ProfilingConfig
profiling_options = '{"output":"/cache/profiling","training_trace":"on","fp_point":"gnn/graphlayer_1_vars/random_uniform/RandomUniform","bp_point":"gradients/Fill"}'
profiling_config = ProfilingConfig(enable_profiling=True, profiling_options = profiling_options)
session_config=tf.ConfigProto()
config = NPURunConfig(profiling_config=profiling_config, session_config=session_config)

```

#### 3.创建Profiling目录
在训练脚本中，调用os，创建目录：
```
import os
os.mkdir("/cache/profiling")
```

#### 4.将容器内采集到的的Profiling数据传输至OBS桶
在训练脚本中，调用moxing接口：
```
import moxing as mox
mox.file.copy_parallel("/cache/profiling", 's3://xxxxx/')
```

修改完成之后启动在ModelArts上的训练，就可以在OBS桶里看到我们采集的Profiling数据：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/114837_513f6c12_7866623.png "屏幕截图.png")

#### 5.解析Profiling数据
由于ModelArts不支持解析数据，因此我们需要把数据拷贝到开发环境上，执行解析，操作步骤和方法可以参考文档：
[解析Profiling数据](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0015.html)

或者可以参考老谭的分享：
[解析Profiling数据](https://gitee.com/ascend/modelzoo/wikis/%E8%AE%AD%E7%BB%83%E6%80%A7%E8%83%BD%E5%A4%AA%E5%B7%AE%E4%BA%86%EF%BC%8C%E6%9C%89%E4%BB%80%E4%B9%88%E5%A5%BD%E5%8A%9E%E6%B3%95%E5%90%97%EF%BC%9F%EF%BC%88Apulis%E7%89%88%EF%BC%89?sort_id=3148795)