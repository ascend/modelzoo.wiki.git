## Modelarts训练方式汇总<a name="section1358541031613"></a>
目前在Modelarts提供了三种方式供开发者执行训练，第一种是直接在web界面上提供训练作业的方式；第二种是利用Pycharm，并使用Modelarts插件方式；第三种是使用Modelarts提供的notebook开发环境。

下面我先说说对前两种训练方式的理解，第三种方式待后续深入研究后再提供。

1. Modelarts上的web界面

   这种方式如果事先在NPU上调试debug打通后基本没问题后，在web上部署便可直接训练。如果前期训练代码没有debug好，直接用这种方式不太友好。Gitee modelzoo下的wiki提供了一个示例指导文档供大家参考学习：[华为云ModelArts上跑通alexnet训练脚本（TensorFlow）](https://gitee.com/ascend/modelzoo/wikis/%E5%8D%8E%E4%B8%BA%E4%BA%91ModelArts%E4%B8%8A%E8%B7%91%E9%80%9Aalexnet%E8%AE%AD%E7%BB%83%E8%84%9A%E6%9C%AC%EF%BC%88TensorFlow%EF%BC%89?sort_id=3093453)，在此就不再举例。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/191650_1a5e8ffb_8267113.png "zh-cn_image_0295927772.png")

2. Pycharm + Modelarts插件方式

   首先要在window下安装一个pycharm软件和下载Pycharm toolkit插件，参看 [Modelarts官方链接](http://support.huaweicloud.com/tg-modelarts/modelarts\_15\_0001.html)，我在OBS上共享了[IDE Pycharm ](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/software/pycharm-community-2020.2.3.exe)和 [pycharm toolkit插件](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/software/Pycharm-ToolKit-2.1.zip)供大家快速下载。同时，官网上提供了一个基于Pycharm + Modelarts的 [指导示例](https://support.huaweicloud.com/bestpractice-modelarts/modelarts_10_0021.html)，但里面的例子是基于GPU环境，使用的是MXNet训练框架。接下来的内容，我主要讲解如何在Modelarts上，基于Ascend NPU引擎使用Tensorflow训练框架跑通AlexNet网络训练示例代码。

## Pycharm+Modelarts实现网络训练和部署<a name="section1358541031610"></a>
-  首先，环境准备工作。包括注册华为云账号，在 **华北北京-4区域** 下创建OBS桶（建议下载使用[OBS Browser+](https://console.huaweicloud.com/console/?region=cn-north-4#/obs/manager/buckets)），安装Pycharm IDE。然后需要安装PyCharm ToolKit，并添加访问密钥。这个不区分NPU还是GPU，可以参考[链接](https://support.huaweicloud.com/bestpractice-modelarts/modelarts_10_0021.html)里的 **步骤一** 。
- 其次，数据集准备。AlexNet网络需要的数据集已公开供大家获取，下载[链接](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/dataset/Flowers-Data-Set.zip)。下载之后，需要本地解压，并将整个文件夹上传到自己的OBS桶上。
    ![Flowers数据集](https://images.gitee.com/uploads/images/2021/0120/112744_7453ae12_1482256.png "Flowers数据集.png")

- 然后，训练代码迁移&适配。在迁移之前，需要确保Tensorflow的训练代码在GPU或者CPU下可以执行，并且完成NPU的代码迁移适配，也就是需要完成这个[链接](https://bbs.huaweicloud.com/forum/thread-83283-1-1.html)里面的Step2。这一块的适配代码，我已经在示例代码中完成了供大家[下载](https://gitee.com/echo_lin/alexnet)。
- 最后，Modelarts上训练部署。在Pycharm工具栏中，选择"ModelArts > Edit Training Job Configuration"，配置插件配置参数。如果使用Modelarts上的常用(Frequently-used)框架/镜像，配置参数详见如下表格；如果使用Modelarts上自定义(Custom)框架/镜像，参数配置详见[另外一篇Wiki](https://gitee.com/ascend/modelzoo/wikis/ModelArts%E8%87%AA%E5%AE%9A%E4%B9%89NPU%E8%AE%AD%E7%BB%83%E7%8E%AF%E5%A2%83%E9%95%9C%E5%83%8F%E6%89%8B%E5%86%8C%E3%80%90%E5%9F%BA%E7%A1%80%E7%89%88%E3%80%91?sort_id=3205360) 中的 **Pycharm上使用自定义镜像** 章节。
    |参数   | 数值及说明 |
    |---------|---------|
    |  Job Name | 自动生成，首次提交训练作业时，该名称也可以自己指定 |
    | Algorithm Source | Frequently-used。如果是Custom，配置参数略有差异 |
    |  AI Engine | **Asend-Powered-Engine,  TF-1.15-python3.7-aarch64** |
    |  Boot File Path | 选择本地的训练启动**Python**脚本 |
    |  Code Directory | 选择训练启动脚本所在的目录 |
    |  OBS Path | 输出路径(train_url)，用于存储训练输出模型和日志文件。 **路径需去除开头的 "obs:/"部分**  |
    |  Data Path in OBS | 训练数据在OBS上的路径(data_url)。  **路径需去除开头的 "obs:/"部分**  |
    |  Specifications | 规格，CPU:24vCPUs 96GiB |
    |  Compute Nodes | 训练节点个数，选 1 |
    |  Running Parameters | 其他超参，用分号隔开。比如 batchsize=4;learning_rate=0.01 |

    ![配置参数](https://images.gitee.com/uploads/images/2021/0209/114703_9bd778ff_1482256.png "配置参数.png")

    需要注意的是：

    1. 如果想用NPU进行Tensorflow代码训练，那么AI Engine 中必须填写  **Ascend-Powered-Engine**  和 **TF-1.15-python3.7-aarch64** 

    2. "OBS Path"是obs上某个文件夹的路径，用于存放训练输出模型和日志文件。比如我的名下有一个名字为"linccnu"的桶，并希望输出模型和日志存储在下面已经创建的log文件夹中。我们就将该log在obs上的路径复制过来，**并且去除开头的"obs:/"部分**，OBS Path中填 **/linccnu/log**

        ![log日志](https://images.gitee.com/uploads/images/2021/0117/214343_08416265_1482256.png "log日志.png")

    3. "Data Path in OBS"是数据准备阶段存放的模型训练需要的OBS全路径，比如我在obs存放的示例训练数据集截图如下。**注意**，不是每个网络的训练数据集都是按 train 和 val 划分的，此处只是讲解如何配置"Data Path in OBS"参数路径。
       ![训练数据集](https://images.gitee.com/uploads/images/2021/0117/214645_cd077af9_1482256.png "训练数据集.png")

        那么在"Data Path in OBS" 我填写**/linccnu/dataset/Flowers-Data-Set/**。注意没有“**obs:/**”打头的字段。另外，里面的数据可以是原始的jpeg图片，也可以是离线转好的tfrecords数据。如果图片数据量很大，建议害是tfrecords数据，因为小文件在OBS传输时比较费时；同时，在模型训练时，可以分batch将训练数据加载进内存中，否则容易撑爆内存。

## 其他<a name="section7271512256"></a>
1. Modelarts的运行机理
   
   Modelarts每启动一个任务，会根据选择的AI Engine配置，创建一个全新的Docker容器，当训练结束或者异常时，会自动销毁该容器和释放占用的NPU资源，并删除上面的代码和数据。
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/192306_80158e80_8267113.png "zh-cn_image_0295927369.png")
2. 关于OBS
  
    **Obs\(Object Storage Service\)对象存储服务是s3协议，我们该路径不能直接在训练代码中使用**，需要使用moxing的接口mox.file.copy\_parallel\([https://support.huaweicloud.com/moxing-devg-modelarts/modelarts\_11\_0005.html](https://support.huaweicloud.com/moxing-devg-modelarts/modelarts_11_0005.html)\)将训练数据从obs文件夹中拷贝到modelarts任务容器中。另外，modelarts创建的NPU模板容器，ModelArts会挂载硬盘至“/cache”目录，用户可以使用此目录来储存临时文件。“/cache”与代码目录共用资源，不同资源规格有不同的容量。其中ascend NPU下具有3T的容量大小。https://support.huaweicloud.com/modelarts\_faq/modelarts\_05\_0090.html

3. 数据拷贝
   
    那问题来了，我们该如何实现将obs上的训练数据传到Modelarts中呢？官方给了一个[示例代码](https://support.huaweicloud.com/modelarts_faq/modelarts_05_0114.html)，如下：

    ```
    ...
    tf.flags.DEFINE_string('data_url', '', 'dataset directory.')
    FLAGS = tf.flags.FLAGS
    import moxing as mox
    TMP_CACHE_PATH = '/cache/data'
    mox.file.copy_parallel('FLAGS.data_url', TMP_CACHE_PATH)
    mnist = input_data.read_data_sets(TMP_CACHE_PATH, one_hot=True)
    ```

    那反过来呢，如何将容器中的数据传到obs上呢？再解决这个问题之前，我们考虑一个应用场景，比如如果我们需要debug算子精度问题，希望得到dump数据，那我们如何得到dump数据，并把dump数据传到obs上。

    首先，我们需要在shell脚本中export环境变量export DUMP\_GE\_GRAPH=2，然后执行运行启动训练文件test\_sh.py。任务启动后，会在**os.getcwd**路径下会生成若干”.pbtxt”和“.txt”dump文件。因为dump的数据文件较多且文件都较大，若非必要，请“\#”号注释掉这个环境变量。

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0209/114914_23455117_1482256.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0209/115009_68873c88_1482256.png "屏幕截图.png")

    然后，针对某个文件夹，我们可以通过mox.file.copy\_parallel接口在Obs和Modelarts间互相拷贝传输文件夹；如果只是想传某个文件，可以通过mox.file.copy接口在obs和容器间互相拷贝传输文件。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0209/115124_796cab14_1482256.png "屏幕截图.png")

4. 数据拷贝性能问题

    从obs上传tfrecords训练数据到modelarts容器中，性能如何呢？比如上传10G甚至100G的耗时情况。我在本地实操了一遍，写了一个简单类似的代码如下。
    ```
    # copy dataset from obs to local
    start = datetime.datetime.now()
    print("===>>>Copy files from obs:{} to local dir:{}".format(config.data_url, config.cache_data_dir))
    mox.file.copy_parallel(src_url=config.data_url, dst_url=config.cache_data_dir)
    end = datetime.datetime.now()
    print("===>>>Copy from obs to local, time use:{}(s)".format((end - start).seconds))
    files = os.listdir(config.cache_data_dir)
    print("===>>>Files number:", len(files))
    ```

   通过上面这段代码，实测从OBS拉取如下截图的8.3G的数据到modelarts容器本地的耗时大概25s。更大的数据集耗时，比如100G的tfrecords，亲测大概要 3mins。注意，这里建议Copy大文件，比如 tfrecords，压缩包等。

   最后，使用pycharm+modelarts plugin插件提交训练任务后，在web界面上\(https://console.huaweicloud.com/modelarts/?region=cn-north-4\#/trainingJobs\)的“训练管理”—“训练作业”可以看到，刚刚提交的任务。注意，Pycharm IDE上，一次只能提交一个任务。当前普通华为云账户，在modelarts上只能在单个节点上训练。

   [https://console.huaweicloud.com/modelarts/?region=cn-north-4\#/trainingJobs](https://console.huaweicloud.com/modelarts/?region=cn-north-4#/trainingJobs)

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0117/221713_8b7e0520_1482256.png "屏幕截图.png")

   同时，更多详细的基于pycharm toolkit工具指南，可以参看链接[https://support.huaweicloud.com/tg-modelarts/modelarts\_15\_0007.html](https://support.huaweicloud.com/tg-modelarts/modelarts_15_0007.html)

5. 日志问题

   当前的模型的训练日志，可以通过IDE打屏，pycharm当前工程的文件夹MA\_LOG获取，甚至可以在配置界面上设置的log日志路径下获得。

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0117/221908_107fe5bc_1482256.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0117/222034_43161538_1482256.png "屏幕截图.png")

6. NPU利用率
当前网络是否下沉到昇腾Ascend910上训练，最直观的方法是在[ModelArts界面](https://console.huaweicloud.com/modelarts/?region=cn-north-4#/trainingJobs)上查看当前训练任务上的资源占用情况。如果NPU曲线的值不为0，那么肯定是下沉到了NPU上训练了。
![npu利用率](https://images.gitee.com/uploads/images/2021/0209/114309_f233454c_1482256.png "npu利用率.png")

## 示例Demo<a name="section06691523142711"></a>

- 以Alexnet为例，它的训练数据集为Flowers-Data-Set可以通过[**Obs链接**](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/dataset/Flowers-Data-Set.zip)获取，训练代码可通过[**Gitee链接**](https://gitee.com/echo_lin/alexnet)获取。