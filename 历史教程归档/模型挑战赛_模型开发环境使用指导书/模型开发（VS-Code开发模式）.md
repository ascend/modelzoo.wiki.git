# 模型开发（VS-Code开发模式）<a name="ZH-CN_TOPIC_0297797156"></a>

用户获取SSH信息后，也可通过配置使用VS Code进行远程链接调试，VS Code需安装Remote插件，如[图1](#fig1217864717263)\~[图6](#fig320719461279)进行配置及登录对应环境。

**图 1**  安装Remote插件<a name="fig1217864717263"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103005_33d415b0_8394326.png "安装Remote插件.png")

**图 2**  添加SSH信息<a name="fig32466480"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103022_446614be_8394326.png "添加SSH信息.png")

**图 3**  输入SSH信息<a name="fig12539246"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103052_03d3fc5c_8394326.png "输入SSH信息.png")

**图 4**  打开Remote SSH环境<a name="fig9045980"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103108_3c133b2c_8394326.png "打开Remote-SSH环境.png")

**图 5**  填写对应SSH登录密码<a name="fig61635746"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103130_a3531d28_8394326.png "填写对应SSH登录密码.png")

**图 6**  选择开发目录<a name="fig320719461279"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103146_628ff250_8394326.png "选择开发目录.png")
