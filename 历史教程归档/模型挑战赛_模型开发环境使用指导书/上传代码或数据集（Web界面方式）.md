# 上传代码或数据集（Web界面方式）<a name="ZH-CN_TOPIC_0297797152"></a>
-   [上传代码](#section21259511)
-   [上传数据集](#section57117878)

## 上传代码<a name="section21259511"></a>

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103042_d35fce12_8394326.gif "icon-note.gif")**说明：** 
>前提条件是：代码开发任务创建后，等待任务调度至“运行中”。

1.  用户可点击代码开发任务中的“上传”按钮，如[图1](#fig958128145917)所示。

    **图 1**  Web界面上传<a name="fig958128145917"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/101656_f4104a7b_8394326.png "Web界面上传.png")
2.  系统弹出如[图2](#fig1416183151015)所示页面，将代码工程相关文件上传至开发环境中。

    支持的文件类型包括：代码文件，zip压缩包和rar压缩包。

    **图 2**  上传代码<a name="fig1416183151015"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/101725_62b67b00_8394326.png "上传代码.png")

    上传后用户可在此前创建代码开发环境的代码路径中查看（如/home/admin/code/Resnet50\_HC，admin为登录用户名）。


## 上传数据集<a name="section57117878"></a>

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103136_9171046f_8394326.gif "icon-note.gif")**说明：** 
>前提条件是：代码开发任务创建后，等待任务调度至“运行中”。

对于数据集上传，用户可依次点击“数据管理 -\> 数据集管理 -\>新增数据集”，进行Web界面上传数据集，数据集支持zip、tar、tar.gz等几个格式文件进行上传，新增数据集配置信息可参考[表1](#table64340216)进行填写。

**图 3**  数据集上传<a name="fig45185659"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/101808_ca86fa97_8394326.png "数据集上传.png")
**表 1**  数据集上传配置说明

<a name="table64340216"></a>
<table><thead align="left"><tr id="row5212777"><th class="cellrowborder" valign="top" width="23.23%" id="mcps1.2.3.1.1"><p id="p19581784"><a name="p19581784"></a><a name="p19581784"></a>选项</p>
</th>
<th class="cellrowborder" valign="top" width="76.77000000000001%" id="mcps1.2.3.1.2"><p id="p42620696"><a name="p42620696"></a><a name="p42620696"></a>配置说明</p>
</th>
</tr>
</thead>
<tbody><tr id="row29724369"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p58863658"><a name="p58863658"></a><a name="p58863658"></a>数据集名称</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p3226955"><a name="p3226955"></a><a name="p3226955"></a>数据集名称</p>
</td>
</tr>
<tr id="row29042598"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p3640263"><a name="p3640263"></a><a name="p3640263"></a>简介</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p26425925"><a name="p26425925"></a><a name="p26425925"></a>数据集描述信息</p>
</td>
</tr>
<tr id="row36506738"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p4255814"><a name="p4255814"></a><a name="p4255814"></a>是否已标注</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p9176617"><a name="p9176617"></a><a name="p9176617"></a>数据是否已标注</p>
<p id="p1258133219266"><a name="p1258133219266"></a><a name="p1258133219266"></a>如已标注且需要在“模型训练”模块可选该数据集，需填写“是”。</p>
</td>
</tr>
<tr id="row15480697"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p45976953"><a name="p45976953"></a><a name="p45976953"></a>数据权限</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p33145693"><a name="p33145693"></a><a name="p33145693"></a>数据权限配置，为了保障数据安全，如通过Web界面上传，建议选择“私有”权限。</p>
<a name="ul29875784"></a><a name="ul29875784"></a><ul id="ul29875784"><li>私有：表示该数据集在平台上只给当前用户使用。</li><li>公有：表示该数据集在平台上可公开使用。</li></ul>
</td>
</tr>
<tr id="row36175091"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p44501236"><a name="p44501236"></a><a name="p44501236"></a>数据源</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p47830395"><a name="p47830395"></a><a name="p47830395"></a>数据的来源</p>
<a name="ul27820371"></a><a name="ul27820371"></a><ul id="ul27820371"><li>网页上传数据源：可选择本地数据集进行上传，支持zip、tar、tar.gz等压缩格式的数据集进行上传。</li><li>使用以其它方式上传的数据源：如用户通过ssh的方式上传数据集，可通过该方式对数据集进行关联，方便平台其它模块使用。</li></ul>
</td>
</tr>
<tr id="row14173748"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.2.3.1.1 "><p id="p7222968"><a name="p7222968"></a><a name="p7222968"></a>上传文件</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.2.3.1.2 "><p id="p48189536"><a name="p48189536"></a><a name="p48189536"></a>选择上传的数据集压缩文件</p>
</td>
</tr>
</tbody>
</table>

上传完成后，可以在“数据集管理”页面，单击数据集名称，查看数据集的存放路径。如[图4](#fig93501575509)所示，cyxx为当前登录用户名。

**图 4**  数据集存储路径<a name="fig93501575509"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/101830_8eefd115_8394326.png "数据集存储路径.png")

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103103_7e584720_8394326.gif "icon-caution.gif")**注意：** 
>如需上传较大的代码工程或数据集，建议SSH方式进行上传。

