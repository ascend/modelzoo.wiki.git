# NPU环境变量配置信息<a name="ZH-CN_TOPIC_0297797148"></a>

-   查看当前使用的device设备id号，如[图1](#fig30058148)所示，device\_id为3。

    **env | grep VIS**：查看当前可使用设备号，默认由平台分配

    **图 1**  查看当前任务使用的device id号<a name="fig30058148"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/105309_92ab2ed3_8394326.png "查看当前任务使用的device-id号.png")


-   设置HCCL通信时间（“\~/.npu/$DLWS\_JOB\_ID/train.sh”中配置）。

    ```
    export HCCL_CONNECT_TIMEOUT=200
    ```

-   NPU驱动日志输出路径为 /var/log/npulog/slog，

    host-0代表该节点所有NPU的驱动报错日志；

    device-n代表第n张NPU卡的驱动报错日志；

    device-os-0代表第0-3张NPU卡的驱动报错日志；

    device-os-4代表第4-7张NPU卡的驱动报错日志。

-   NPU驱动日志打印到标准输出（“\~/.npu/$DLWS\_JOB\_ID/train.sh”中配置）。

    ```
    export SLOG_PRINT_TO_STDOUT=1
    #设置此变量会默认以debug模式输出日志，NPU驱动不会再写入log日志到/var/log/npulog/slog下
    ```

-   设置全局日志级别。
    -   Tensorflow的日志控制环境变量（“\~/.npu/$DLWS\_JOB\_ID/train.sh”中配置）

        ```
        export GLOBAL_LOG_LEVEL=3 
        export TF_CPP_MIN_LOG_LEVEL=3
        export SLOG_PRINT_TO_STDOUT=0
        
        GLOBAL_LOG_LEVEL取值范围说明如下： 
         0：对应DEBUG级别。 
         1：对应INFO级别。 
         2：对应WARNING级别。 
         3：对应ERROR级别。 
         4：对应NULL级别。
        ```

    -   Mindspore的日志控制环境变量（“/var/log/npu/conf/slog/slog.conf”中配置）

        ```
        #note, 0:debug, 1:info, 2:warning, 3:error, 4:null(no output log), default(3)
        global_level=3
        # Event Type Log Flag, 0:disable, 1:enable, default(1)
        enableEvent=0
        ```

    -   日志文档等级可介绍：[日志参考](https://support.huawei.com/enterprise/zh/doc/EDOC1100180794?idPath=23710424%7C251366513%7C22892968%7C251168373)


