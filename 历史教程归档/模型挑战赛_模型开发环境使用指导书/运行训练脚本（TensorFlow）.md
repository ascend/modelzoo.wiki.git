# 运行训练脚本（TensorFlow）<a name="ZH-CN_TOPIC_0297797145"></a>

-   [前提条件](#section202244492565)
-   [操作步骤](#section75699591607)

## 前提条件<a name="section202244492565"></a>

-   已经准备好适配TensorFlow的脚本。
-   已经创建好开发环境，具体操作请参考[创建开发环境](https://gitee.com/ascend/modelzoo/wikis/%E5%88%9B%E5%BB%BA%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83?sort_id=3460973)。
-   已经将代码/数据集上传至开发环境中，具体操作请参考[上传代码或数据集（Web界面方式）](https://gitee.com/ascend/modelzoo/wikis/%E4%B8%8A%E4%BC%A0%E4%BB%A3%E7%A0%81%E6%88%96%E6%95%B0%E6%8D%AE%E9%9B%86%EF%BC%88Web%E7%95%8C%E9%9D%A2%E6%96%B9%E5%BC%8F%EF%BC%89?sort_id=3460976)或者[上传代码或数据集（SSH方式）](https://gitee.com/ascend/modelzoo/wikis/%E4%B8%8A%E4%BC%A0%E4%BB%A3%E7%A0%81%E6%88%96%E6%95%B0%E6%8D%AE%E9%9B%86%EF%BC%88SSH%E6%96%B9%E5%BC%8F%EF%BC%89?sort_id=3460984)。

## 操作步骤<a name="section75699591607"></a>

本章节以一个已迁移好的适配TensorFlow框架的[Resnet50模型代码](https://www.huaweicloud.com/ascend/resources/modelzoo/Model%20Scripts/d3fe81f860f849578714b260a8068205)为例，介绍如何进行模型训练。

1.  打开Jupyter，请参考[模型开发（Jupyter-Notebook模式）](https://gitee.com/ascend/modelzoo/wikis/%E6%A8%A1%E5%9E%8B%E5%BC%80%E5%8F%91%EF%BC%88Jupyter-Notebook%E6%A8%A1%E5%BC%8F%EF%BC%89?sort_id=3460994)，启动一个Terminal，解压文件进入代码路径。

    ```
    cd ~/code
    unzip ModelZoo_Resnet50_HC.zip
    ```

2.  修改数据集路径\(data\_url\)。

    编辑 res50\_256bs\_1p.py配置文件，如[图1](#fig3121152)。

    ```
    vim ModelZoo_Resnet50_HC/00-access/src/configs/res50_256bs_1p.py
    ```

    **图 1**  修改数据集路径\(/home/\{username\}/dataset/tiny\_imagenet\)<a name="fig3121152"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103349_0da20b62_8394326.png "修改数据集路径(-home-username-dataset-tiny_imagenet).png")

3.  执行模型训练。

    **本次活动分配基本为每个用户只有一张NPU卡，在编辑完示例代码后直接执行如下命令即可拉起模型训练。**

    ```
    python ~/code/ModelZoo_Resnet50_HC/00-access/src/mains/res50.py --config_file=res50_256bs_1p --max_train_steps=1000 --iterations_per_loop=100  --debug=True --eval=False --model_dir=d_solution/ckpt${DEVICE_ID}
    ```

4.  训练结束后，进入模型输出路径\(model\_dir配置的输出路径\)，即可查看对应模型文件。

    ```
    cd d_solution
    ls -alt
    ```

    **图 2**  模型输出路径<a name="fig45666040"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103415_f7928c8c_8394326.png "模型输出路径.png")

    >![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103332_28a17862_8394326.gif "icon-note.gif")**说明：** 
    >示例命令中admin为当前登录用户，用户可根据当前登录用户名进行调整。

    -   data\_url ：为数据集路径，data\_url填写的为用户数据集保存的路径。可以通过[图4](Web界面上传.md#fig93501575509)进行查询。
    -   config\_file：为模型配置文件
    -   max\_train\_steps：为模型训练最大步长
    -   model\_dir：为模型输出路径
    -   在调试过程中，驱动错误会存储在“/var/log/npulog/slog/”下，通常系统报错放在host-0下，对应卡的报错在device-以及device-os-\*下，对应调用的是哪一张卡，就去哪一个目录去找对应的日志，如[图3](#fig4546697)所示。（device id可参考[图1](NPU环境变量配置信息.md#fig30058148)进行查询）。
    -   第一次训练执行完成后，如再次启动训练任务程序会自动加载之前的权重文件，建议把输出目录清空后再次进行训练。

        **图 3**  查看npu日志<a name="fig4546697"></a>  
        ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/103437_094ef821_8394326.png "查看npu日志.png")



