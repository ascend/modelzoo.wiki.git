# 上传代码或数据集（SSH方式）<a name="ZH-CN_TOPIC_0297797152"></a>

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103220_533a765c_8394326.gif "icon-note.gif")**说明：** 
>前提条件是：代码开发任务创建后，等待任务调度至“运行中”。

1.  在代码开发任务列表中点击对应任务的“SSH”按钮，如[图1](#fig47614557181)所示。

    **图 1**  获取代码开发docker环境SSH登录信息<a name="fig47614557181"></a>  
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/102306_b8d62530_8394326.png "获取代码开发docker环境SSH登录信息.png")

    系统会弹出SSH登录信息，左键点击弹出信息即可复制SSH信息。

2.  根据获取的SSH信息，使用WinSCP将代码工程或数据集上传至代码开发镜像环境内（如“/home/用户名/”路径下，用户名对应当前登录的用户名），如[图2](#fig1744427192115)所示配置WinSCP登录信息。

    -   文件协议：选择SCP
    -   端口：为获取SSH信息中对应的端口
    -   用户名：为当前登录用户名
    -   密码：目前固定为tryme2017

    **图 2**  WinSCP配置SSH登录信息<a name="fig1744427192115"></a>  
   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/102327_267a2877_8394326.png "WinSCP配置SSH登录信息.png")


