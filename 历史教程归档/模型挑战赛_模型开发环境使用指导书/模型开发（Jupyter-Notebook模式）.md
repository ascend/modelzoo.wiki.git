# 模型开发（Jupyter-Notebook模式）<a name="ZH-CN_TOPIC_0297797155"></a>

用户可在代码开发列表中，单击“Jupyter”按钮进入Notebook环境进行模型开发。

**图 1**  启动Jupyter<a name="fig41435040"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/102759_e77b72bd_8394326.png "启动Jupyter.png")

**图 2**  Jupyter Notebook交互式界面<a name="fig795116"></a>  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/102818_75407b53_8394326.png "Jupyter-Notebook交互式界面.png")

