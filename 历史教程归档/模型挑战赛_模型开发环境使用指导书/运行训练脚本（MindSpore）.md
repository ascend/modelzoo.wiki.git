# 运行训练脚本（MindSpore）<a name="ZH-CN_TOPIC_0297797146"></a>

-   [前提条件](#section202244492565)
-   [操作步骤](#section18172142116385)

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103400_d7a66dde_8394326.gif "icon-note.gif")**说明：** 
>该流程与[运行训练脚本（TensorFlow）](https://gitee.com/ascend/modelzoo/wikis/%E8%BF%90%E8%A1%8C%E8%AE%AD%E7%BB%83%E8%84%9A%E6%9C%AC%EF%BC%88TensorFlow%EF%BC%89?sort_id=3461035)类似，此处只针对差异进行介绍。

## 前提条件<a name="section202244492565"></a>

-   已经准备好适配MindSpore的脚本。
-   已经创建好开发环境，具体操作请参考[创建开发环境](https://gitee.com/ascend/modelzoo/wikis/%E5%88%9B%E5%BB%BA%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83?sort_id=3460973)。
-   已经将代码/数据集上传至开发环境中，具体操作请参考[上传代码或数据集（Web界面方式）](https://gitee.com/ascend/modelzoo/wikis/%E4%B8%8A%E4%BC%A0%E4%BB%A3%E7%A0%81%E6%88%96%E6%95%B0%E6%8D%AE%E9%9B%86%EF%BC%88Web%E7%95%8C%E9%9D%A2%E6%96%B9%E5%BC%8F%EF%BC%89?sort_id=3460976)或者[上传代码或数据集（SSH方式）](https://gitee.com/ascend/modelzoo/wikis/%E4%B8%8A%E4%BC%A0%E4%BB%A3%E7%A0%81%E6%88%96%E6%95%B0%E6%8D%AE%E9%9B%86%EF%BC%88SSH%E6%96%B9%E5%BC%8F%EF%BC%89?sort_id=3460984)。

## 操作步骤<a name="section18172142116385"></a>

本章节以一个已迁移好的适配MindSpore框架的[Resnet模型代码](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/official/cv/resnet)为例，介绍如何进行模型训练。

进入脚本所在路径，启动训练，此处以cifar10数据集为例。

```
cd ~/code/mindspore/model_zoo/official/cv/resnet/scripts
bash run_standalone_train.sh resnet50 cifar10 ~/dataset/cifar10/
```

本示例训练脚本run\_standalone\_train.sh 中将标准输出存入train/log文件中。

如果需要输出到终端上，需要进入run\_standalone\_train.sh中去掉\>重定向命令。

如果需要修改模型文件的存放路径，则需要进入项目src文件夹下，修改对应的config.py文件，训练结束后可到对应路径下载对应输出。

>![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/103414_964dcd9f_8394326.gif "icon-note.gif")**说明：** 
>用户在调试MindSpore模型时，如果前一次训练任务异常退出，需要清理环境相关进程（本示例需要清理如下两个相关的进程），然后再次拉起训练脚本。

```
ps -aux |grep python
python /usr/local/lib/python3.7/site-packages/mindspore/_extends/remote/kernel_build_server_ascend.py
python /home/admin/code/ms-resnet/train.py --net=resnet50 --dataset=cifar10 --run_distribute=True --device_num=2 --dataset_path=/data/dataset/storage/cifar10/cifar10-bin/
```

清除训练脚本进程，执行kill 进程id 清除相关进程

```
ps -aux |grep train |awk '{print $2}' |xargs -n 1 kill -9
```

清除MindSpore进程

```
ps -aux |grep mindspore |awk '{print $2}' |xargs -n 1 kill -9
```

停止训练任务，手动执行kill命令关闭训练进程。

