### Nvidia官方获取方式
在Nvidia提供的ModelZoo网站上查找需要的训练网络模型

https://ngc.nvidia.com/catalog/models?orderBy=modifiedDESC&pageNumber=1&query=&quickFilter=&filters=

![输入图片说明](https://images.gitee.com/uploads/images/2021/0121/104318_62abc614_1869725.png "屏幕截图.png")

https://github.com/NVIDIA/DeepLearningExamples/tree/master/TensorFlow/LanguageModeling/BERT

|  GPUs |  Sequence Length  | Batch size / GPU: mixed precision, FP32  | Gradient Accumulation: mixed precision, FP32  | Global Batch Size  | Throughput - mixed precision  | Throughput - FP32  | Throughput speedup (FP32 - mixed precision)  | Weak scaling - mixed precision  | Weak scaling - FP32  |
|---|---|---|---|---|---|---|---|---|---|
|1|	128|	16 , 8|	4096, 8192|	65536	|134.34|39.43|	3.41|	1.00|	1.00|
|---|---|---|---|---|---|---|---|---|---|
|4|	128|	16 , 8|	1024, 2048|	65536	|449.68|152.33|	2.95|	3.35|	3.86|
|---|---|---|---|---|---|---|---|---|---|
|8|	128|	16 , 8|	512, 1024|	65536	|1001.39|285.79|3.50|	7.45|	7.25|
|---|---|---|---|---|---|---|---|---|---|
|1|	512|	4 , 2|	8192, 16384|	32768	|28.72  |9.80  |2.93|	1.00|	1.00|
|---|---|---|---|---|---|---|---|---|---|
|4|	512|	4 , 2|	2048, 4096|	32768	|109.96  |35.32  |3.11|	3.83|	3.60|
|---|---|---|---|---|---|---|---|---|---|
|8|	512|	4 , 2|	1024, 2048|	32768	|190.65  |69.53  |2.74|	6.64|	7.09|
|---|---|---|---|---|---|---|---|---|---|
### 论文中性能数据获取方式

登陆PaperWithCode，根据论文名称查找模型实现代码，部分模型在github开源社区上有可能找得到V100上的性能数据

https://paperswithcode.com/


### 公有云V100弹性云主机实测数据获取



