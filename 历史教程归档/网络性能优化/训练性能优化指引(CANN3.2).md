 :exclamation: 这篇性能分析的wiki是基于20.2版本(CANN3.2.0)，浏览之前请确认下当前您使用的昇腾软件包版本，不同版本在工具使用上有出入。获取的profiling数据需要使用对应的profiling工具版本去解析，否则可能解析失败。

---
说到性能优化，可能是大家比较头疼的事儿。因为它涉及方方面面，比如大到整个工程的性能，小到某个接口函数的性能甚至是某个循环的性能，且优化这个过程又是耗时耗力的。在这儿，我们主要针对AI异构计算架构CANN，讨论深度学习中的网络训练性能优化Profiling，[Profiling工具的使用方法官方](https://support.huaweicloud.com/TensorFlowdevg-cann202training1/atlasmprtg_13_0032.html)也提供了指导手册。

## 目录
- [前提](#前提)
- [预备知识](#预备知识)
- [采集性能数据](#采集性能数据)
- [解析性能数据](#解析性能数据)
- [分析性能数据](#分析性能数据)
- [FAQ](#FAQ)

## <span id="前提">前提</span>
在进行网络性能优化之前，必须得确保你的网络在Ascend910环境上跑通。如果开发者还未在Ascend910环境上跑通自己的训练网络，可以移步到昇腾官方发布的[资料链接](https://support.huaweicloud.com/TensorFlowdevg-cann202training1/atlasmprtg_13_0001.html)进行网络的迁移，或者参看别人总结的[wiki](https://gitee.com/ascend/modelzoo/wikis/%E6%A8%A1%E5%9E%8B%E8%AE%AD%E7%BB%83%E4%B8%80%E6%8C%87%E7%A6%85?sort_id=3147602)，在这里我不细说了。接下来，我们会着重讲解如何将在Ascend910环境上跑通的网络进行训练性能的分析。

## <span id="预备知识">预备知识</span>
当前CANN软件包的安装分为两种：第一种是开发环境和运行环境独立的，第二种是开发环境和运行环境合一的，详见[链接](https://support.huaweicloud.com/instg-cli-cann202/atlasrun_03_0002.html) 。这两种不同的环境对Profiling分析流程上有啥区别呢？下面我们以图来解释。

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0303/101522_0cd729b7_1482256.png"/></div>

- 上图左侧是开发环境和运行环境分开独立的安装场景（如ModelArts平台）。在运行环境中，训练过程中采集的性能数据会落盘到代码中设置的性能存放路径中，具体如何设置性能采集路径在[采集性能数据](#采集性能数据)小节中会介绍；在开发环境中，首先将运行环境中生成的数据，通过scp获取得到；然后使用ToolKit包中安装的msprof工具进行解析。
- 上图右侧是开发环境和运行环境合一的安装场景（如依瞳Apulis平台）。开发&运行环境合一的好处，就是所有的包均安装在一台设备上，在性能分析时不需要将生成的profiling数据scp到另一台设备，直接在当前设备上即可使用msprof工具进行解析。

综合来看，无论上面哪种场景，既然要进行网络训练性能数据的分析，那首先就得采集性能数据，其次才能有针对性地进行解析和分析。下面我们以开发&运行合一的环境为例（依瞳Apulis平台），讲解如何进行性能数据的采集，解析及分析。针对ModelArts平台上的如何采集原始性能数据可以参看[这篇Wiki](https://gitee.com/ascend/modelzoo/wikis/Modelarts%E9%87%87%E9%9B%86Profiling%E6%80%A7%E8%83%BD%E6%95%B0%E6%8D%AE?sort_id=3646848)。

## <span id="采集性能数据">采集性能数据</span>
[官方文档](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0010.html)中提供了三种方式采集性能数据，分别为训练脚本Profiling配置，通过API接口方式，通过设置环境变量方式。下面我们主要讲解通过配置Profiling参数对训练脚本做一点改动，并针对TensorFlow框架两种模式分别介绍，示例中fp_point和bp_point取的是MobileNetV2中的前向和反向计算节点。

:exclamation: **一般情况下，我们只需要配置option中基本的output，task_trace和aicpu这三个参数信息。示例中提到的training_trace，fp_point和bp_point这三个参数，属于training_trace方面的进阶参数，如非必要可以删除不设置，但此篇wiki我还是对进阶参数进行了讲解。**

- Estimator模式下，通过NPURunConfig中的profiling_config开启Profiling数据采集，代码示例如下：
    ```
    from npu_bridge.estimator.npu.npu_config import NPURunConfig
    from npu_bridge.estimator.npu.npu_config import ProfilingConfig
    ...
    profiling_options = '{"output": "/home/zhonglin/profiling", \
                          "training_trace": "on",\
                          "task_trace": "on", \
                          "aicpu": "on",\
                          "fp_point": "MobilenetV2/Conv/Conv2D", \
                          "bp_point": "gradients/MobilenetV2/Conv/Conv2D_grad/Conv2DBackpropFilter"}'
    profiling_config = ProfilingConfig(enable_profiling=True, profiling_options=profiling_options)
    session_config=tf.ConfigProto()
    config = NPURunConfig(profiling_config=profiling_config, session_config=session_config)
    ```
- sess.run模式下，通过session配置项profiling_mode、profiling_options开启Profiling数据采集，代码示例如下：
    ```
    from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig

    custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
    custom_op.name = "NpuOptimizer"
    custom_op.parameter_map["use_off_line"].b = True
    custom_op.parameter_map["profiling_mode"].b = True  # 打开profiling
    custom_op.parameter_map["profiling_options"].s = tf.compat.as_bytes('{"output":"/home/zhonglin/profiling","training_trace":"on","task_trace":"on","aicpu":"on","fp_point":"MobilenetV2/Conv/Conv2D","bp_point":"gradients/MobilenetV2/Conv/Conv2D_grad/Conv2DBackpropFilter"}')
    config.graph_options.rewrite_options.remapping = RewriterConfig.OFF
    with tf.Session(config=config) as sess:
        sess.run()
    ```
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/175622_3d7a004a_1482256.png "屏幕截图.png")

profiling_options参数配置请参见[Profiling options参数解释](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0068.html)，用户根据自身情况修改output,fp_point以及bp_point的值即可。另外，需要注意的是，如果output参数路径不存在需要事先创建好；然后如何确定图中的前向节点fp_point名字和反向节点bp_point名字，也有一定的方法。
首先，我们在sess.run模式下，添加如下代码获取[网络图pbtxt文件](https://tensorflow.google.cn/versions/r1.15/api_docs/python/tf/io/write_graph)。
```
tf.io.write_graph(sess.graph_def, './', 'graph.pbtxt')
```
其次，我们通过下面的两条规则，在生成的图文件graph.pbtxt中搜索fp_point和bp_point节点名字。这里以MobileNetV2为例，生成的[grapb.pbtxt下载链接](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/profiling/graph.pbtxt)。
- fp_point从图的第一个node开始搜索，跳过数据节点和存储节点，选第一个计算节点。数据节点和存储节点通过name或者op可以判断出，一般op为Const，VariableV2,IteratorV2,Identity,Reshape，Cast等以及name中带有step，Dataset，seed，kernel,initializer等的node均可排除。在MobileNetV2的graph中，我们取如下截图高亮部分作为fp_point。
    <div align=center><img src="https://images.gitee.com/uploads/images/2021/0302/104116_786ab61e_1482256.png"/></div>
- bp_point从图的最后一个node反向查找，找到第一个出现的带有gradients的计算图的计算节点。一般选fp_point对应的梯度算子，但不是优化器更新该fp_point参数的节点。在MobileNetV2的graph中，我们取如下截图高亮部分作为bp_point。
    <div align=center><img src="https://images.gitee.com/uploads/images/2021/0302/104418_7fa9bcca_1482256.png"/></div>

-------------------------
好，上面讲了一堆如何配置profiling参数信息，用于采集性能数据。配置完后，接下来我们只要启动训练跑几个迭代iter正常退出即可。_这里需要注意一点，不能人为的用CTRL+C异常终止训练，这样会导致后续解析性能数据失败。_ 训练正常退出后，会在你指定的profiling输出路径中会生成一个以JOBXXXXXAAAAA类似命名的文件夹，假设该文件夹所在的绝对路径为/home/zhonglin/profiling。
<div align=center><img src="https://images.gitee.com/uploads/images/2021/0302/105502_67653a73_1482256.png"/></div>
<div align=center><img src="https://images.gitee.com/uploads/images/2021/0302/105816_da9859fc_1482256.png"/></div>


## <span id="解析性能数据">解析性能数据</span>
好，在上一步中我们采集到了原始性能数据，那我们该如何解析其中可读性差的done文件呢？ 这里我们可以借助CANN软件包中集成的ToolKit工具包——[msprof.pyc文件](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0013.html)。
20.2版本中，该工具文件安装在路径
`
/usr/local/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/profiler_tool/analysis/msprof
`
或在路径
`
/home/HwHiAiUser/Ascend/ascend-toolkit/latest/arm64-linux/toolkit/tools/profiler/profiler_tool/analysis/msprof
`
该工具文件具备多个功能，包括解析原始集采的性能数据，查询训练的Job信息以及导出Profiling的timeline和summary数据。

1. [**解析profiling数据**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0015.html) 
    ```
    python3.7.5 msprof.pyc import [-h] -dir <dir>
    ```
2. [**查询训练的job信息**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0016.html) 
    ```
    python3.7.5 msprof.pyc query [-h] -dir <dir>
    ```
3. [**导出timeline数据**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0018.html) 
    ```
    python3.7.5 msprof.pyc export timeline [-h] -dir <dir> [--iteration-id <iteration_id>] 
    ```
4. [**导出summary数据**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0023.html)
    ```
    python3.7.5 msprof.pyc export summary [-h] -dir <dir> [--iteration-id <iteration_id>] 
    ```
为了方便使用，上述四条命令可以集成在一个run_msprof.sh脚本中，如下：
```
#!/bin/bash
set -e
### Before run this shell, make sure you have generated profiling data
### $1 is the absolute directory of profiling data 
### $MSPROF_DIR is only use for on Apulis Plateform, you may change it on other platform
PROFILING_DIR=$1
MSPROF_DIR=/home/HwHiAiUser/Ascend/ascend-toolkit/latest/arm64-linux/toolkit/tools/profiler/profiler_tool/analysis/msprof

cd $MSPROF_DIR

python3.7 msprof.pyc import -dir ${PROFILING_DIR}
echo "===>>>[OK] msprof sqlite.\n"

python3.7 msprof.pyc query -dir ${PROFILING_DIR}
echo "===>>>[OK] msprof query.\n"

python3.7 msprof.pyc export timeline -dir ${PROFILING_DIR}
echo "===>>>[OK] msprof timeline.\n"

python3.7 msprof.pyc export summary -dir ${PROFILING_DIR}
echo "===>>>[OK] msprof summary.\n"
```
我们可以使用如下命令执行run_msprof.sh文件进行原始性能数据的解析，并得到timeline和summary文件夹。
```
## 假设/home/zhonglin/profiling/路径下存放有采集到的原始性能数据JOBXXXXAAAAA
sh run_msprof.sh /home/zhonglin/profiling/
```
- timeline文件夹下存放的是生成的json文件。timeline文件夹下每个文件的数据说明，可参看[对应的链接](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0019.html)，里面有详细的介绍。另外，将timeline文件夹scp到window系统中，可以在Chrome浏览器中输入"chrome://tracing"地址，然后将落盘json文件拖到空白处即可打开文件内容，并使用键盘上的快捷键查看，w：放大 s：缩小 a：左移 d：右移
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/113931_7ff4562e_1482256.png "屏幕截图.png")
- summary文件夹下存放的是生成的csv文件。将summary文件夹scp到window系统中，可以使用件excel打开。summary文件夹下每个文件的数据说明，可参看[对应的链接](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0024.html)，里面有详细的介绍。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/113956_1c0a8772_1482256.png "屏幕截图.png")

## <span id="分析性能数据">分析性能数据</span>
OK，到了这一步，我们就需要沉下心来，具体分析解析出来的timeline和summary文件夹下的数据了，看看当前网络的性能瓶颈在哪。主要分两步，第一步是迭代耗时；第二部是算子性能耗时。下面我们以训练性能比较好的MobileNetV2和性能较差的AlexNet为例分别进行讲解，它们的训练源码下载链接分别为[MobileNetV2](https://gitee.com/ascend/modelzoo/tree/master/built-in/TensorFlow/Official/cv/image_classification/MobileNetV2_for_TensorFlow)和[AlexNet](https://gitee.com/echo_lin/alexnet)。

- [**MobileNetV2训练性能数据探析**](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/profiling/mobilenet-v2.rar)【点击链接下载】
    当前MobileNetV2在Ascend910上的性能，性能数据大致如下表格，但我们仍深入看看NPU性能数据，看看它的性能好在哪里。
    | platform         | batch size |  speed    |
    | :--------------: | :--------: |:--------: |
    | 1xAscend 910     |    192     | 1882 img/s|
    | 1xTesla V100-16G |    192     | 774 img/s |
    | 1xAscend 910     |    128     | 1805 img/s|
    | 1xTesla V100-16G |    128     | 691 img/s |
    | 1xAscend 910     |    64      | 1498 img/s|
    | 1xTesla V100-16G |    64      | 525 img/s |
    | 1xAscend 910     |    32      | 1110 img/s|
    | 1xTesla V100-16G |    32      | 355 img/s |
    1. 迭代耗时分析【宏观】
    打开Chrome浏览器输入"chrome://tracing"地址，将timeline里面的training_trace_0_1.json拖到空白处进行打开。通过键盘上的快捷键(w：放大 s：缩小 a：左移 d：右移)，查看每次迭代的耗时情况。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/144138_94952bdb_1482256.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/143336_71cf03d4_1482256.png "屏幕截图.png")
    从上面的截图中可以看到，第二次迭代训练的总耗时为106.9 ms，其中fp+bp耗时为96 ms左右，FP+BP前面空白处的差异耗时部分主要是数据增强耗时（主要是数据读取/加载耗时）。training_trace的图说明可以参看[官方发布的资料](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0003.html)。另外，training_trace_0_1.json文件也有对应的csv文件，可以在summary中打开training_trace_0_1.csv文件查看，也可以很直观的看到：`Iteration Time = FP to BP Time + Grad Refresh + Data Aug`。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/145455_12818070_1482256.png "屏幕截图.png")
     **从宏观的迭代耗时层面看**：数据增强Data Aug部分耗时比算子FP+BP耗时小很多，不是性能的瓶颈 :+1: :+1: :+1: 。 

    2. 算子耗时分析【微观】
    接下来，我们深入进入，分析具体每一个算子的性能耗时情况。在这我们暂且主要看两个文件op_statistic_0_1.csv和op_summary_0_1.csv。

        - [**op_summary_0_1.csv文件**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0024.html)。该文件统计的是某个迭代下（默认为1），bp_point + fp_point整个链路的算子耗时具体情况。也即是配置参数中，bp_point到fp_point的所有节点耗时。在这个文件中，着重看Task Duration列，它记录着当前算子的耗时。可以通过表格中的自定义排序，选择Task Duration为主要关键字，进行降序重排表格，开头部分截图如下。可见，当前网络中涉及的算子，耗时比较大的主要是DepthwiseConv2D的正反向算子，最大耗时是DepthwiseConv2DBackpropFilterD接近7 ms。一般来说，出现毫秒ms级别的算子要尤其关注下。
        ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/153005_b4a28265_1482256.png "屏幕截图.png")
        
        - [**op_statistic_0_1.csv文件**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0025.html)。该文件是对bp_point + fp_point整个链路上算子，不区分OP Name，按算子的OP Type做了统计。比如将Conv2D的算子统计为一行，统计调用次数，总耗时，平均耗时，最大耗时，最小耗时等。
        ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/151644_4154b641_1482256.png "屏幕截图.png")

        **从微观的算子耗时层面看**：2590个算子中，有一个算子耗时较大接近7 ms，耗时在2-3ms之间的算子有5个，耗时在1-2ms之间的算子有10个，高耗时部分占比较小。
 
-  [**AlexNet训练性能数据探析**](https://zhonglin-public.obs.cn-north-4.myhuaweicloud.com/profiling/alexnet.rar)【点击链接下载】
    通过实测，同一份AlexNet网络的训练代码的性能比对数据大致如下表格。从表格中可以看出，无论是FP+BP还是数据增强/读取部分，单卡情况Ascend910都是比Tesla V100要差一些。接下来我们依旧深入NPU性能数据，看看性能瓶颈点在哪里。
    | platform         | batch size |  bp+fp  | get data |
    | :--------------: | :--------: |:--------: |:--------: |
    | 1xAscend 910     |    32      | 38 ms | 80 ms |
    | 1xTesla V100-16G |    32      | 23 ms | 52 ms |

    源码打印截图如下：
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0303/105212_f6ee4384_1482256.png "屏幕截图.png")
    1. 迭代耗时分析【宏观】
    打开Chrome浏览器输入"chrome://tracing"地址，将timeline里面的training_trace_0_1.json拖到空白处进行打开。通过键盘上的快捷键(w：放大 s：缩小 a：左移 d：右移)，查看每次迭代的耗时情况。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/160423_01bc32c1_1482256.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/160501_69fc6eed_1482256.png "屏幕截图.png")
    从上面的截图中可以看到，第二次迭代训练的总耗时为131.4 ms，其中fp+bp耗时为21.6 ms左右，FP+BP前面空白处的差异耗时部分主要是数据增强耗时（主要是数据读取/加载耗时），**耗时将近有108 ms**。training_trace的图说明可以参看[官方发布的资料](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0003.html)。另外，training_trace_0_1.json文件也有对应的csv文件，可以在summary中打开training_trace_0_1.csv文件查看，也可以很直观的看到：`Iteration Time = FP to BP Time + Grad Refresh + Data Aug`。
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/161005_10b4edfb_1482256.png "屏幕截图.png")
     **从宏观的迭代耗时层面看**：数据增强Data Aug部分耗时是算子FP+BP耗时的5倍，是性能的一个主要瓶颈 :broken_heart: :broken_heart: :broken_heart:。  开发者自身可以从训练代码层面优化数据加载/读取部分。华为官方资料给出了[数据预处理](https://support.huaweicloud.com/TensorFlowdevg-cann202training1/atlasmprtg_13_0029.html)的方法供参考。

    2. 算子耗时分析【微观】
    接下来，我们深入进入，分析具体每一个算子的性能耗时情况。在这我们暂且主要看两个文件op_statistic_0_1.csv和op_summary_0_1.csv。

        - [**op_summary_0_1.csv文件分析**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0024.html)。该文件统计的是某个迭代下（默认为1），bp_point + fp_point整个链路的算子耗时具体情况。也即是配置参数中，bp_point到fp_point的所有节点耗时。在这个文件中，着重看Task Duration列，它记录着当前算子的耗时。可以通过表格中的自定义排序，选择Task Duration为主要关键字，进行降序重排表格，开头部分截图如下。可见，当前网络中涉及的算子，耗时比较大的主要是MatMulV2算子，最大耗时接近5 ms。一般来说，出现毫秒ms级别的算子要尤其关注下。
        ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/162316_b96dd230_1482256.png "屏幕截图.png")
        
        - [**op_statistic_0_1.csv文件分析**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0025.html)。该文件是对bp_point + fp_point整个链路上算子，不区分OP Name，按算子的OP Type做了统计。比如将 Mul 算子统计为一行，统计调用次数，总耗时，平均耗时，最大耗时，最小耗时等。
        ![输入图片说明](https://images.gitee.com/uploads/images/2021/0302/163754_a6355d2f_1482256.png "屏幕截图.png")

        **从微观的算子耗时层面看**：182个算子中，有一个算子耗时较大接近5 ms，耗时大于1ms的算子有5个，累计耗时有15ms，这五个算子耗时占比比较高，且都集中在同一个算子MatMul，可能需要进一步优化该算子性能 :broken_heart:  :broken_heart: :broken_heart:。对MatMul算子的优化，开发者去[Modelzoo社区](https://gitee.com/ascend/modelzoo/issues)提issue。

## <span id="FAQ">FAQ</span>
- 没有生成training_trace文件
    如果你执行`sh run_msprof.sh /home/zhonglin/profiling/`后，发现timeline中没有training_trace_0_1.json文件，那可能存在几种情况：
    1. 很大概率是你设置的bp_point和bp_point有问题，需要检查一下；
    2. 训练异常终止了。
- 解析数据失败
  1. 可能是采集数据不完整。这种情况主要是由于session方式下，如果不是采用
    ```with tf.Session(config=config) as sess：```
    而是使用
    ```sess = tf.Session(config=config) ```
    方式创建的sess，那在训练代码结束时需要手动关闭会话sess，加上代码`sess.close()`
  2. 使用msprof工具解析性能数据的CANN版本，和训练时采集性能数据的CANN版本不匹配。