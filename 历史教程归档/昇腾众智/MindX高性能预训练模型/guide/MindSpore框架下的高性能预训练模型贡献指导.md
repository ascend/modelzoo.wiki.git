### MindSpore框架下的高性能预训练模型贡献指导

- [如何在MindSpore社区进行贡献](#1-如何在MindSpore社区进行贡献)
- [高性能预训练模型在MindSpore社区下的上库目录规范](#2-高性能预训练模型在MindSpore社区下的上库目录规范)
- [高性能预训练模型众智PR提交要求](#3-高性能预训练模型众智PR提交要求)
- [常见问题](#4-常见问题)

#### 1 如何在MindSpore社区进行贡献
MindSpore框架下的高性能预训练模型，在MindSpore社区进行贡献，通用流程参见：[昇思贡献指南](https://gitee.com/mindspore/models/blob/master/CONTRIBUTING_CN.md)

#### 2 高性能预训练模型在MindSpore社区下的上库目录规范
 **说明：** MindSpore社区的[模型仓](https://gitee.com/mindspore/models)中，在具体的模型目录下，有基于原框架的训练与离线推理代码。MindX 高性能预训练模型基于现有目录新增扩展子目录和文件( **涉及的目录和文件参见下面目录树中的注释** )，实现新增功能交付， **不允许直接修改已有代码** 。


    ├── modelart            # MindX高性能预训练模型新增，训练modelarts功能
    │   ├── res             # （可选）AI市场封面图片，和readme文件，可放这里 
    │   ├── config.yaml     # （可选）AI市场发布配置文件
    │   ├── readme.md       # （可选）AI市场模型描述文件
    │   └── train_start.py  # ModelArts训练任务启动脚本
    ├── ascend310_infer
    │   ├── inc
    │   └── src
    ├── scripts
    │   ├── docker_start.sh  # MindX高性能预训练模型新增，训练启动容器脚本
    │   ├── run_distribute_train_ascend.sh
    │   ├── run_eval_ascend.sh
    │   ├── run_infer_310.sh
    │   └── run_standalone_train_ascend.sh
    ├── infer                # MindX高性能预训练模型新增推理目录  
    │   ├── docker_start_infer.sh  # 推理启动容器脚本
    │   ├── README.md        # （可选）离线推理文档
    │   ├── convert          
    │   │   ├── convert.sh   # 转换om模型脚本
    │   │   └── aipp.cfg     # （根据需要提供）AIPP配置文件
    │   ├── data             
    │   │   ├── model        # 模型文件目录，模型文件上库不提交，统一归档到指定位置
    │   │   └── input        # 模型输入数据集
    │   │   └── config       # 模型相关配置文件（如label、SDK的pipeline）
    │   ├── mxbase           # 基于mxbase推理
    │   │   ├── build        # mxbase编译目录
    │   │   ├── src          # 源文件目录  
    │   │   │   ├── xx.cpp   # mxbase推理源文件
    │   │   │   ├── xx.h     # mxbase推理头文件
    │   │   │   └── main.cpp # mxbase入口函数文件
    │   │   ├── CMakeLists.txt  # cmake编译配置文件
    │   │   ├── build.sh     # 编译脚本
    │   └── sdk              # 基于sdk run包推理；如果是C++实现，存放路径一样
    │   │   ├── main.py      # sdk调用入口脚本
    │   │   ├── build.sh     # 编译sdk可执行程序脚本
    │   └── util             # （根据需要提供）工具包
    ├── src
    │   ├── FasterRcnn
    │   ├── aipp.cfg
    │   ├── config.py
    │   ├── convert_checkpoint.py
    │   ├── dataset.py
    │   ├── lr_schedule.py
    │   ├── network_define.py
    │   └── util.py
    ├── Dockerfile
    ├── eval.py
    ├── export.py
    ├── LICENSE
    ├── mindspore_hub_conf.py
    ├── postprocess.py
    ├── README.md
    ├── README_CH.md
    ├── requirements.txt
    └── train.py

样例：[yolov4-tiny](https://gitee.com/mindspore/models/tree/r1.3/official/cv/yolov4_tiny/)

#### 3 高性能预训练模型众智PR提交要求
参见：[PR提交要求](https://gitee.com/ascend/modelzoo/wikis/%E6%98%87%E8%85%BE%E4%BC%97%E6%99%BA/MindX%E9%AB%98%E6%80%A7%E8%83%BD%E9%A2%84%E8%AE%AD%E7%BB%83%E6%A8%A1%E5%9E%8B/guide/PR%E6%8F%90%E4%BA%A4%E8%A6%81%E6%B1%82%E5%92%8C%E8%A7%84%E8%8C%83)

#### 4 常见问题
1. [如何合入PR的多个commit](https://gitee.com/ascend/modelzoo/wikis/%E6%98%87%E8%85%BE%E4%BC%97%E6%99%BA/MindX%E9%AB%98%E6%80%A7%E8%83%BD%E9%A2%84%E8%AE%AD%E7%BB%83%E6%A8%A1%E5%9E%8B/guide/PR%E6%8F%90%E4%BA%A4%E5%90%88%E5%B9%B6%E5%A4%9A%E4%B8%AAcommit%EF%BC%8C%E8%A7%A3%E5%86%B3needs-squash%E6%A0%87%E7%AD%BE)

