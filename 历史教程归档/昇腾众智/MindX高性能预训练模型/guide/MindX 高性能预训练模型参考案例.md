### MindX 高性能预训练模型参考案例

MindSpore模型：
1. [YOLOv4 Tiny](https://www.hiascend.com/zh/software/modelzoo/detail/C/ed8926c5412d4121ad3dc738c639b0ce)
2. [ResNeXt-101](https://www.hiascend.com/zh/software/modelzoo/detail/C/c257819d9da24bde82b31bf81a6dae5a)
3. [BERT-Base](https://www.hiascend.com/zh/software/modelzoo/detail/C/2dd8b0f9da4941df9472ea1c977309a7)

PyTorch模型：
1. [DeepLabv3+](https://www.hiascend.com/zh/software/modelzoo/detail/C/91b3fc3434ef48acaf157556cb408946)
2. [Wide&Deep](https://www.hiascend.com/zh/software/modelzoo/detail/C/52dfbb6e4ab44b4fb7057fc061d27595)
