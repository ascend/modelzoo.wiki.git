# 使用MindStudio开发高性能预训练模型指导

本文以华为昇腾modelzoo高性能预训练模型unet为例介绍如何使用华为MindStudio开发者工具进行众智高性能预训练模型开发。整体方案为在Windows安装MindStudio开发者工具，通过ssh方式连接远程服务器环境，本地开发，远程训练。

# 创建容器并开启ssh登录服务

## 创建容器

在模型代码目录下的script目录下，打开docker_start.sh，没有可自行创建。增加参数-p {port}:22将容器端口22映射到物理机端口port,port可自行选取。

```
#!/bin/bash
docker_image=$1
data_dir=$2
model_dir=$3

docker run -it --ipc=host \
               --device=/dev/davinci0 \
               --device=/dev/davinci1 \
               --device=/dev/davinci2 \
               --device=/dev/davinci3 \
               --device=/dev/davinci4 \
               --device=/dev/davinci5 \
               --device=/dev/davinci6 \
               --device=/dev/davinci7 \
               --device=/dev/davinci_manager \
               --device=/dev/devmm_svm \
               --device=/dev/hisi_hdc \
               --privileged \
               -p 20000:22 \
               -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
               -v /usr/local/Ascend/add-ons/:/usr/local/Ascend/add-ons \
               -v ${data_dir}:${data_dir} \
               -v ${model_dir}:${model_dir} \
               -v /var/log/npu/conf/slog/slog.conf:/var/log/npu/conf/slog/slog.conf \
               -v /var/log/npu/slog/:/var/log/npu/slog/ \
               -v /var/log/npu/profiling/:/var/log/npu/profiling \
               -v /var/log/npu/dump/:/var/log/npu/dump \
               -v /var/log/npu/:/usr/slog ${docker_image} \
               /bin/bash

```

执行 bash docker_start.sh {train_image} {data_path} {模型代码根目录}，镜像可从[昇腾镜像仓](https://ascendhub.huawei.com/#/index)下载对应架构的mindspore镜像。

## 进入容器

执行docker exec -it {container_name/ container_id} bash进入容器。

安装openssh-server.

```
apt-get install openssh-server
```

打开/etc/ssh/sshd_config文件设置PermitRootLogin为yes,PasswordAuthentication为yes。

```
...
PermitRootLogin yes
PasswordAuthentication yes
...
```

重启ssh服务。

```
sevice ssh restart
```

验证是否可以ssh登录容器。

```
ssh root@ip:port
```

# 安装MindStudio

安装参考(https://support.huaweicloud.com/usermanual-mindstudio301/atlasms_02_0412.html)

# 创建/导入工程

通过新建或打开创建模型工程，对于众智高性能预训练模型可从(http://gitee.com/mindspore/mindspore)下载对应模型代码，下载完成后在MindStudio中打开模型代码目录。

![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/0938226tmac3qmisqfyxqt.png)

配置窗口中选择Type:Ascend Trainng，Framework可根据模型所使用框架自行选择，这里选择MindSpore。

![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/093859hnpczhu0m0rdk9ou.png)

创建好的工程如图所示。


![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/094849z7gtv2esjbm5kczb.png)

# 配置环境

创建好工程后，便可以在本地进行代码开发，调测运行可通过远程连接功能将代码同步到远端服务器运行，所以这里需要做相关配置，包括远端服务器地址、执行命令、执行参数、环境变量。如图打开配置页。

![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/095446owhayht7ybpxmdhg.png)
![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/1000086bhgfgu8b9sjhzey.png)
## 说明
- SSH Connection设置为创建的容器地址，即user@ip:port，ip为昇腾服务器地址，port为上文创建容器时所映射的容器端口号。
- Excutable为执行训练的命名，一般可将train.py作为执行命令，并在Command Arguments设置train.py所带参数。当然，用户可根据自身需要设置其他入口脚本也可以。
# 执行训练

点击下图按钮或菜单栏的run菜单下的run选项执行训练任务。
![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/100953tfrxrhytv8tgfv9t.png)


在下方输出栏可查看任务输出。

![image.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202107/09/104611r1rk9x9gh2g6hfjx.png)
