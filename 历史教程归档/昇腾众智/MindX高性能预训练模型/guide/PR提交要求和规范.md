PR提交标题：
[XX大学][高校贡献][Pytorch/Tesorflow/Mindspore][模型名称]-高性能预训练模型提交+功能

PR提交描述：
- PR模板（以resnet模型为例）
```
    # 模型名称
    
    - 软件包

    CANN 5.0.2
    SDK版本：21.0.2
    固件驱动 21.0.2
    
    # 自验报告
    ```shell    
    # ModelArts训练
    # 是否正确输出了ckpt、air/onnx模型文件
    python3.7 modelarts/train_start.py --train_url=xxx --data_url=xxx ...
    # 验收结果： OK / Failed
    # 备注： 目标输出结果；无输出日志，运行报错，报错日志xx 等
    
    # SDK推理
    # 是否正确输出了推理结果、推理精度
    运行SDK推理：bash run.sh ./val_union/ resnet50_npu_result
    测试推理精度：python3.7 classification_task_metric.py resnet50_npu_result/ ./val_label.txt ./ ./result.json
    # 验收结果： OK / Failed
    # 备注： 目标精度达标；验收测试性能336FPS；无输出日志，运行报错，报错日志xx 等

    # MxBase推理
    # 是否正确输出推理结果
    编译可执行程序：bash build.sh
    运行推理程序：./resnet ../data/input
    # 验收结果： OK / Failed
    # 备注： 推理结果正确，无输出日志，运行报错，报错日志xx 等


```

Commit提交内容：
【模型】ResNet
【修改说明】模型训练脚本增加执行权限
【验证结果】ModelArts、SDK、MxBase功能验证通过，SDK推理精度达标
【修改人】zhangsan（拼音全拼）
【评审人】chenshushu（拼音全拼）


MindSpore:
代码提交标准：
https://gitee.com/mindspore/community/blob/master/security/coding_guild_cpp_zh_cn.md
https://gitee.com/mindspore/community/blob/master/security/coding_guild_python_zh_cn.md
https://gitee.com/mindspore/community/blob/master/security/comments_specification_zh_cn.md

TensorFlow/Pytorch:
代码提交相关：
https://gitee.com/ascend/modelzoo/blob/master/contrib/CONTRIBUTING.md

规范标准
1、C++代码遵循google编程规范：Google C++ Coding Guidelines；单元测测试遵循规范： Googletest Primer。
link: https://zh-google-styleguide.readthedocs.io/en/latest/google-cpp-styleguide/contents/

2、Python代码遵循PEP8规范：Python PEP 8 Coding Style；单元测试遵循规范： pytest
link: https://www.cnblogs.com/bymo/p/9567140.html

规范备注
1、优先使用string类型，避免使用char*；

2、禁止使用printf，一律使用cout；

3、内存管理尽量使用智能指针；

4、不准在函数里调用exit；

5、禁止使用IDE等工具自动生成代码；

6、控制第三方库依赖，如果引入第三方依赖，则需要提供第三方依赖安装和使用指导书；

7、一律使用英文注释，注释率30%--40%，鼓励自注释；

8、函数头必须有注释，说明函数作用，入参、出参；

9、统一错误码，通过错误码可以确认那个分支返回错误；

10、禁止出现打印一堆无影响的错误级别的日志；