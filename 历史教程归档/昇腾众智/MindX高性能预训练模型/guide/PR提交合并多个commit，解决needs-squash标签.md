# stat/needs-squash标签

由于一个PR提交多个commit导致如下现象：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/173640_d15dfb5a_8174600.png "pr_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/173901_3f001548_8174600.png "pr_2.png")

为什么要解决squash？这个由于PR合入master后，多个commit id会存在master主干上，对模型版本发布造成麻烦。

# 解决多个commit合并

## 查看提交log

进入git bash命令行，输入git log:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/175439_398903ba_8174600.png "pr_3.png")
从log中可以看到有两次提交

## 合并多个commit

执行命令，git rebase -i HEAD~2:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/175908_a671d677_8174600.png "pr_4.png")

以第一个commit为基准，把后续的commit的pick均修改为s（注意小写）：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/180206_ee7a1e6a_8174600.png "pr_5.png")

保存并推出；重新查看log，发现两次提交已合并：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/180412_c7d2d18a_8174600.png "pr_6.png")

 **注意：如果需要撤回修改，执行命令：git rebase --abort** 

## push远端
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/190809_5950995d_8174600.png "pr_7.png")

查看结果：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/191022_e6f6fc3f_8174600.png "pr_8.png")

