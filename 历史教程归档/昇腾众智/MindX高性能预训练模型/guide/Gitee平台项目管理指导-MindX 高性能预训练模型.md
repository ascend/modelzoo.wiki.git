# Gitee平台项目管理指导-MindX 高性能预训练模型

## 1 Requirement任务跟踪管理

### 1.1 设置任务“主状态”

![enter image description here](https://images.gitee.com/uploads/images/2021/0409/094808_612fcaa4_7432804.png "屏幕截图.png")

### 1.2 设置任务“子状态”

![输入图片说明](https://images.gitee.com/uploads/images/2021/0914/103142_4c574f84_923381.png "屏幕截图.png")

### 1.3 MindX 高性能预训练模型的工作流

![输入图片说明](https://images.gitee.com/uploads/images/2021/0914/103350_edf0bae5_923381.png "屏幕截图.png")
### 1.4 其他

大量的子状态标签，会降低Gitee任务跟踪的可读性，引起混乱，所以在设置串行工作流的新子状态标签时，需要删除的上一子状态的标签。
如：在完成“训练精度调优”后，若开始进行“训练性能调优”，则在设置“train-acc-dbg/doing”时，删除“train-acc-dbg/done”标签；若暂未能进行后继动作，则保持当前标签不变。


## 2 Bug-Report任务跟踪管理
![enter image description here](https://images.gitee.com/uploads/images/2021/0409/100707_5d656004_7432804.png "屏幕截图.png")
