### PyTorch框架下的高性能预训练模型贡献指导

- [贡献PyTorch框架下的高性能预训练模型基本流程](#1-贡献PyTorch框架下的高性能预训练模型基本流程)
- [高性能预训练模型在PyTorch社区下的上库目录规范](#2-高性能预训练模型在PyTorch社区下的上库目录规范)
- [高性能预训练模型众智PR提交要求](#3-高性能预训练模型众智PR提交要求)

#### 1 贡献PyTorch框架下的高性能预训练模型基本流程
PyTorch框架下的高性能预训练模型，在Ascend社区ModelZoo进行贡献，通用流程参见：
[昇腾ModelZoo贡献指南](https://gitee.com/ascend/modelzoo/blob/master/contrib/CONTRIBUTING.md)
[昇腾ModelZoo贡献流程详解&协议说明](https://gitee.com/wangjiangben_hw/ascend-pytorch-crowdintelligence-doc/blob/master/pytorch-train-guide/modelzoo%E8%B4%A1%E7%8C%AE%E6%B5%81%E7%A8%8B%E8%AF%A6%E8%A7%A3&%E5%8D%8F%E8%AE%AE%E8%AF%B4%E6%98%8E.md#gitee%E4%BB%93pr%E8%B4%A1%E7%8C%AE%E6%B5%81%E7%A8%8B)

#### 2 高性能预训练模型在Ascend社区ModelZoo的上库目录规范
 **说明：** Ascend社区的[模型仓](https://gitee.com/ascend/modelzoo)中，模型的训练和推理分离在不同的路径，为方便统一开发者贡献MindX 高性能预训练模型，仅使用训练模型所在的目录新增扩展子目录和文件( **涉及的目录和文件参见下面目录树中的注释** )，实现新增功能交付， **不允许直接修改已有代码** 。

    ├── modelarts           # MindX高性能预训练模型新增，训练modelarts功能
    │   ├── res             # （可选）AI市场封面图片，和readme文件，可放这里
    │   ├── config.yaml     # （可选）AI市场发布配置文件
    │   ├── readme.md       # （可选）AI市场模型描述文件
    │   └── train_start.py  # ModelArts训练任务启动脚本
    ├── scripts
    │   ├── docker_start.sh # MindX高性能预训练模型新增，训练启动容器脚本
    ├── infer               # MindX高性能预训练模型新增 
    │   ├── docker_start_infer.sh # 推理启动容器脚本
    │   ├── README.md       # 离线推理文档
    │   ├── convert          
    │   │   ├── convert.sh   # 转换om模型脚本
    │   │   └── aipp.cfg     # （根据需要提供）AIPP配置文件
    │   ├──data             # 包括模型文件、模型输入数据集、模型相关配置文件（如label、SDK的pipeline）
    │   │   ├── model       # 模型文件目录，模型文件上库不提交，统一归档到指定位置
    │   │   └── input       # 模型输入数据集
    │   │   └── config      # 模型相关配置文件（如label、SDK的pipeline）
    │   ├── mxbase          # 基于mxbase推理
    │   │   ├── build       # mxbase编译目录
    │   │   ├── src         # 源文件目录    
    │   │   │   ├── xx.cpp  # mxbase推理源文件
    │   │   │   ├── xx.h    # mxbase推理头文件
    │   │   │   └── main.cpp   # mxbase入口函数文件
    │   │   ├── CMakeLists.txt # cmake编译配置文件
    │   │   ├── build.sh    # 编译脚本
    │   └── sdk             # 基于sdk run包推理；如果是C++实现，存放路径一样
    │   │   ├── main.py     # sdk调用入口脚本
    │   │   ├── build.sh    # 编译sdk可执行程序脚本  
    │   └── util            # （根据需要提供）工具包
    ├── delete_onnx_pad.py
    ├── densenet_0_2_2.py
    ├── densenet_0_5_0.py
    ├── densenet121_1p_main.py
    ├── densenet121_8p_main.py
    ├── Dockerfile
    ├── docker_start.sh     # 推理和训练启动容器脚本
    ├── env_new.sh
    ├── eval.sh
    ├── LICENSE
    ├── npu_set_env.sh
    ├── pthtar2onx.py
    ├── README.md           # 英文版，后续支持
    ├── README_CH.md        # （可选）中文版，MindX高性能预训练模型上传后，覆盖原有REAMDE，该文件与昇腾社区资料内容一致
    ├── requirements.txt
    ├── run_1p.sh
    ├── run_8p.sh
    └── run_to_onnx.sh
样例：[deeplabv3+](https://gitee.com/ascend/modelzoo/tree/master/built-in/PyTorch/Official/cv/semantic_segmentation/DeepLabv3+_ID1695_for_PyTorch)

#### 3 高性能预训练模型众智PR提交要求
参见：[PR提交要求](https://gitee.com/ascend/modelzoo/wikis/%E6%98%87%E8%85%BE%E4%BC%97%E6%99%BA/MindX%E9%AB%98%E6%80%A7%E8%83%BD%E9%A2%84%E8%AE%AD%E7%BB%83%E6%A8%A1%E5%9E%8B/guide/PR%E6%8F%90%E4%BA%A4%E8%A6%81%E6%B1%82%E5%92%8C%E8%A7%84%E8%8C%83)