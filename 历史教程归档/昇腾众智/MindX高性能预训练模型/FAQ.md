# 高性能预训练模型开发常见问题FAQ

-   [1 ModelArts问题FAQ](#1-ModelArts问题FAQ)
    -    [FAQ1-1 华为云登录](#FAQ1-1-华为云登录)
    -    [FAQ1-2 如何进行代码调试](#FAQ1-2-如何进行代码调试)
    -    [FAQ1-3 如何使用适配训练脚本](#FAQ1-3-如何使用适配训练脚本)
    -    [FAQ1-4 如何使用pytorch框架创建训练作业](#FAQ1-4-如何使用pytorch框架创建训练作业)
    -    [FAQ1-5 使用账号登录obs-browser客户端报用户名密码错误](#FAQ1-5-使用账号登录obs-browser客户端报用户名密码错误)
    -    [FAQ1-6 模型训练无法创建日志目录](#FAQ1-6-模型训练无法创建日志目录)
    -    [FAQ1-7 如何在ModelArts上安装软件依赖](#FAQ1-7-如何在ModelArts上安装软件依赖)
-   [2 MindX SDK问题FAQ](#2-MindX-SDK问题FAQ)
    -    [FAQ2-1 SDK pipeline配置错误](#faq2-1-SDK-pipeline配置错误)
    -    [FAQ2-2 如何开发SDK后处理代码](#faq2-2-如何开发SDK后处理代码)
    -    [FAQ2-3 有没有TensorBase的操作例子或者说明文档](#faq2-3-有没有TensorBase的操作例子或者说明文档)
    -    [FAQ2-4 模型推理结果都一样](#faq2-4-模型推理结果都一样)
    -    [FAQ2-5 运行程序时报申请内存失败](#faq2-5-运行程序时报申请内存失败)
    -    [FAQ2-6 如何获取sdk推理后的结果](#faq2-6-如何获取sdk推理后的结果)

-   [3 MxBase问题FAQ](#3-MxBase问题FAQ)
    -    [FAQ3-1 如何查看编译过程详细日志](#faq3-1-如何查看编译过程详细日志)
    -    [FAQ3-2 编译MxBase推理二进制时报Undefined reference to vtable](#faq3-2-编译MxBase推理二进制时报Undefined-reference-to-vtable)
    -    [FAQ3-3 CMakeList的编写和参数详解](#faq3-3-CMakeList的编写和参数详解)
    -    [FAQ3-4 CMake无法找到OpenSSL库](#faq3-4-CMake无法找到OpenSSL库)
    -    [FAQ3-5 ModelInference报ret145000的解决办法](#faq3-5-ModelInference报ret145000的解决办法)
    -    [FAQ3-6 ModelInference报ret507011的可能原因及解决办法](#faq3-6-ModelInference报ret507011的可能原因及解决办法)
    
-   [4 其他问题FAQ](#4-其他问题faq)
    -    [FAQ4-1 查询推理服务器用了哪些卡](#FAQ4-1-查询推理服务器用了哪些卡)
    -    [FAQ4-2 使用npu-device出现问题](#FAQ4-2-使用npu-device出现问题)
    -    [FAQ4-3 docker容器下gdb无法正常工作的解决办法](#FAQ4-3-docker容器下gdb无法正常工作的解决办法)
    -    [FAQ4-4 Python使用llib.urlopen打开https链接SSL报错CERTIFICATE_VERIFY_FAILED](#FAQ4-4-Python使用llib.urlopen打开https链接SSL报错CERTIFICATE_VERIFY_FAILED)
    -    [FAQ4-5 训练开启Apex混合精度加速opt_level="O1"并在完成训练后转onnx失败的解决办法](#FAQ4-5-训练开启Apex混合精度加速opt_level="O1"并在完成训练后转onnx失败的解决办法)  
    -    [FAQ4-6 推荐模型在转onnx时报IndexError的解决方法](#FAQ4-6-推荐模型在转onnx时报IndexError的解决方法)  
    -    [FAQ4-7 pytorch模型1p训练时使用非0卡训练](#FAQ4-7-pytorch模型1p训练时使用非0卡训练)  
    -    [FAQ4-8 检查是否签署了CLA](#FAQ4-8-检查是否签署了CLA)  
    -    [FAQ4-9 gdb调试常用命令](#FAQ4-9-gdb调试常用命令)  
    -    [FAQ4-10 代码冲突解决办法](#FAQ4-10-代码冲突解决办法)
    -    [FAQ4-11 常见的问题定位方法](#FAQ4-11-常见的问题定位方法)
-----------------------------------------------------------------------------------------------------------

## 1  ModelArts问题FAQ

### FAQ1-1-华为云登录
方式1：使用个人华为云账户登录，该方式较为简单，不在赘述。
方式2：IAM账户方式登录
打开[华为云登录链接](https://auth.huaweicloud.com/authui/login.html?service=#/login),在登录界面选择IAM账户登录，如下图示：
(![登录界面](https://images.gitee.com/uploads/images/2022/0223/175845_ef6568cc_7897911.png "login.png")), (![选择IAM账户](https://images.gitee.com/uploads/images/2022/0223/175858_5c9fcfb2_7897911.png "iam_login.png"))
登录之后，新账户在首次登录后会要求重置密码，根据提示操作即可。
进入华为云界面后，依次选择控制台--北京四
![输入图片说明](https://images.gitee.com/uploads/images/2022/0226/180405_166951cc_7897911.png "屏幕截图.png")

对于OBS服务客户端，使用同样的方式登录即可，登录完成后不需要创建“桶”，请使用华为工程师分配的桶。
![OBS客户端登录](https://images.gitee.com/uploads/images/2022/0223/180645_b97dcaba_7897911.png "屏幕截图.png")

#### 特别声明：登录账户，请和华为支持工程师联系获取。获取账户后，仅用于modelarts功能开发（modelarts/OBS/SWR服务），不要用于其他用途，否则造成的后果需要自行承担。

### FAQ1-2 如何进行代码调试

* 现象描述
在提交PR到Gitee后，会进行缺陷和规范扫描，如下图： 
![](https://images.gitee.com/uploads/images/2021/0317/173852_59998386_7863637.png "屏幕截图.png")
* 处理方法
在合并代码前，需要将所有的非误报类的告警都消除，误报类的请标记误报。 其中有一些pylint告警可根据实际情况进行屏蔽，如可在代码的相应位置添加如下代码：
    ```
    #pylint: disable=import-error
    #pylint: disable=too-many-lines
    #pylint: disable=too-many-arguments
    #pylint: disable=unused-variable
    #pylint: disable=unused-argument
    ```
    具体的pylint使用方法可在网上自行查找。 注意：请根据实际情况进行屏蔽，遵循最小化屏蔽原则，避免不合理的屏蔽，所有屏蔽的告警项都会统计记录。 

### FAQ1-3 如何使用适配训练脚本
使用场景
在ModelArts上使用创建训练作业进行训练前，上传适配好的训练脚本
操作步骤
参考链接：https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0240.html
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/112916_00cb274e_923381.png "屏幕截图.png")

### FAQ1-4 如何使用pytorch框架创建训练作业
在ModelArts上创建训练作业时需要选择训练框架，目前常用框架里面没有适配昇腾环境的pytorch镜像，需要采取自定义镜像的方式，但是新版的训练作业里面自定义镜像功能还未上线，解决办法如图所示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/171248_5dd574cb_8755417.png "c6123132-4ce9-48af-acc9-c09f097f950a.png")
剩下的操作与其他框架一样，详情参考modelarts指导文档：https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0238.html
备注：图片中的链接https://console.huaweicloud.com/modelarts/?region=cn-north-4#/trainingJobs/create/

### FAQ1-5 使用账号登录obs-browser客户端报用户名密码错误
* 现象描述
在使用创建的华为云账号登录obs-browser+时，提示“登录认证失败，账号或者密码错误”
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/201004_b112f1c6_923381.png "屏幕截图.png")
* 处理方法
需要创建一个IAM用户，用IAM用户登录obs browser+，参考链接：https://support.huaweicloud.com/usermanual-obs/obs_03_0122.html

### FAQ1-6 模型训练无法创建日志目录
* 现象描述
![Image description](https://images.gitee.com/uploads/images/2021/0916/161701_e3f0892e_7397423.png "屏幕截图.png")
模型训练需创建/tmp/log/trainging.log, 但是一直无法创建，导致训练过程无法追踪，训练无法正常运行
* 问题解决
![Image description](https://images.gitee.com/uploads/images/2021/0916/161821_ac2d090e_7397423.png "屏幕截图.png")
可以看到进入modelarts训练开始后，当前目录是/home/work，当前目录没有root权限，因此无法创建。修改为当前目录就可以创建成功了。
* tips
但是其他训练作业也有成功创建/tmp/log目录的，这个问题仍需进一步定位

### FAQ1-7 如何在ModelArts上安装软件依赖
使用场景
训练模型时需要引用依赖包
操作步骤
参考链接中的方式一：https://support.huaweicloud.com/modelarts_faq/modelarts_05_0063.html
![输入图片说明](https://images.gitee.com/uploads/images/2021/1026/153201_80e51a2e_923381.png "屏幕截图.png")


## 2 MindX SDK问题FAQ

### FAQ2-1 SDK pipeline配置错误

* 错误信息提示：

```
W0729 02:28:39.937480 231944 MxpiMetadataGraph.cpp:393] Node list(mxpi_objectpostprocessor0)does not exist.
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/162659_edd7e527_8174600.jpeg "screenshot_20210729100154056.jpg")

* 原因分析
由于插件配置错误，导致有些插件加载不成功：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/163552_f7138527_8174600.jpeg "1.jpg")

### FAQ2-2 如何开发SDK后处理代码
使用场景
当前后处理基类所采用的数据结构无法满足需求，可以新增后处理基类继承PostProcessBase，并写入新的数据结构。
操作步骤
参考链接：https://support.huaweicloud.com/ug-mfac-mindxsdk201/atlasmx_02_0070.html

### FAQ2-3 有没有TensorBase的操作例子或者说明文档
使用场景
开发后处理插件时需要对数据进行转换为TensorBase
操作步骤
参考链接：https://gitee.com/ascend/mindxsdk-referenceapps/blob/master/docs/%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99/TensorBase.md

### FAQ2-4 模型推理结果都一样
* 现象描述：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0907/090937_debda67d_9323271.png "屏幕截图.png")
* 原因分析：
  可能原因模型转换时output_type参数设置为FP16影响了推理精度，可将其设置为--output_type=FP32。
  ```
  atc --model=./cspdarknet53.air --framework=1 --output=./cspdarknet53 --input_format=NCHW --input_shape="actual_input_1:1,3,224,224" --soc_version=Ascend310 --insert_op_conf=./aipp.config --output_type=FP32
  ```
### FAQ2-5 运行程序时报申请内存失败
* 现象描述：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0910/100457_ae75ffc6_923381.png "屏幕截图.png")
* 原因分析：
一般是之前运行的程序资源或者上一次执行申请的内存资源没有释放

### FAQ2-6 如何获取SDK推理后的结果。
* 现象描述：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1030/151459_c09bc0e8_8833035.png "31f4426f-7e47-45e5-972e-2bced2984249.png")
有图上可看到推理已得到结果。
* 原因分析：
获取到指针里面的数据，如以下：
result = MxpiDataType.MxpiTensorPackageList()
result.ParseFromString(infer_result[0].messageBuf)
res = np.frombuffer(result.tensorPackageVec[0].tensorVec[0].dataStr, dtype=np.float32)

## 3 MxBase问题FAQ

### FAQ3-1 如何查看编译过程详细日志

* 现象描述
在执行编译时报错找不到头文件
![输入图片说明](https://images.gitee.com/uploads/images/2021/0701/214723_e666476f_923381.png "屏幕截图.png")
* 处理方法
在CMakeLists.txt添加打印消息查看详细日志
message(WARNING ${XXX})
![输入图片说明](https://images.gitee.com/uploads/images/2021/0701/214823_5f3367a0_923381.png "屏幕截图.png")

### FAQ3-2 编译MxBase推理二进制时报“Undefined reference to vtable”
* 现象描述
错误信息提示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/172431_f7a092f1_923381.png "屏幕截图.png")
* 可能原因分析
链接器linker需要将虚函数表vtable 放入某个object file，但是linker无法找到正确的object文件。这个错误常见于刚刚创建一系列有继承关系的class的时候，这个时候很容易忘了给base class的virtual function加上函数实现。
* 处理方法
为虚拟析构函数提供一个定义。

### FAQ3-3 CMakeList的编写和参数详解
使用场景
编写CMakeLists.txt配置cmake编译链接相关信息。
操作步骤
参考链接：https://blog.csdn.net/wfei101/article/details/77150234

### FAQ3-4 CMake无法找到OpenSSL库

* 现象描述
  错误信息提示：

```
-- Could NOT find Git (missing:  GIT_EXECUTABLE) 
ZLib include dirs: /usr/include
ZLib libraries: /usr/lib/arm-linux-gnueabihf/libz.so
Compiling with SSL support
CMake Error at /usr/local/share/cmake-2.8/Modules/FindPackageHandleStandardArgs.cmake:97 (message):
  Could NOT find OpenSSL, try to set the path to OpenSSL root folder in the
  system variable OPENSSL_ROOT_DIR (missing: OPENSSL_LIBRARIES
  OPENSSL_INCLUDE_DIR)
Call Stack (most recent call first):
  /usr/local/share/cmake-2.8/Modules/FindPackageHandleStandardArgs.cmake:291 (_FPHSA_FAILURE_MESSAGE)
  /usr/local/share/cmake-2.8/Modules/FindOpenSSL.cmake:313 (find_package_handle_standard_args)
  CMakeLists.txt:436 (find_package)
```

* 规避方法

```
apt-get install libssl-dev
```

### FAQ3-5 ModelInference报ret145000的解决办法

* 现象描述
  以ssd_resnet50_fpn模型为例，在针对单张图片进行推理时出现错误
  推理命令 ./ssd_resnet50_v1_fpn ./convert/ssd_resnet50_fpn.om ./test.jpg 
  错误信息提示：

```
...
E0817 14:08:52.223390  5084 ModelInferenceProcessor.cpp:394] [145000][Error code unknown] Model infer execute failed.
E0817 14:08:52.223418  5084 ModelInferenceProcessor.cpp:293] [145000][Error code unknown] Failed to execute model infer.
E0817 14:08:52.223433  5084 SSDResNet50V1Fpn.cpp:165] ModelInference failed, ret=145000.
E0817 14:08:52.223441  5084 SSDResNet50V1Fpn.cpp:202] Inference failed, ret=145000.
E0817 14:08:52.265486  5084 main_ssd.cpp:50] SsdDResNet50V1Fpn process failed, ret=145000.
...
```

* 排查方法
首先在[CANN用户手册](https://support.huaweicloud.com/aclpythondevg-cann502alpha3infer/atlaspyapi_07_0405.html)中找到aclError，对应ret=145000是什么错误，可以发现ACL_ERROR_GE_PARAM_INVALID = 145000，代表参数校验失败。
因此可以打印详细日志信息，首先配置环境变量
export ASCEND_GLOBAL_LOG_LEVEL=1
export ASCEND_SLOG_PRINT_TO_STDOUT=1
然后再执行一次推理命令，会生成如下日志
```
...
[ERROR] GE(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.514 [davinci_model.cc:3233]3331 CheckInputAndModelSize: ErrorNo: 145000(Parameter invalid.) Input size [614400] can not be smaller than op size [19660800] after 64-byte alignment
[ERROR] GE(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.543 [davinci_model.cc:3304]3331 UpdateIoTaskArgs: ErrorNo: 145000(Parameter invalid.) Check input size and model size failed, op[x]
[ERROR] GE(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.567 [davinci_model.cc:3249]3331 CopyModelData: ErrorNo: 145000(Parameter invalid.) [ZCPY] Update input data to model failed.
[ERROR] GE(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.641 [davinci_model.cc:3704]3331 NnExecute: ErrorNo: -1(failed) Copy input data to model failed. model id: 1
[ERROR] GE(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.665 [graph_loader.cc:223]3331 ExecuteModel: ErrorNo: 145000(Parameter invalid.) Execute model failed, model_id:1.
[ERROR] ASCENDCL(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.686 [model.cpp:700]3331 ModelExecute: Execute model failed, ge result[145000], modelId[1]
[ERROR] ASCENDCL(3331,ssd_resnet50_v1_fpn ):2021-08-17-12:45:06.623.707 [model.cpp:718]3331 aclmdlExecute: aclmdlExecute failed, result[145000], modelId[1]
...

```
仔细观察可以发现614400 * 32 = 19660800。排查发现ssd_resnet50_fpn.om在经过atc转换前的air文件在生成时指定了batch_size=32，而输入仅为一张图片，因此出现了参数校验错误。

* 解决方法
推理时输入的图片个数应与训练后生成的air文件指定的batch size相同，否则报错。


### FAQ3-6 ModelInference报ret507011的可能原因及解决办法

* 现象描述
  在使用mxbase进行推理时可能会遇到模型报错ret=507011（**该问题在sdk推理也可能存在**）
  错误信息提示：

```
...
E0819 14:12:39.905298 19079 ModelInferenceProcessor.cpp:394] [507011][Error code unknown] Model infer execute failed.
E0819 14:12:39.905325 19079 ModelInferenceProcessor.cpp:293] [507011][Error code unknown] Failed to execute model infer.
E0819 14:12:39.905344 19079 WdlBase.cpp:96] ModelInference failed, ret=507011.
E0819 14:12:39.905350 19079 WdlBase.cpp:199] Inference failed, ret=507011.
...
```

* 可能原因
目前已知两个可能原因：
1. 有人占用卡或者CANN环境没配置对，但是如果是使用的众智环境一般不会遇到这个问题，如果遇到可以在和当前使用者沟通了后重启机器（有可能是使用者退出容器时导致aicore等错误）
2. 可能是输入错误问题。

* 排查原因

首先在[CANN用户手册](https://support.huaweicloud.com/aclpythondevg-cann502alpha3infer/atlaspyapi_07_0405.html)中找到aclError，对应ret=507011是什么错误，可以发现ACL_ERROR_RT_MODEL_EXECUTE = 507011，代表模型执行失败。

模型执行失败可能是两个原因：1）模型转换有问题；2）输入数据有问题（例如不满足模型输入或者输入类型错误等等）
因此可以打印详细日志信息，首先配置环境变量
export ASCEND_GLOBAL_LOG_LEVEL=1
export ASCEND_SLOG_PRINT_TO_STDOUT=1
以pytorch wide&deep模型为例，执行一次mxbase推理命令，会生成如下日志
```
...
[ERROR] RUNTIME(19079,wdl):2021-08-19-14:12:39.904.908 [device_error_proc.cc:408]19082 ProcessCoreErrorInfo:The error from device(1), serial number is 16, there is an aicore error, core id is 0, error code = 0x10, error string = Illegal instruction, which is usually caused by unaligned UUB addresses. dump info: pc start: 0x1080801dc000, current: 0x1080801dc164, vec error info: 0x507f573, mte error info: 0x4d, ifu error info: 0x11ba2a131380, ccu error info: 0x737f413100262256, cube error info: 0x33, biu error info: 0x0, aic error mask: 0x65000200d000288, para base: 0x1080800e8000.
[INFO] RUNTIME(19079,wdl):2021-08-19-14:12:39.904.978 [device_error_proc.cc:607] 19082 ProcErrorInfo: finished to process device error info, retCode=0.
[INFO] RUNTIME(19079,wdl):2021-08-19-14:12:39.904.986 [engine.cc:937] 19082 ReportExceptProc: excptCallBack_ is null.
[INFO] RUNTIME(19079,wdl):2021-08-19-14:12:39.904.993 [stream.cc:903] 19082 TryDelRecordedTask: del public task from stream, stream_id=5, tailTaskId=1, delTaskId=1, head=2, tail=2
[INFO] RUNTIME(19079,wdl):2021-08-19-14:12:39.905.000 [logger.cc:1359] 19082 TaskFinished: device_id=0, stream_id=5, task_id=1, task_type=13,task_finish_num=217
[ERROR] RUNTIME(19079,wdl):2021-08-19-14:12:39.905.015 [task.cc:676]19082 PrintErrorInfo:Aicore kernel execute failed, device_id=0, stream_id=1, report_stream_id=5, task_id=143, fault kernel name=-1_0_1_Gather_19, ctx is NULL!.
...

```
注意到最后一行，ctx is NULL!!这里体现的意思是上下文没有内容(ctx也就是context)，并且这里的gather算子将会用作对某个离散特征在embedding_dict的映射作用（可以用netron查看onnx或者om以及源码中模型结构来对比），如果输入数据不正确，可能就会导致这个问题，因此在这个报错场景下应该去匹配输入数据是否有误。


## 4 其他问题FAQ

### FAQ4-1 查询推理服务器用了哪些卡

* 执行命令查看

```
if [ $(docker ps | wc -l) -gt 1 ]; then docker ps | grep -v CONT | 
awk '{print $1}' | xargs docker inspect --format='{{printf "%.5s" .ID}} {{range .HostConfig.Devices}}{{.PathOnHost}} {{end}}' | 
sort -k2; fi; echo ok
```

* 返回结果
![输入图片说明](https://images.gitee.com/uploads/images/2021/0625/151150_15cee51c_923381.png "屏幕截图.png")
前面是容器的id 后面是容器占用的卡，这里是两个容器，分别占用了0卡和1卡


### FAQ4-2 使用npu-device出现问题
1. 常见device错误50700 
* 错误信息提示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/173659_227ca778_7397423.jpeg "device-50700.jpg")
* 原因分析
在docker启动时挂载device与其他docker冲突，可执行FAQ4-1命令查看docker用卡情况并结合npu-smi info查看卡实际使用状态，与其他用户协商卡的使用时间

2. 推理时出现device错误 
* 错误信息提示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/174147_0e510240_7397423.jpeg "推理-device-error.jpg")
* 原因分析
推理时使用device, device_id需从0重新计，而在训练时device_id还是原始的device_id
比如启动docker时挂卡5
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/174551_d99cab7e_7397423.jpeg "dockerfile_device.jpg")
则在推理时，pipeline文件中device_id定义需为0：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/175245_82e244bd_7397423.jpeg "pipeline_change.jpg")
若为原始device_id:5,则会报错：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0806/174747_14bfe8e9_7397423.jpeg "推理pipeline_error.jpg")
在训练时device_id直接设为5即可。

### FAQ4-3 docker容器下gdb无法正常工作的解决办法
* 现象描述
错误信息提示：

```
warning: Error disabling address space randomization: Operation not permitted
Cannot create process: Operation not permitted
During startup program exited with code 127.
(gdb)
```

* 原因分析
linux 内核为了安全起见，采用了Seccomp(secure computing)的沙箱机制来保证系统不被破坏。它能使一个进程进入到一种“安全”运行模式，该模式下的进程只能调用4种系统调用（system calls），即read(), write(), exit()和sigreturn()，否则进程便会被终止。
* 处理方法
以docker run --security-opt seccomp=unconfined的模式运行container才能利用GDB调试。

### FAQ4-4 Python使用llib.urlopen打开https链接SSL报错CERTIFICATE_VERIFY_FAILED
* 现象描述
错误信息提示：

```
urllib.error.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:833)>
```

* 原因分析
Python 升级到 2.7.9 之后引入了一个新特性，当使用urllib.urlopen打开一个 https 链接时，会验证一次 SSL 证书，而当目标网站使用的是自签名的证书时就会抛出一个 urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:581)> 的错误消息
* 处理方法
[参考链接](https://blog.csdn.net/mid_Faker/article/details/109953646?utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.pc_relevant_baidujshouduan&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.pc_relevant_baidujshouduan)

### FAQ4-5 Pytorch框架训练开启Apex混合精度加速，并在完成训练后转onnx失败的解决办法
* 现象描述
在pytorch框架训练完成并存储模型的pth参数文件后，马上利用torch.onnx.export函数通过pth文件转onnx文件可能会失败。
错误信息提示：
```
···
File "/usr/local/ma/python3.7/lib/python3.7/site-packages/apex/amp/wrap.py", line 28, in wrapper
return orig_fn(*new_args, **kwargs)
File "/usr/local/ma/python3.7/lib/python3.7/site-packages/torch/nn/functional.py", line 1610, in linear
ret = torch.addmm(bias, input, weight.t())
File "/usr/local/ma/python3.7/lib/python3.7/site-packages/apex/amp/wrap.py", line 21, in wrapper
args[i] = utils.cached_cast(cast_fn, args[i], handle.cache)
File "/usr/local/ma/python3.7/lib/python3.7/site-packages/apex/amp/utils.py", line 113, in cached_cast
if cached_x.grad_fn.next_functions[0][0].variable is not x:
AttributeError: 'NoneType' object has no attribute 'next_functions'
```

* 原因分析
在调用amp.initialize初始化model和optimizer时，如果指定了opt_level为O1，则**有可能**引起这个问题。在指定opt_level="O1"后，amp会添加有关pytorch functions和Tensor methods的自动转换。而此时amp根本不应该直接更改模型（model）的参数或其它相关属性，换句话说，它们应该保持fp32。为了证明这点，可以load在训练过程中得到的checkpoint.pth.tar文件之后运行如下脚本：
```
for param in model.parameters():
    print(param.dtype)
```
之后会发现所有的参数均为float32类型。
onnx会记录graph内部的各种使用到的函数和其它数据类型。无论它们是什么类型，都将以float32和float16的混合方式执行，因为opt_level="O1"。
* 处理方法
因此，如果opt_level="O1"，并且需要创建onnx，但在禁用自动转换的情况下运行它：
```
with amp.disable_casts():
    dummy_input = torch.randn(1, 224, 224, 3).to(device)
    torch.onnx.export(model, dummy_input, './model.onnx', verbose=True)
```
[参考链接](https://github.com/NVIDIA/apex/issues/349)

### FAQ4-6 推荐模型在转onnx时报IndexError的解决方法
* 现象描述
以Pytorch-Wide&Deep为例，它的模型输入为N*M的float类型Tensor，其中N为batch size，M为特征维度（在criteo数据集为39），如果采用如下编码来转onnx模型文件，则会报错IndexError: index out of range in self。
```
dummy_input = torch.randn(size[1, 39])
torch.onnx.export(model, dummy_input, onnx_file_path, input_names=input_names, output_names=output_names,
                      opset_version=11)
```

* 原因分析
  - 某些推荐数据集中同时包含了稀疏特征和稠密特征，并且对系数特征进行了编码，稠密特征（即值为连续数值的特征）进行了归一化。稠密特征可以作为float类型直接输入作为模型输入，但是稀疏特征则必须先应映射至高维稠密的连续空间。以Criteo为例，它共包含了26个稀疏特征（C1,C2...,C26），每一个特征域都包含了不同个数的特征，例如C1共有1460个不同的特征值（假设定义sparse_feature_num=1460）。
  - 因此在模型中则会使用torch.nn.Embedding(sparse_feature_num, 4, sparse=sparse)为C1内编码为0-1459的稀疏特征映射到一个4维的连续向量中。因此如果简单地使用torch.randn来生成输入Tensor，生成的随机稀疏特征值可能就会边界溢出，例如对应C1特征域生成的稀疏特征编码为2000，则会导致torch.nn.Embedding找不到对应的embedding vector，则报IndexError: index out of range in self错误。

* 处理方法
为每个稀疏特征域C1-C26生成对应在固定[0,sparse_feature_num)范围内的随机编码，而稠密特征可以直接用torch.randn生成
```
···
sparse_nunique = [1460, 583, 10131227, 2202608, 305, 24, 12517, 633, 3, 93145, 5683, 8351593, 3194, 27, 14992,
                      5461306, 10, 5652, 2173, 4, 7046547, 18, 15, 286181, 105, 142572]
sparse_dummy_input = []
for nunique in sparse_nunique:
    sparse_dummy_input.append(torch.randint(0, nunique, size=[1, 1]))
    sparse_dummy_input = torch.cat(sparse_dummy_input, dim=-1)
dummy_input = torch.cat([sparse_dummy_input.float(), torch.randn(size=[1, 13])], dim=-1)

torch.onnx.export(model, dummy_input, onnx_file_path, input_names=input_names, output_names=output_names,
                  opset_version=11)
```
其它同时存在稀疏和稠密特征的数据集可以采用相同的方法，一定要注意统计稀疏特征编码的最大数量，不要溢界。

### FAQ4-7 pytorch模型1p训练时使用非0卡训练
关键修改点：
```
可直接修改CALCULATE_DEVICE：
CALCULATE_DEVICE = "npu:0"
也可通过传参的方式修改CALCULATE_DEVICE：
CALCULATE_DEVICE = "npu:{}".format(args.npu)
以下是单卡训练使用卡信息的关键配置点
torch.npu.set_device(CALCULATE_DEVICE)
model = model.to(CALCULATE_DEVICE)
criterion = nn.CrossEntropyLoss().to(CALCULATE_DEVICE)
```

* 现象描述：修改CALCULATE_DEVICE之后，出现以下问题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/173034_c487b5e4_7397423.png "pytorch换卡.png")

* 原因分析：
因为在不同的函数里调用了变量CALCULATE_DEVICE，但是修改并不是在函数外部实现的，因此会导致两次使用的变量CALCULATE_DEVICE不一致，最后出现错误
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/172435_c0a46bc3_7397423.png "屏幕截图.png")

* 处理方法：
```
global CALCULATE_DEVICE
CALCULATE_DEVICE = "npu:{}".format(args.npu)
```
这样对CALCULATE_DEVICE的改变才会全局生效。

### FAQ4-8 检查是否签署了CLA
参考链接：https://github.com/opensourceways/test-infra/blob/74797ffb3b99b56ffec9731177964c7e62e1d679/prow/gitee-plugins/cla/faq.md

### FAQ4-9 gdb调试常用命令
* 处理方法：

```
打开GDB调试：
SET(CMAKE_BUILD_TYPE "Debug")
SET(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g -ggdb")
SET(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")

gdb调试启动：
gdb yolov4_csp
设置超参：
set args ../data/test/ 1
容器调试需要：
set disable-randomization off
断点设置：
b main.cpp:96
打印信息：
p initParam
```

### FAQ4-10 代码冲突解决办法
个人仓库master代码冲突解决办法
注意：备份好自己的代码。
第一步：打开你的PR，找到你第一次提交的commit id,如果你是通过自己个人仓的master提交，执行git reset --hard 第一次提交的commit id。
第二步：执行git log 查看到第一次提交的前一次commit id,在一次执行git reset --hard 前一次commit id，此时你已经回退到你没有任何代码提交的版本。
第三步：现在需要你把主仓库的代码合并到你个人仓库上，请参考 https://www.cnblogs.com/eason-d/p/12761340.html,合并后完成后，
第四步：现在在把你本地的代码更新到个人仓库上，PR上会显示你无提交文件和记录，然后在本地把你的文件和.jenkins文件添加后，更新到个人仓库上完成，PR无异常。

个人仓库分支代码冲突解决办法
第一步：打开你的PR，找到你第一次提交的commit id，执行git check master ,切换到master后，执行git reset --hard 第一次提交的commit id。
第二步：执行git log 查看到第一次提交的前一次commit id,在一次执行git reset --hard 前一次commit id，此时你已经回退到你没有任何代码提交的版本。
第三步：现在需要你把主仓库的代码合并到你个人仓库上，请参考 https://www.cnblogs.com/eason-d/p/12761340.html,合并后完成后。
第四步：打开你的PR，找到你第一次提交的commit id，执行git check 个人分支,切换到个人分支后，执行git reset --hard 第一次提交的commit id。
第五步：执行git log 查看到第一次提交的前一次commit id,在一次执行git reset --hard 前一次commit id，此时你已经回退到你没有任何代码提交的版本。
第六步：执行git merge master,把自己master和个人分支合并。 
第七步：现在在把你本地的代码更新到个人仓库上，PR上会显示你无提交文件和记录，然后在本地把你的文件和.jenkins文件添加后，更新到个人仓库上完成，PR无异常。


### FAQ4-11 常见的问题定位方法
1.GE图的保存在进行atc转换时，添加环境变量export DUMP_GE_GRAPH=2，export DUMP_GRAPH_LEVEL=3，生成ge图,txt文件，pbtxt文件。
2.常用的环境变量，先设置debug日志级别，并对/root/ascend/log/plog下的日志进行备份，
export GLOBAL_LOG_LEVEL=3 
export TF_CPP_MIN_LOG_LEVEL=3
export SLOG_PRINT_TO_STDOUT=0
GLOBAL_LOG_LEVEL取值范围说明如下： 
 0：对应DEBUG级别。 
 1：对应INFO级别。 
 2：对应WARNING级别。 
 3：对应ERROR级别。 
 4：对应NULL级别。
SDK的日志控制环境变量（“/usr/local/sdk_home/mxManufacture/configf”中配置）
#note, 0:debug, 1:info, 2:warning, 3:error, 4:null(no output log), default(3)
global_level=1
配置生成coredump文件
ulimit -c unlimited
mkdir /var/log/npu/dump
chown -R HwHiAiUser:HwHiAiUser /var/log/npu/dump
echo "/var/log/npu/dump/core.%e%p" > /prfoc/sys/kernel/core_pattern
检查配置
ll /varl/log/npu/dump
cat /prfoc/sys/kernel/core_pattern
gdb python3.7 core*(dump文件）

