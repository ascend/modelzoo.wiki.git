# BNInference<a name="ZH-CN_TOPIC_0000001151592363"></a>

## 算子功能<a name="section1885015911372"></a>

自定义算子，BatchNorm算子在推理场景上映射的算子。

## 实现公式<a name="section1316191643812"></a>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/091857_efe18bbe_8394326.png "zh-cn_image_0000001151729383.png")

内部计算是将公式做了等价转换（![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/091908_7a1887b3_8394326.png "zh-cn_image_0000001151729409.png")两个值在host\_cpu上计算出来）：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/091922_fdd217e6_8394326.png "zh-cn_image_0000001151809519.png")

## 约束说明<a name="section0216313123910"></a>

支持数据类型：float16 和float32

