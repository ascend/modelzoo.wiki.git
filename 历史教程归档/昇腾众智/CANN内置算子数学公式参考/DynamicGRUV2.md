# DynamicGRUV2<a name="ZH-CN_TOPIC_0000001105272418"></a>

## 算子功能<a name="section1885015911372"></a>

-   功能：该算子实现标准GRU计算逻辑
-   输入：X、init\_h、weight、bias
-   输出：y、output\_h

## 实现公式<a name="section1316191643812"></a>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/091734_f8104549_8394326.png "zh-cn_image_0000001105272516.png")

## 约束说明<a name="section0216313123910"></a>

-   支持的门顺序：i、r、h\[默认\]和r、i、h
-   不支持多层、双向
-   X默认是pad之后的输入，不支持seq\_length处理

