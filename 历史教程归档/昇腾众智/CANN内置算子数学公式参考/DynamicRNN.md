# DynamicRNN<a name="ZH-CN_TOPIC_0000001151711583"></a>

## 算子功能<a name="section1885015911372"></a>

-   功能：该算子实现标准LSTM计算逻辑。
-   输入：X、init\_h、init\_c、weight、bias
-   输出：y、output\_h、output\_c

## 实现公式<a name="section1316191643812"></a>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/091630_7964b738_8394326.png "zh-cn_image_0000001105432004.png")

## 约束说明<a name="section0216313123910"></a>

-   四个门排列顺序固定为：i、c、f、o
-   不支持多层、双向
-   不支持peephole、cell\_clip和num\_projection
-   X默认是pad之后的输入，不支持seq\_length处理

