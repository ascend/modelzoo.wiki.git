## 1. ckpt文件转换为PB文件  
参考命令以及注释如下：
```
# CKPT_PATH: ckpt的路径，示例："./results_mbv3_74_10/model.ckpt-1200000"
# DST_FOLDER: 生成的pb模型的存储文件夹，示例："./ATC/pb_model"

def main(): 
    tf.compat.v1.reset_default_graph()
    # 定义网络的输入节点，输入大小与模型在线测试时一致
    inputs = tf.compat.v1.placeholder(tf.float32, shape=[None, 224, 224, 3], name="input")
    # 调用网络模型生成推理图，用法参考slim
    network_fn = nets_factory.get_network_fn(
        'mobilenet_v3_large',
        num_classes=1001,
        is_training=False) # inference
    logits, end_points = network_fn(inputs) # 获得模型的输出节点，此处为最后一个conv的输出，未经过softmax，inference时如果只需要index可以不使用
    # 重新定义网络的输出节点
    predict_class = tf.identity(logits, name='output') # 重新定义一个identity节点name方便后续freeze_graph时指向；因为后续离线推理时候我额外使用了tf.argmax，所以此处未定义。
    with tf.compat.v1.Session() as sess:
        #保存图，在 DST_FOLDER 文件夹中生成tmp_model.pb文件
        # tmp_model.pb文件将作为input_graph给到接下来的freeze_graph函数
        tf.io.write_graph(sess.graph_def, DST_FOLDER, 'tmp_model.pb')    # 通过write_graph生成模型文件
        freeze_graph.freeze_graph(
		        input_graph=os.path.join(DST_FOLDER, 'tmp_model.pb'),   # 传入write_graph生成的模型文件
		        input_saver='',
		        input_binary=False, 
		        input_checkpoint=CKPT_PATH,  # 传入训练生成的checkpoint文件
		        output_node_names='output',  # 与重新定义的推理网络输出节点保持一致
		        restore_op_name='save/restore_all',
		        filename_tensor_name='save/Const:0',
		        output_graph=os.path.join(DST_FOLDER, 'mobilenet_v3_large.pb'),   # 改为需要生成的推理网络的名称
		        clear_devices=False,
		        initializer_nodes='')
```

## 2. PB文件转换为OM文件
*接下来的部分均在华为云ECS上以`HwHiAiUser`用户运行。ECS的申请和部署[参考](https://gitee.com/ascend/modelzoo/wikis/%E5%9C%A8ECS%E4%B8%8A%E5%BF%AB%E9%80%9F%E5%88%9B%E5%BB%BA%E7%A6%BB%E7%BA%BF%E6%8E%A8%E7%90%86%E7%8E%AF%E5%A2%83?sort_id=3268587)*

*为了使用MindStudio的图形化界面，请使用支持X11转发的ssh客户端，例如MobaXterm(Windows), Linux系统上直接使用`ssh -X user@remotehost -p PORT`命令即可*

按照上述参考申请的ECS实例下应均在`/home/HwHiAiUser/MindStudio-ubuntu`下预安装好了MindStudio，运行
```
cd /home/HwHiAiUser/MindStudio-ubuntu/bin
bash MindStudio.sh
```
即可在本地开启图形化界面
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164611_a784facf_1100114.png "ms_main.png")
转换步骤如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164653_265c32da_1100114.png "ms_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164702_0f5cb606_1100114.png "ms_2.png")
注意input shape的batch维度设置为1（默认为-1）
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164712_377505cf_1100114.png "ms_3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164725_4a97dfb5_1100114.png "ms_4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164736_5b620947_1100114.png "ms_5.png")
转换成功后的om文件储存于`/home/HwHiAiUser/modelzoo/mobilenet_v3_large/device/mobilenet_v3_large.om`


## 3. 图像数据转换为bin文件
1. 使用Image读入JEPG文件为numpy数组；
2. 进行和在线推理时一致的图像处理操作（例如：减均值、归一化、调整大小、中心裁剪等）
3. 利用numpy的.tofile()函数将处理好的数组导出为bin文件，注意文件命名（例如：ILSVRC2012_val_00000001.JPEG --> ILSVRC2012_val_00000001.JPEG.bin）
```
for file in files:
    if file.endswith('.JPEG'):
        src = src_path + "/" + file
        print("start to process %s" % src)
        img_org = cv2.imread(src) # 读入数据
        res = pre_process_img(img_org) # 对原始图片进行需要的预处理
        res.tofile(dst_path+"/" + file+".bin") # 处理后的图片保存为bin文件
```

## 4. 使用msame测试模型离线推理
参考[msame_ccl_branch](https://gitee.com/ascend/tools/tree/ccl/msame)安装好msame，
相对于master分支，ccl分支目录作为输入，测试命令如下：

```
MODEL="/home/HwHiAiUser/mobilenetv3/om_model/device/mobilenet_v3_large.om" # MindStudio转换得到的om模型文件
INPUT="input" # 预处理完成后的bin文件所在目录
OUTPUT="output" # 推理结果存储目录

./msame --model $MODEL --input $INPUT --output $OUTPUT
```
![msame输出示例](https://images.gitee.com/uploads/images/2020/1230/164837_d541d82b_1100114.png "msame.png")


## 5. 对比精度
在上步中`$OUTPUT`目录下可以得到推理完成的结果（每个样本为1001维的数组），通过分别读入每个样本的推理结果数组并通过np.argmax函数即可以获得预测的index，再与该样本的gt进行一一对比便可以获得最后的精度结果。
```
files = os.listdir(OUTPUT)
for file in files:
    if file.endswith(".bin"):
        output_num += 1
        tmp = np.fromfile(OUTPUT+'/'+file, dtype='float32')
        inf_label = int(np.argmax(tmp)) + offset
        try:
            pic_name = str(file.split(".JPEG")[0])+".JPEG"
            print("%s, inference label:%d, gt_label:%d"%(pic_name,inf_label,label_dict[pic_name]))
            if inf_label == label_dict[pic_name]:
                check_num += 1
        except:
            print("Can't find %s in the label file: %s"%(pic_name,label_path))
top1_accuarcy = check_num/output_num
print("Totol pic num: %d, Top1 accuarcy: %.4f"%(output_num,top1_accuarcy))
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/1230/164921_3bf5a20f_1100114.png "acc.png")
