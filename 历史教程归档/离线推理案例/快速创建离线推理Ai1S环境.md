本文主要讲解如何在ecs上更快捷的 **基于已有的共享镜像**  创建昇腾 310推理 Ai1S环境。由于推理环境是需要安装ToolKit包，因此该环境同时也集成好了不少工具，包括profiling需要的msprof工具。如果想了解更多详细的细节介绍，可以参看这位大神的视频——[ **环境部署（计算加速型ECS）** ](https://education.huaweicloud.com/courses/course-v1:HuaweiX+CBUCNXA025+Self-paced/about?isAuth=0&cfrom=hwc)，讲解的非常棒！ 
:exclamation:另外，ECS服务器是消耗Modelarts代金券的，即使[弹性云服务器关机后还会计费](https://support.huaweicloud.com/ecs_faq/zh-cn_topic_0018124776.html?utm_campaign=ua&utm_content=ecs&utm_term=buyecs)，因此如果长时间不用了记得**删除它**，**删除它**，**删除它**。 

### <span id="目录">目录</span>
- [说明](#说明)
- [获取推理共享镜像](#获取推理共享镜像)
- [创建Ai1S推理服务器](#创建Ai1S推理服务器)
- [环境验证](#环境验证)
- [FAQ](#FAQ)

### <span id="说明">说明</span>
根据当前wiki指导，你将获取一个共享镜像，该镜像内置安装了ToolKit包和MindStudio集成开发环境(软件版本信息可能跟当前wiki提到的20.1版本不同)。你可以在华为云上创建一个属于你自己的弹性云服务器ECS，用于昇腾相关的开发和调试工作。

### <span id="获取推理共享镜像">获取推理共享镜像</span>
为什么标题说是快速创建推理环境呢？快就快在这里，我们不需要像视频——[环境部署（计算加速型ECS）](https://education.huaweicloud.com/courses/course-v1:HuaweiX+CBUCNXA025+Self-paced/about?isAuth=0&cfrom=hwc)那样自己从头搞一个宿主镜像环境。这里我们是站在前人的肩膀上，直接使用创建好并包含MindStudio工具的镜像。问题来了，如何才能获取到该镜像环境呢？分两种情况：
第一种情况，如果项目组同学都没有，则向华为方获取，大致步骤如下：
- 首先，向华为的项目经理或者接口人提供华为云账号，说明希望获取昇腾310环境的Ai1S推理镜像
- 其次，华为方会有对应的同事将该镜像通过共享的方式共享给你
- 然后，共享给你之后，你还需要 [**打开链接**](https://console.huaweicloud.com/ecm/?agencyId=0703e2ce96800ff21f35c01d3901ff44&region=cn-north-4&locale=zh-cn#/ims/manager/imageList/selfImage) 接收共享邀请
    ![接受邀请](https://images.gitee.com/uploads/images/2020/1218/113910_2f01ecec_1482256.png "屏幕截图.png")
- 最后，在后面创建ECS推理服务器时，你可以在 **镜像-共享镜像** 中看到已经给你共享的镜像。这里镜像的名字可能跟你的不一致，仅供参考。
    ![共享镜像](https://images.gitee.com/uploads/images/2020/1218/113149_d76e4f9e_1482256.png "屏幕截图.png")

第二种情况，如果项目组有同学已经获得过共享镜像，可以通过他分享获取，大致步骤如下：
- 复制共享镜像，并且假设取名为 copy_c75_share，名字自己随意取。
![复制镜像1](https://images.gitee.com/uploads/images/2020/1218/161430_41f1074b_1482256.png "屏幕截图.png")
![复制镜像2](https://images.gitee.com/uploads/images/2020/1218/162355_3f14c89b_1482256.png "屏幕截图.png")
- 点击 **确定** 后稍等几分钟完成后，可以选择右侧的**更多->共享**，输入华为云账号，分享给项目组的其他同学。
![共享镜像](https://images.gitee.com/uploads/images/2020/1218/162215_51bf533f_1482256.png "屏幕截图.png")

### <span id="创建Ai1S推理服务器">创建Ai1S推理服务器</span>
- 首先，登入[ **云服务器控制台** ](https://console.huaweicloud.com/ecm/?agencyId=0703e2ce96800ff21f35c01d3901ff44&region=cn-north-4&locale=zh-cn#/dashboard) 购买弹性云服务器。

    ![购买ecs入口](https://images.gitee.com/uploads/images/2020/1218/101313_42d8853b_1482256.png "购买ecs入口.png")

    计费模式选择 **按需计费** ，区域选择 **华北-北京4** ，CPU架构选择 **X86** ，规格选择 **AI加速型** 中的 **ai1s.large.4**。

    ![购买配置1](https://images.gitee.com/uploads/images/2020/1218/102945_a00f327c_1482256.png "屏幕截图.png")

    镜像选择**共享镜像** ，就是刚开始第一节**获取推理共享镜像**拿到的。系统盘旁边的数据盘根据自己需要是否增加数据盘。当前提供的C75版本推理镜像系统盘有100G，根据各自模型需求看是否可放下推理数据。

    ![共享镜像和系统盘](https://images.gitee.com/uploads/images/2020/1218/103351_8483e407_1482256.jpeg "2.jpg")

- 进入到下一步，可进行网络，弹性公网IP( **按流量计费** )，带宽大小的申请，没有特别要注意的地方，可参照如下截图。

    ![弹性公网IP](https://images.gitee.com/uploads/images/2021/0114/195221_86bb1385_1482256.png "弹性公网IP.png")

- 再进入到下一步，可设置 **云服务器名字** 和 **服务器的root密码** 。此处名字我取得是 **ecs-infer** ，此处特别要注意，该密码必须记住，后面登入服务器时需要使用。

    ![云服务器名字和root密码](https://images.gitee.com/uploads/images/2020/1218/103808_3567bdff_1482256.jpeg "4.jpg")

- 最后进行配置的确认和最终的购买，并返回查看刚刚购买的ECS推理服务器。Here，我们可以看到分配的公网IP，后面我们可以使用这个IP进行SSH登入使用了。

    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/104136_d62a6446_1482256.jpeg "6.jpg")

### <span id="环境验证">环境验证</span>
这里需要注意了，我们不要在控制台界面里点击**远程登入** 。因为MindStudio使用到了图形界面，我们转向 MobaXterm 软件，里面可以配置和开启 X Server。MobaXterm软件可以去[ **官方网址中下载** ](https://mobaxterm.mobatek.net/download-home-edition.html)，目前为了方便大家，现在OBS上共享一份[ **下载地址链接** ](https://shared-data.obs.cn-north-4.myhuaweicloud.com:443/MobaXterm.rar?AccessKeyId=TALZSLBPEUQOGIOHMQNU&Expires=1639363896&Signature=Ftu6eD0me1Swi6R9An1uNc5ssCk%3D)。

在MobaXterm上使用ECS分配的公网IP地址进行远程登入，里面菜单栏可以看到X Server 默认是开启的。然后在当前root登入状态下，使用命令`passwd HwHiAiUser`修改HwHiAiUser密码（ **记住这个修改的密码** ）。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/105550_b6c88129_1482256.png "屏幕截图.png")

好，进入之后，可能很多人就迫不及待的想去路径 _/home/HwHiAiUser/MindStudio-ubuntu/bin_ 下启动 MindStudio了，这里就需要额外的注意。如果你之前是用root用户登入进去的，你切换到路径 _/home/HwHiAiUser/MindStudio-ubuntu/bin_ 下是启动不了MindStudio的，并且即使通过su切换到HwHiAiUser用户下启动也会失败。 

这个为什么呢？由于环境内置的MindStudio预置的安装用户是 HwHiAiUser，所以我们必须得进行切换。方法有两种：
-  **方法1** ：在当前root登入状态下，使用如下命令切换到HwHiAiUser用户。

    `ssh -X HwHiAiUser@124.70.60.174  【此处的IP改成ECS上自己申请的】`

-  **方法2** ：在MobaXterm登入界面时，直接使用HwHiAiUser用户名登入，**不使用root登入**。

    ![使用HwHiAiUser登入](https://images.gitee.com/uploads/images/2020/1218/110724_74f20e52_1482256.png "屏幕截图.png")

OK，登入之后我们在路径 _/home/HwHiAiUser/MindStudio-ubuntu/bin_ 下启动 MindStudio。 

![启动MindStudio](https://images.gitee.com/uploads/images/2020/1218/110927_eab51ef3_1482256.png "屏幕截图.png")

基于ECS推理环境的赋能培训介绍可移步到[ **社区链接** ](https://bbs.huaweicloud.com/forum/thread-83283-1-1.html)中对应的模块进行学习，在此就不过多的描述。
![推理赋能](https://images.gitee.com/uploads/images/2020/1218/111539_45d0b604_1482256.png "屏幕截图.png")

### <span id="FAQ">FAQ</span>
- msprof性能Proiling工具路径
    ```
    /home/HwHiAiUser/Ascend/ascend-toolkit/latest/x86_64-linux/toolkit/tools/profiler/profiler_tool/analysis/msprof
    ```