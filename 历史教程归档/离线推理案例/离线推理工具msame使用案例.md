开始之前，相信您已经了解离线推理的基本概念和原理，准备好了Ascend310推理环境，安装好Cann软件，下面开始我们的离线推理之旅：
### 1、编译msame离线推理工具
[msame工具](https://gitee.com/ascend/tools/tree/master/msame)
工具支持源码编译，或者使用者自行添加或者修改代码，使用者重新编译
进入msame目录
```
cd $HOME/AscendProjects/tools/msame/
```
设置环境变量DDK_PATH、NPU_HOST_LIB
```
export DDK_PATH=/home/HwHiAiUser/Ascend/ascend-toolkit/20.10.0.B023
export NPU_HOST_LIB=/home/HwHiAiUser/Ascend/ascend-toolkit/20.10.0.B023/acllib/lib64/stub
```
运行编译脚本
```
./build.sh g++ $HOME/AscendProjects/tools/msame/out
```
第一个参数指定编译器，由运行环境决定。  
第二个参数指定工具生成的目录，填相对路径的话，是相对out目录。

#### 注意事项
运行工具的用户在当前目录需要有创建目录以及执行工具的权限，使用前请自行检查。

### 2、制作输入数据bin文件
预处理要和训练模型时保持一致，数据类型要和生成om时设置的参数一致，比如om要求输入fp32,则生成的bin文件类型也要保持相同，否则会运行失败，或者精度异常。

### 3、开始推理
工具为命令行的运行方式，例如推理单个bin文件：
```
./msame --model /home/HwHiAiUser/ljj/colorization.om --input /home/HwHiAiUser/ljj/colorization_input.bin --output /home/HwHiAiUser/ljj/AMEXEC/out/output1 --outfmt TXT --loop 2
```
推理文件夹下所有bin文件：
```
./msame --model /home/HwHiAiUser/ljj/colorization.om --input /home/HwHiAiUser/ljj/ --output /home/HwHiAiUser/ljj/AMEXEC/out/output1
```
如果有多个输入，需要用**英文逗号**隔开，注意逗号两边不能有空格；多输入为目录时，需要保持多个目录下文件名相同。\
例如：./msame --model /home/HwHiAiUser/ljj/colorization.om --input dir1/,dir2/,dir3/ --output \
bin文件在三个目录dir1、dir2、dir3下命名相同

### 4、后处理
利用输出的bin文件，计算精度

### 5、完整流程参考
[efficientnet-B8离线推理](https://gitee.com/ascend/modelzoo/blob/master/contrib/Research/cv/efficientnet-b8/ATC_efficientnet-b8_tf_nkxiaolei/README.MD)

### 6、常见问题
1、推理失败：查看输入的shape和类型是否正确，和转换om时参数是否一致，device是否被其他人占用

可以在`$home/ascend/log`目录下可以获取到`device`和`plog`的`log`。`msame`执行时候的报错可以在`plog`目录下的`.log`文件中查找到。如果`.log`文件过多，可以先删除，再执行`msame`进行推理得到报错，最后取`plog`目录下查看最新的log

下面是`ECS Ai1S`环境上的一个例子：

通过查看`log`发现`om`模型需要4个输入数据，但是只提供了3个输入数据，所以报错
```shell
HwHiAiUser@ecs-5491:~/ascend/log$ pwd
/home/HwHiAiUser/ascend/log
HwHiAiUser@ecs-5491:~/ascend/log$ tree .
.
├── device-0
│   └── device-23039_20210605213413324.log
└── plog
    └── plog-23039_20210605213411967.log
HwHiAiUser@ecs-5491:~/ascend/log$ cat plog/plog-23039_20210605213411967.log
...
[ERROR] GE(23039,msame):2021-06-05-21:34:12.280.621 [/home/jenkins/agent/workspace/Compile_GraphEngine_Centos_X86/graphengine/ge/graph/load/model_manager/davinci_model.cc:3338]23039 UpdateIoTaskArgs: ErrorNo: -1(failed) Verify input data num failed: model requires 4, but user actually feeds 3
[ERROR] GE(23039,msame):2021-06-05-21:34:12.280.626 [/home/jenkins/agent/workspace/Compile_GraphEngine_Centos_X86/graphengine/ge/graph/load/model_manager/davinci_model.cc:3303]23039 CopyModelData: ErrorNo: 145000(Parameter invalid.) [ZCPY] Update input data to model failed.
[ERROR] GE(23039,msame):2021-06-05-21:34:12.280.630 [/home/jenkins/agent/workspace/Compile_GraphEngine_Centos_X86/graphengine/ge/graph/load/model_manager/davinci_model.cc:3753]23039 NnExecute: ErrorNo: -1(failed) Copy input data to model failed. model id: 1
[ERROR] GE(23039,msame):2021-06-05-21:34:12.280.634 [/home/jenkins/agent/workspace/Compile_GraphEngine_Centos_X86/graphengine/ge/graph/load/graph_loader.cc:232]23039 ExecuteModel: ErrorNo: 145000(Parameter invalid.) Execute model failed, model_id:1.
[ERROR] ASCENDCL(23039,msame):2021-06-05-21:34:12.280.638 [../../../../../acl/model/model.cpp:687]23039 ModelExecute: Execute model failed, ge result[145000], modelId[1]
[ERROR] ASCENDCL(23039,msame):2021-06-05-21:34:12.280.642 [../../../../../acl/model/model.cpp:705]23039 aclmdlExecute: aclmdlExecute failed, result[145000], modelId[1]
...
```

2、精度异常：一般根据经验来说，问题大多数出在数据预处理和后处理环节，预处理要和原始模型的训练对齐，om里面的输入输出数据类型也要对齐