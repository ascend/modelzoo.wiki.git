 **Step1:训练模型转换PB文件**  
模型输入

```
    input_ids = tf.placeholder(tf.int32, [None, 384], "input_ids")
    input_mask = tf.placeholder(tf.int32, [None, 384], "input_mask")
    segment_ids = tf.placeholder(tf.int32, [None, 384], "segment_ids")
    p_mask = tf.placeholder(tf.int32, [None, 384], "p_mask")

```
模型的输出

```
    start_top_log_probs, start_top_index = tf.nn.top_k(start_log_probs, k=start_n_top)
    end_top_log_probs = tf.reshape(end_top_log_probs,[-1, start_n_top * end_n_top])
    end_top_index = tf.reshape(end_top_index, [-1, start_n_top * end_n_top])
    cls_logits = tf.squeeze(cls_logits, -1)
```
接下来按照[示例代码](https://support.huaweicloud.com/mprtg-A800_9000_9010/atlasprtg_13_0042.html)的做法，需要使用freeze_graph.freeze_graph函数来对冻结图 

```
    tf.train.write_graph(sess.graph_def, './pb_albert_model', 'model.pb')
    freeze_graph.freeze_graph(
            input_graph='./pb_albert_model/model.pb',
            input_saver='',
            input_binary=False,
            input_checkpoint=ckpt_path,
            output_node_names="end_logits/TopKV2,end_logits/Reshape_1,end_logits/Reshape_2,answer_class/Squeeze",
            restore_op_name='save/restore_all',
            filename_tensor_name='save/Const:0',
            output_graph='./pb_albert_model/albert_base.pb',
            clear_devices=False,
            initializer_nodes='')
```  
其中tf.train.write_graph将你定义的网络结构保存到.pb文件中，在文件里面可以看到网络的各个节点。freeze_graph最重要的参数就是output_node_names，它需要的是输出节点的名字，至于这些名字是怎么找的，我是通过在tf.train.write_graph保存的model.pb文件里面找到的:

![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/144044_dc4693b5_8147558.png "WeChat6e25921335efe609353d1fab0aba2f33.png")

一般来说需要的输出都是对应着model.pb的最后一个节点。上图中的answer_class/Squeeze对应的就是最后一个输出cls_logits。由于albert测squad是多输出，因此就多保存了几次model.pb，每次只保留一个输出。至于为什么所需要的输出是5个，但是却只有4个节点，是因为TopKV2这个节点中包含了两个输出start_top_log_probs, start_top_index  
保存之后，就可以对pb模型进行转换。

 **Step2:PB文件转OM文件**  
虽然官网给的[转换例子](https://support.huaweicloud.com/usermanual-mindstudioc73/atlasmindstudio_02_0047.html)是使用mind studio进行转换，但是我看到了附带的pdf文档里面有使用命令行进行转换，并且我又在服务器上发现了atc的命令，因此就偷了一个懒，没有安装mind studio，直接用命令行进行转换。
```
export PATH=/usr/local/python3.7.5/bin:$PATH
export PYTHONPATH=/usr/local/Ascend/ascend-toolkit/latest/atc/python/site-packages/te:$PYTHONPATH
export LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/atc/lib64:${LD_LIBRARY_PATH}

/usr/local/Ascend/ascend-toolkit/latest/atc/bin/atc --model=./albert_base.pb --framework=3 --output=./albert_base --soc_version=Ascend910 \
        --input_shape="input_ids:1,384;input_mask:1,384;segment_ids:1,384;p_mask:1,384" \
        --log=info \
        --out_nodes="end_logits/TopKV2:0;end_logits/TopKV2:1;end_logits/Reshape_1:0;end_logits/Reshape_2:0;answer_class/Squeeze:0"
```
上述命令中重要的有两点，第一点是要设置input_shape, batch需要设置为1，另外一点就是要具体指定out_nodes的输出，正如在上述所说，TopKV2节点对应与两个输出TopKV2:0和TopKV2:1,一个是topk的概率，另一个是topk得到的index。在这个案例中，输出的概率和index在之后的精度测试中都需要，因此两个节点的输出都要，此外，对于单输出的网络节点，也需要加上:0，不然会报错。

**Step3:数据转换bin文件**
数据转换成bin文件又多种方式，一种就是使用numpy.array的tofile函数，注意，如果是多条测试数据的多输入，那么最好将输入保存在不同的文件夹中，每个文件夹中包含多条测试样例，相同测试样例的不同输入文件夹下的文件名相同，如下：

```
    for idx in .....:
        input_id.tofile("./input_ids/{0:05d}.bin".format(idx))
        input_mask.tofile("./input_masks/{0:05d}.bin".format(idx))
        segment_id.tofile("./segment_ids/{0:05d}.bin".format(idx))
        p_mask.tofile("./p_masks/{0:05d}.bin".format(idx))
```

**Step4:使用msame进行离线打通**
按照[msame](https://gitee.com/ascend/tools/tree/master/msame)的安装过程安装好msame，这里我使用的是[ccl分支的msame](https://gitee.com/ascend/tools/tree/ccl/msame)因为这个版本可以读取多测试样例的多目录输入，安装方式相同。安装完后运行msame，不同的目录中间用,隔开

```
input_id_path=/path/to/input/input_ids
input_mask_path=/path/to/input/input_masks
segment_id_path=/path/to/input/segment_ids
p_mask_path=/path/to/input/p_masks
ulimit -c 0
./msame --model /path/to/om_model/albert_base.om --input ${input_id_path},${input_mask_path},${segment_id_path},${p_mask_path} --output /path/to/output --outfmt TXT
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/153600_a137f6fb_8147558.png "WeChat4540d0f7afaaab38de13f2b28b0db7a5.png")

**Step5:精度对比**
由于在上述的推理过程中得到的结果是txt格式，因此，只需要把得到的输出读入list，在使用原先的测试精度的文件进行测试即可，结果如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/154551_8c732368_8147558.png "WeChat622d788104ba7181ee738830eaecb78d.png")