1、网络中有with tf.device('/cpu:0')等算子，记得直接删除哦，这类算子把网络指定到cpu或者gpu设备执行，会导致迁移到NPU失败：
   参考inception网络报错：
   ![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110910_56acd06f_7971653.png "屏幕截图.png")

2、session.run方式迁移时有的同学遇到如下错误：
   ![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/111529_f3a3f802_7971653.png "屏幕截图.png")
   解决： config.gragh_options写错，应该是 config.graph_options
    **划重点：** ：
   迁移过程中只有from npu_bridge.estimator import npu_ops是和NPU有关系，加载NPU优化器，其他脚本都为TensorFlow原生功能，如果遇到类似算子报错，请同学们仔细检查下脚本，先不要怀疑NPU哦
   
```
   from npu_bridge.estimator import npu_ops
   #如下配置全部为TensorFlow原生功能
   from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig
   config = tf.ConfigProto()
   custom_op = config.gragh_options.rewrite_options.custom_optimizers.add()
   custom_op.name = "NpuOptimizer"
   custom_op.parameter_map["use_off_line"].b = True # 必须显式开启，在昇腾AI处理器执行训练
   config.graph_options.rewrite_options.remapping = RewriterConfig.OFF  # 必须显式关闭remap
```
3、estimator迁移方案中讲述到需要处理：dataset = dataset.batch(batch_size, **drop_remainder=True** )，那么网络中实在没有这个咋办呢？例如下面的脚本
   不妨先把batch_size设置为1，先缓解这个问题；或者保证自己的数据集是成整batch的，数据集个数可以被batchsize数整除。

```
 # Build the Estimator
    model = NPUEstimator(model_fn,config=npu_config)
    
    # Define the input function for training
    input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'images': mnist.train.images}, y=mnist.train.labels,
        batch_size=batch_size, num_epochs=None, shuffle=True)
    # Train the Model
    model.train(input_fn, steps=num_steps)
```

4、迁移的参数说明
   
        custom_op.parameter_map["mix_compile_mode"].b =  True  设置全下沉
        custom_op.parameter_map["precision_mode"].s = tf.compat.as_bytes("allow_mix_precision")  设置混合精度