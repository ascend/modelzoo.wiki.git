**背景：** 昇腾910 Tensorflow版本暂不支持动态shape输入，因此需要对训练脚本进行修改，采用静态shape方案进行训练

**动态shape修改调试**

约束：WideDeep embedding规模较小，建议在178W ID数量以下，适用于较小数据集（否则内存不足）。如果embedding规模很大，建议将embedding部署到Host端，或者使用动态shape版WideDeep。

**修改方法：**

1.删除tf.unique行及相关行（动态shape）

此时数据将没有去重功能，将出现重复计算，另外将导致unsortedsegmentsum直接在完整的embedding上进行更新。在多卡情况下，unsortedsegmentsum输出的维度与完整的embedding维度一样，在这种情况下，Allreduce的耗时与规模与embedding的维度成正比。所以，在embedding较大的情况下，Allreduce耗时将显著增加。

```
self.id_uniq_u_, self.id_uniq_inver_all_ = tf.unique(tf.reshape((self.id_hldr_uniq_all-config.common_feat_num), [-1]), out_idx=tf.dtypes.int32)
self.id_uniq_u = self.id_uniq_u_ 
self.id_uniq_inver_all = tf.reshape(self.id_uniq_inver_all_, [-1, config.general_dim])
self.id_uniq_inver_ori = tf.gather(self.id_uniq_inver_all, indices=self.batch_indices)
self.id_uniq_inver = tf.reshape(self.id_uniq_inver_ori, [-1, self.dim_unique])
self.id_uniq_local_mask = tf.where(tf.equal(tf.cast(get_rank_id(), dtype=tf.int32), self.id_uniq_u%get_rank_size())) 
self.id_uniq_local = tf.gather_nd(self.id_uniq_u, self.id_uniq_local_mask)       
self.id_uniq_local_ = tf.reshape(self.id_uniq_local, [-1, 1])    
self.id_uniq_owner = self.id_uniq_local // get_rank_size()
self.index_all = tf.reshape(tf.range(0,tf.shape(self.id_uniq_u)[0]), [-1,1])
index_all = self.index_all
self.uniq_index = self.id_uniq_local_mask #tf.gather_nd(index_all, self.id_uniq_local_mask)
self.uniq_index = tf.gather_nd(index_all, self.id_uniq_local_mask)

```

2.8卡情况下unsortedsegmentsum特殊处理

增加以下的135~146行代码，实现在unsortedsegmentsum之后才进行Allreduce，否则将无法收敛。

```
        if rank_size > 1:
            avg_grads_w = []
            avg_grads_d = []
            for grad, var in grads_w:
                avg_grad = hccl_ops.allreduce(grad, "sum") if grad is not None else None
                avg_grads_w.append((avg_grad, var))
            for grad, var in grads_d:
                avg_grad = hccl_ops.allreduce(grad, "sum") if grad is not None else None
                avg_grads_d.append((avg_grad, var))
            apply_gradient_op_w = self.wide_ptmzr.apply_gradients(avg_grads_w)
            apply_gradient_op_d = self.deep_optimzer.apply_gradients(avg_grads_d)
            self.train_op = tf.group(apply_gradient_op_d, apply_gradient_op_w)
        else:
            self.train_op = tf.group(self.deep_optimzer, self.wide_ptmzr)
```
