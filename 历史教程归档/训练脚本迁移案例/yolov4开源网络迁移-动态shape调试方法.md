**背景：** 昇腾910 Tensorflow版本暂不支持动态shape输入，因此需要对训练脚本进行修改，采用静态shape方案进行训练

**动态shape修改调试**

**原生实现方式：**

开源训练代码，网络输入端图片是None x None（非固定尺度，即动态shape输入）

**修改方法：**

1.修改主训练流程（train.py）

数据预处理后，在网络输入端，把全部输入图片resize（固定）到608 x 608。如图：


```
def valid_shape(*x):
    image, y_true_13, y_true_26, y_true_52, gt_box_13, gt_box_26, gt_box_52 = x
    y_true = [y_true_13, y_true_26, y_true_52]
    gt_box = [gt_box_13, gt_box_26, gt_box_52]

    # npu modified
    if args_input.mode == 'single':
        image.set_shape([args.batch_size, args.img_size[0], args.img_size[1], 3])
        y_true[0].set_shape([args.batch_size, 13, 13, 3, 86])
        y_true[1].set_shape([args.batch_size, 26, 26, 3, 86])
        y_true[2].set_shape([args.batch_size, 52, 52, 3, 86])
    elif args_input.mode == 'multi':
        image.set_shape([args.batch_size, args.img_size[0], args.img_size[1], 3])
        y_true[0].set_shape([args.batch_size, 19 * 1, 19 * 1, 3, 86])
        y_true[1].set_shape([args.batch_size, 19 * 2, 19 * 2, 3, 86])
        y_true[2].set_shape([args.batch_size, 19 * 4, 19 * 4, 3, 86])

    gt_box[0].set_shape([args.batch_size, 1, 32, 4])
    gt_box[1].set_shape([args.batch_size, 1, 64, 4])
    gt_box[2].set_shape([args.batch_size, 1, 128, 4])

    image = color_jitter(
        image, brightness=0.125, contrast=0.5, saturation=0.5, hue=0.05)

    return image, y_true_13, y_true_26, y_true_52, gt_box_13, gt_box_26, gt_box_52
```


2.修改数据读取（./utils/data_utils.py）

静态shape训练需要把训练图片全部padding 到608x608，如果是动态shape输入训练，把./utils/data_utils.py中，如图所示代码段注释即可。

```
if mode == 'train' and iter_cnt >= IterControl and multi_scale:
        cav = np.zeros((608, 608, 3), dtype=np.float32) + 0.5
        true_h, true_w, c = img.shape
        cav[:true_h, :true_w, :] = img
        img = cav.astype(np.float32)
        img_size = [608, 608]
        print('padding to 608, 608 from %d, %d'%(true_h, true_w))
```
