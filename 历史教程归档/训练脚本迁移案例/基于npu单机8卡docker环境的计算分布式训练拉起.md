前期得到不同npu卡对应的device_id

### 准备工作

得到npu卡对应的device_id,device_ip 和 本机的ip地址
执行cat /etc/hccn.conf得到不同npu卡的device_id

![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/104107_414bf850_8394326.png "查看当前任务使用的device-id号.png")

`device_id`就是这里面的`address_*`。其中0卡对应`address_0`

本机ip地址： 执行`ifconfig` 得到本机的ip，可以看到有很多ip，选择物理机的ip即可。这里使用的是物理机对外的ip。在ranktable上会用到，但是对于单机影响不大。

### 准备docker环境脚本

本示例dockers开启使用两张卡(`0卡和1卡`)进行训练拉起，如需要开启8卡进行训练，复用拉起两张卡的逻辑即可，在dockers脚本里面相对添加


```
--device=/dev/davinci1  
--device=/dev/davinci0
```

总的docker启动脚本如下

`docker run -it -v /home/train_use/zhegongda:/data --device=/dev/davinci1 --device=/dev/davinci_manager --device=/dev/devmm_svm --device=/dev/davinci0  -v /var/log/npu/conf/slog/slog.conf:/var/log/npu/conf/slog/slog.conf -v /var/log/npu/slog/container/5:/var/log/npu/slog -v /var/log/npu/profiling/container/5:/var/log/npu/profiling -v /var/log/npu/dump/container/5:/var/log/npu/dump -v /var/log/npu/docker_slog_5:/usr/slog --device=/dev/hisi_hdc -w /data tf:x86-201rc1 bash`

需要添加什么卡便需要在dockers脚本里面配置--device的相关参数


```
docker 运行参数解释  

--device 添加设备 
-v  宿主机与docker虚拟机之间的文件(夹)映射  
-w  启动的工作路径 
tf:x86-201rc1 这个是docker里面的镜像 不用改
bash 启动/bin/bash脚本 
```

### 配置 ranktable 文件

开启0卡和1卡的ranktable脚本示例


```
{
    "server_count": "1",
    "server_list": [
        {
            "device": [
                {
                    "device_id": "0",
                    "device_ip": "192.168.10.1",
                    "rank_id": "0"
                },
                {
                    "device_id": "1",
                    "device_ip": "192.168.20.1",
                    "rank_id": "1"
                }
            ],
            "server_id": "183.129.171.130"
        }
    ],
    "status": "completed",
    "version": "1.0"
}
```

相关解释


```
// hccl_1p_3.json
{
    "server_count": "1",//这个单机上就为1 
    "server_list": [ // 配置需要启动的卡  
        {
            "device": [
                {
                    "device_id": "0",// 0 卡
                    "device_ip": "192.168.10.1",// 准备工作中的
                    "rank_id": "0"// rank的标识，rankID从0开始
                },
                {
                    "device_id": "1",
                    "device_ip": "192.168.20.1",
                    "rank_id": "1"
                }
            ],// 如果需要多张卡 继续补全即可
            "server_id": "183.129.171.130"//这个server_id对应的是物理机的ip  我是用的是该主机对外的ip地址，使用内部ip应该也没事，单机应该不影响
        }
    ],
    "status": "completed",
    "version": "1.0"
}
```

### 准备启动脚本


```
set -x #linux 基本配置   执行指令后，会先显示该指令及所下的参数
#export POD_NAME=another0 # 这一行需要注释掉 
# ================= 默认路径配置 ===============================#
export install_path=/usr/local/Ascend/nnae/latest
export LD_LIBRARY_PATH=/usr/local/Ascend/driver/lib64/common/:/usr/local/Ascend/driver/lib64/driver:/usr/local/Ascend/add-ons/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${install_path}/fwkacllib/lib64:$LD_LIBRARY_PATH
export PYTHONPATH=${install_path}/fwkacllib/python/site-packages:${install_path}/fwkacllib/python/site-packages/auto_tune.egg:${install_path}/fwkacllib/python/site-packages/schedule_search.egg:$PYTHONPATH
export PATH=${install_path}/fwkacllib/ccec_compiler/bin:${install_path}/fwkacllib/bin:$PATH
export PYTHONPATH=/usr/local/Ascend/tfplugin/latest/tfplugin/python/site-packages:${install_path}/fwkacllib/python/site-packages/hccl:$PYTHONPATH
export ASCEND_OPP_PATH=/usr/local/Ascend/nnae/latest/opp
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib/x86_64-linux-gnu
# ================= 默认路径配置 ===============================#

export SOC_VERSION=Ascend910
export HCCL_CONNECT_TIMEOUT=200 # 超时设置 


export RANK_TABLE_FILE=./hccl_lp_3.json # 配置的ranktable文件   

export JOB_ID=10086 # job_id
export PRINT_MODEL=1 # 打印图 一般不需要
export RANK_SIZE=2 #  注意这里面的RANK_SIZE与要启动的卡数目相对应
#export DUMP_GE_GRAPH=2

ulimit -c 0 #linux 基本设置

execpath=${PWD}
/usr/local/Ascend/toolkit/bin/adc --host 127.0.0.1:22118 --log "SetLogLevel(0)[info]" # 设置打印级别 
#device_phy_id=5
#export DUMP_GE_GRAPH=1
#export DEVICE_ID=$device_phy_id
#export deivce_id=$device_phy_id
#export DEVICE_INDEX=$device_phy_i
export SLOG_PRINT_STDOUT=1  # 配置： 打印的级别



#====================启动分布式=========================#
cd dpn # 进入相应的文件夹

for((i=0;i<2;i++)); #每个device_id都要拉起一个训练，使用for循环拉起
do
 export DEVICE_ID=$i # device_id 
 export RANK_ID=$i # device_id对于的rank_id 对应于ranktable
 python3.7 train_dpn_distribute.py & # 注意结束符号有个 & 
done
```

### train训练代码设置


```
# =========== 导入相关包   ======= # 
from npu_bridge.estimator import npu_ops
from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig
from npu_bridge.estimator.npu.npu_optimizer import NPUDistributedOptimizer
# =============================== #

# ====== 获得RANK_SIZE 和 RANK_ID # 
rank_size = int(os.getenv('RANK_SIZE'))
rank_id = int(os.getenv('RANK_ID'))
# ============================== #

# 注意不要导入一下的包获得RANK_SIZE 和 RANK_ID    这些会报错# 
# from hccl.manage.api import get_local_rank_id
# from hccl.manage.api import get_rank_size
# from hccl.manage.api import get_rank_id
# ====================================================== #

# ================== 数据的分布 ======================== #  
images = image_data["images"]
labels = image_data["labels"]
dataset = tf.data.Dataset.from_tensor_slices((images, labels))
#=========== 不同device之间 dataset的共享 =========#
dataset = dataset.shard(rank_size,rank_id)
#=================================================#
dataset.map(_parse_augmentation)
dataset = dataset.shuffle(batch_size * 100)
dataset = dataset.repeat()
dataset = dataset.batch(batch_size, drop_remainder=True)
iterator = dataset.make_one_shot_iterator()
images_batch, labels_batch = iterator.get_next()
# ==================================================== #


# ================ 设置loss ======================= # 
loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=out, labels=inputy)
loss = tf.reduce_mean(loss)
opt = tf.train.AdagradOptimizer(0.01 * rank_size)
opt = NPUDistributedOptimizer(opt) # Add NPU Distributed Optimizer 
train_op = opt.minimize(loss)
# checkpoint_dir = './models' if rank_id == 0 else None
# ================================================ #


# ================= Seesion相关的配置 ============ #
config = tf.ConfigProto()
custom_op= config.graph_options.rewrite_options.custom_optimizers.add()
custom_op.name ="NpuOptimizer"
custom_op.parameter_map["use_off_line"].b = True
config.graph_options.rewrite_options.remapping = RewriterConfig.OFF#关闭remap开关
# =============================================== #



# ================ 执行训练  ==================== #
sess = tf.Session(config=config)

with tf.Session(config=config) as sess:
    x_in,y_in = sess.run([images_batch, labels_batch])
    each_loss,_ = sess.run([loss,train_op],feed_dict={inputx: x_in, inputy: y_in})
    print(each_loss)
sess.close()
# ============================================= # 
```
