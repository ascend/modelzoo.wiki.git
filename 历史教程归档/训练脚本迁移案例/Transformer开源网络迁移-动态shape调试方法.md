 **背景：** 昇腾910 Tensorflow版本暂不支持动态shape输入，因此需要对训练脚本进行修改，采用静态shape方案进行训练

**1. 网络迁移修改说明**

| 修改文件名：                                      | 修改点                                                       | 修改原因                                                  |
| ------------------------------------------------- | :----------------------------------------------------------- | --------------------------------------------------------- |
| parallel_tfrecord_input_pipeline.py               | parallel_tfrecord_input_pipeline.py                          | 数据预处理不支持动态shape，修改为静态shape处理                       |
| vocab_utils.py                                    | if vocab_size % 16 > 0:       vocab_size = (16-vocab_size%16) + vocab_size | 词表大小需要16的倍数,否则精度有问题，版本bug修改          |
| transformer_utils.py:tf.contrib.layers.layer_norm | mean, variance = tf.nn.moments(x, [-1], keep_dims=True)       with tf.name_scope("batch_norm_custom"):       epsilon_tensor = tf.reshape(tf.constant([epsilon]), [1,1])       norm_x = (x - mean) * tf.rsqrt(variance + epsilon_tensor)       y = norm_x * scale + bias         y = tf.cast(y, x_dtype) | tf.contrib.layers.layer_norm调用，前期版本不支持修改      |
| beam_search_decoder.py:dynamic_decode             | static_decode                                                | 推理流程：tf.while_loop不支持（推理流程），版本不支持修改 |
| trainer_lib.py                                    | run_config_cls = NPURunConfig                                | 指导书迁移                                                |
| nmt_estimator.py                                  | class NMTEstimator(NPUEstimator)                             | 指导书迁移                                                |
| optimize_utils.py                                 | NPUDistributedOptimizer       NPULossScaleOptimizer       FixedLossScaleManager       ExponentialUpdateLossScaleManager | 指导书迁移                                                |

**2. 动态shape修改调试**

**原始实现方式：**

根据句子长度，将相似的句子长度组成一个batch，以batch中最长句子的长度为基准，把其他所有句子进行padding补齐，因此不同batch的句子长度会不一致，存在动态输入的情况。

**修改方案：**

由于NPU不支持动态输入，修改方案是把输入句子长度固定为128，具体方法是把短句子拼接成128的长句子，长句子超出128的丢弃。比如，有10个长度为10的短句子，我们把短句子拼接在一起，逗号分隔开，新增segment_id参数来区分不同的句子，然后组成固定size的batch进行训练。这样，网络的输入就固定为128，解决了动态shape暂不支持的问题。

**修改方法：**

训练脚本具体修改点：

**1）新增segment_id参数来区分不同的句子，拼接句子的segment_id为当前句子编号（0-句子个数）** 

数据预处理文件修改点：create_training_data_concat.py

新增句子拼接实现


```
def concat_sents(instances):
  max_skip = 1000
  visited = {}
  max_length = FLAGS.max_seq_length
  ret = []
  for i, instance in enumerate(instances):
    # skip visited instance
    if i in visited:
      continue
    visited[i] = 1

    # find another instance to concat
    skip_count = 0
    for j in range(i+1, len(instances)):
      if j in visited:
        continue
      # update skip_count
      skip_count += 1
      if skip_count > max_skip:
        break
    
      length = [x+y for x,y in zip(instance.length, instances[j].length)]
      if max(length) > max_length:
        continue
      else:
        instance = combine_instance(instance, instances[j])
        visited[j] = 1
    
    # now instance is already a concatenation of one or more instances
    ret.append(instance)

  return ret
```


网络结构修改点：noahnmt/encoders/transformer_encoder.py


```
    if segment_ids is not None:
      # create mask to avoid cross-segment attention
      segment_ids = tf.cast(segment_ids, constant_utils.DT_FLOAT())
    
      self_mask = tf.expand_dims(segment_ids, -1) - tf.expand_dims(segment_ids, 1)
    
      graph_utils.add_dict_to_collection({"self_mask_init": self_mask}, "SAVE_TENSOR")
    
      self_mask = tf.clip_by_value(tf.abs(self_mask), 0, 1)
    
      graph_utils.add_dict_to_collection({"self_mask_clip": self_mask}, "SAVE_TENSOR")
    
      # self_mask = tf.cast(self_mask, constant_utils.DT_FLOAT())
    
      self_mask = 1 - self_mask
    
      graph_utils.add_dict_to_collection({"self_mask_sub": self_mask}, "SAVE_TENSOR")
    
      sequence_mask = sequence_mask * self_mask
    
      graph_utils.add_dict_to_collection({"sequence_mask_new": sequence_mask}, "SAVE_TENSOR")
      
```


**2）修改原有参数position_ids实现，拼接句子的position_id为各个句子从0到seq_length**  

编码修改：noahnmt/encoders/transformer_encoder.py


```
      if position_ids is not None:
        pos_enc = common_utils.gather_npu(pos_enc, position_ids)
      else:
        pos_enc = tf.expand_dims(pos_enc,0)
```


解码修改：noahnmt/decoders/transformer_decoder.py


```
      position_ids = features.get("target_posids", None)
      if position_ids is not None:
        positions_embed = common_layers.gather_npu(positions_embed, position_ids)
        inputs = inputs + positions_embed
      else:
        positions_embed = positions_embed[:input_shape[1]]
        inputs = self._combiner_fn(inputs, tf.expand_dims(positions_embed,0))
```





