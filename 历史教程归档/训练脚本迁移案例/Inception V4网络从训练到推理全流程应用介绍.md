 **背景：** 图像分类是计算机视觉领域的重要领域，它的目标是将图像分类到预定义的标签。许多研究者提出不同种类的神经网络，并且极大提升了分类算法的性能。Inception-v4网络在人脸安防、交通场景识别、图像内容检索等方面有广泛应用，本节简单介绍一下Inception-v4网络如何在昇腾硬件上进行训练和推理应用。

**0. Inception-v4网络训练到推理应用场景介绍**

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/162735_65c6a42a_8267113.png "image-20201104150914839.png")


**1.开源网络迁移**
GPU参考代码：https://github.com/tensorflow/models/tree/master/research/slim

NPU脚本地址：https://gitee.com/ascend/modelzoo/tree/master/built-in/TensorFlow/Official/cv/image_classification/InceptionV4_ID0002_for_TensorFlow

优化点：

| 修改文件名         | 修改点                                                       | 修改原因                                                     |
| ------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ |
| data_loader.py     | 新增代码：<br/>ds = ds.interleave(tf.data.TFRecordDataset, cycle_length=num_readers, block_length=1, num_parallel_calls = tf.data.experimental.AUTOTUNE)<br/>options = tf.data.Options()<br/>options.experimental_threading.max_intra_op_parallelism = 1<br/>ds = ds.with_options(options)<br/>ds = ds.prefetch(buffer_size = batch_size) | 并发读取数据，性能调优                                       |
| inception_v4.py    | dropout算子修改:net = npu_ops.dropout(net, dropout_keep_prob) | 性能调优：dropout算子性能调优                                |
| hyper_param.py     | for i in range(total_steps):<br/>    last_epoch = i // steps_per_epoch<br/>    if i < warmup_steps:<br/>        lr = linear_warmup_lr(i + 1, warmup_steps, base_lr,            warmup_init_lr)<br/>    else:<br/>        lr = eta_min + (base_lr - eta_min) * (1. + math.cos(math.pi*last_epoch / T_max)) / 2<br/>    lr_each_step.append(lr) | 精度调优：学习率更新                                         |
| inception_utils.py | 新增代码：<br/>def inception_arg_scope(<br/>    weight_decay= 0.0004,<br/>    use_batch_norm=True,<br/>    batch_norm_decay=0.9997,<br/>    batch_norm_epsilon=0.001,<br/>    activation_fn=tf.nn.relu,<br/>    batch_norm_updates_collections=tf.compat.v1.GraphKeys.UPDATE_OPS,<br/>    batch_norm_scale=False): | 精度调优                                                     |
| model.py           | 新增代码： <br/>with  slim.arg_scope(inception_v4.inception_v4_arg_scope(weight_decay=self.args.weight_decay)):<br/><br/>total_loss = tf.losses.get_total_loss(add_regularization_losses = True)<br/><br/>from npu_bridge.estimator.npu.npu_loss_scale_optimizer import NPULossScaleOptimizer<br/>from npu_bridge.estimator.npu.npu_loss_scale_manager import FixedLossScaleManager | NPU的loss scale和原生的存在一些差异，对global step等使用差异 |

**2.模型固化**

**固化思路**：用户已经定义推理模型结构，加载网络模型结构保存成unfrozen pb，然后调用Tensorflow freeze_graph接口把unfrozen pb和ckpt中的权重固化。

**前提条件**：在使用freeze_graph()固化模型时，最重要的是确定“输出节点名称”，因为网络其实比较复杂，定义了输出节点的名字，那么freeze的时候就只把该结点所需要的子图固化下来，其他的都删除。

参考代码如下： 脚本见附件ckpt2pb_freeze.py

```
import tensorflow as tf
from tensorflow.python.tools import freeze_graph
# 导入网络模型文件
from inception import inception_v4
from tensorflow.contrib import slim as slim

# 指导ckpt路径
ckpt_path = "ckpt_tf/model.ckpt-250200"

def main(): 
    tf.reset_default_graph()
    # 定义输入节点
    inputs = tf.placeholder(tf.float32, shape=[None, 299, 299, 3], name="input")
    # 调用网络模型生成推理图
    with slim.arg_scope(inception_v4.inception_v4_arg_scope()):
        top_layer, end_points = inception_v4.inception_v4(inputs=inputs, num_classes=1000, dropout_keep_prob=1.0, is_training = False)
    # 定义输出节点
    #predict_class = tf.argmax(logits, axis=1, output_type=tf.int32, name="output")
    with tf.Session() as sess:
        tf.train.write_graph(sess.graph_def, './pb_model_tf', 'model.pb')    # 默认，不需要修改
        freeze_graph.freeze_graph(
              input_graph='./pb_model_tf/model.pb',   # 默认，不需要修改
              input_saver='',
              input_binary=False, 
              input_checkpoint=ckpt_path, 
              output_node_names='InceptionV4/Logits/Logits/BiasAdd',  # 与上面定义的输出节点一致
              restore_op_name='save/restore_all',
              filename_tensor_name='save/Const:0',
              output_graph='./pb_model_tf/inception_v4_tf.pb',   # 改为对应网络的名称
              clear_devices=False,
              initializer_nodes='')
    print("done")

if __name__ == '__main__': 
    main()
```

**3.在线推理**

在不知道原始训练模型train源码，我们也可以从pb中恢复网络结构，并NPU/GPU进行预测，从读取的序列化数据中导入网络结构即可。

**1）从pb文件加载graph**

```
with tf.gfile.GFile(model_file, "rb") as gf:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(gf.read())

with tf.Graph().as_default() as graph:
    tf.import_graph_def(graph_def, name="")
```

**2）读取数据**

```
image= tf.gfile.FastGFile(image_file, 'rb').read()
```

**3）预处理**

```
img = tf.image.decode_jpeg(image)
if img.dtype != tf.float32:
    img = tf.image.convert_image_dtype(img, dtype=tf.float32)   #
img = tf.image.central_crop(img, central_fraction=0.875)
img = tf.expand_dims(img, 0)
img = tf.image.resize_bilinear(img, [299, 299], align_corners=False)
img = tf.squeeze(img, [0])
img = tf.subtract(img, 0.5)
img = tf.multiply(img, 2.0)
```

**4）执行训练**

```
out = self.sess.run(self.output_tensor, feed_dict={self.input_tensor: data})
```

**5）后处理**

```
acc, acc_op = tf.metrics.accuracy(labels=[labels], predictions=predicted_classes)
top5acc = tf.cast(tf.nn.in_top_k(logits, [labels], 5), tf.float32)
```

**4.离线推理**

**1）pb转om**

**设置环境变量：**

```
export install_path=/usr/local/Ascend
export PATH=/usr/local/python3.7.5/bin:${install_path}/atc/ccec_compiler/bin:${install_path}/atc/bin:$PATH
export PYTHONPATH=$ PYTHONPATH:${install_path}/atc/python/site-packages:${install_path}/atc/python/site-packages/auto_tune.egg/auto_tune:${install_path}/atc/python/site-packages/schedule_search.egg:$PYTHONPATH
export LD_LIBRARY_PATH=${install_path}/atc/lib64:$LD_LIBRARY_PATH
export ASCEND_OPP_PATH=${install_path}/opp
```

**ATC命令行：**

**A：不带AIPP转模型**

```
atc --model=inception_v4_tf.pb --framework=3 --output=inception_v4_tf --output_type=FP32 --soc_version=Ascend310 --input_shape="input:1,299,299,3" --log=info
```

**B：带AIPP转模型**

```
atc --model=inception_v4_tf.pb --framework=3 --output=inception_v4_tf --output_type=FP32 --soc_version=Ascend310 --input_shape="input:1,299,299,3" --log=info -- insert_op_conf=inception_v4_aipp.cfg
```

 **2）开发推理Demo**

由于DVPP硬件预处理 有一些对齐约束，所以从开源Tensorflow/openCV映射到昇腾DVPP+AIPP预处理时，需要考虑这些对齐补边操作，对中间crop/resize过程进行矫正，保证两种方式预处理输出图片接近，示例如下

Tensorflow原始预处理实现逻辑：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/162934_4f499e39_8267113.png "截图.PNG")

DVPP+AIPP预处理逻辑：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/162946_b85fba5a_8267113.png "截图2.PNG")

**5.模型小型化**

对原始Tensorflow框架的网络模型进行量化，量化是指对模型参数和数据进行低比特处理，让最终生成的网络模型更加轻量化，从而达到节省网络模型空间、降低传输时延、提高计算效率、达到性能提升与优化的目标。

1）从测试集中选择32张图片进行量化

2）使用amct接口进行模型量化

```
amct.create_quant_config(config_file=config_path,
                         graph=graph,
                         skip_layers=[],
                         batch_num=1)
record_path = os.path.join(PATH, 'record.txt')
amct.quantize_model(graph=graph,
                    config_file=config_path,
                    record_file=record_path)
with tf.compat.v1.Session() as sess:
    sess.run(tf.compat.v1.global_variables_initializer())
    sess.run(output_tensor_qu, feed_dict={input_tensor: batch})
amct.save_model(pb_model=model_path,
                outputs=['InceptionV4/Logits/Logits/BiasAdd'],
                record_file=record_path,
                save_path=os.path.join(PATH, RESULT_DIR, 'InceptionV4'))
```

3）模型量化pb

量化后模型文件大小是量化前文件的1/4左右（fp32->int8）

下图inception_v4_tf.pb是量化前文件，InceptionV4_quantized.pb是量化后文件大小

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/163020_907da6b1_8267113.png "截图3.PNG")

