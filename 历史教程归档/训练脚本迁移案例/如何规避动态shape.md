 **1、什么是动态shape** 

动态shape相对于静态shape而言，顾名思义就是计算图中算子的shape信息是动态不确定的，只有在运行时才会知道确定的shape信息。判断网络是否存在动态shape最直观的方式就是tensorboard的shape信息通常为？，或者pbtxt中shape信息为-1.
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/154547_634d3159_7971653.png "屏幕截图.png")

 **2、为什么会存在动态shape** 

网络是由算子组成的，算子是由shape、dtype等关键信息组成，而shape信息又与数据排布方式密切相关，如常见的NHWC或者NCHW（格式排布参考一文读懂数据格式、内存排布方式、转换算子），以CV举例， C表示通过数，通常不会改变，N即BatchSize，训练过程中可变，图片尺度 H和W，多个迭代间也有可能改变。

动态shape存在与实际业务相关，比如CV类（目标检测）的不同图片大小、NLP中不同句子长度、语音语义中一句话的长度，不同step/epoch间BatchSize/Datasize以达到提升网络的鲁棒性，从而提升训练精度的目的。

目前NPU对动态shape支持度不友好，在网络迁移的时候会报错（如下），有些场景是可以规避的。

```
  File "/root/myvenv/orgenv/lib/python3.7/site-packages/te/platform/fusion_manager.py", line 706, in call_op_func
    return opfunc(*inputs, *outputs, *attrs)
  File "/usr/local/Ascend/ascend-toolkit/latest/arm64-linux/opp/op_impl/built-in/ai_core/tbe/impl/conv2d.py", line 149, in op_select_format
    param_list = _select_format(params)
  File "/usr/local/Ascend/ascend-toolkit/latest/arm64-linux/opp/op_impl/built-in/ai_core/tbe/impl/conv2d.py", line 113, in _select_format
    unknownshape_format="NC1HWC0")
TypeError: gen_param() got an unexpected keyword argument 'unknownshape_format'
```


 **3、动态batchsize引入** 
   
   设置dataset = dataset.batch(batch_size,drop_remainder=True)，或者batchsize=1规避

 **4、动态HW引入** 
   
   一般在数据预处理的时候对数据进行随机的crop，会让数据尺寸不断变化，可以注释这块的随机操作
   也可以对输出的img_crop和label_crop设置固定shape img_crop.set_shape((h, w, 3))

```
def random_crop_and_pad_image_and_labels(image, label, crop_h, crop_w, ignore_label=255):
    label = tf.cast(label, dtype=tf.float32)
    label = label - ignore_label # Needs to be subtracted and later added due to 0 padding.
    combined = tf.concat(axis=2, values=[image, label])
    image_shape = tf.shape(image)
    combined_pad = tf.image.pad_to_bounding_box(combined, 0, 0, tf.maximum(crop_h, image_shape[0]), tf.maximum(crop_w, image_shape[1]))

    last_image_dim = tf.shape(image)[-1]
    last_label_dim = tf.shape(label)[-1]
    combined_crop = tf.random_crop(combined_pad, [crop_h,crop_w,4])
    img_crop = combined_crop[:, :, :last_image_dim]
    label_crop = combined_crop[:, :, last_image_dim:]
    label_crop = label_crop + ignore_label
    label_crop = tf.cast(label_crop, dtype=tf.uint8)
    return img_crop, label_crop
```
再或者如下输入格式，把shape固定，-1修改成固定值

```
def cnn_model_fn(features, labels, mode):
    """Model function for CNN."""
    # Input Layer
    # Reshape X to 4-D tensor: [batch_size, width, height, channels]
    # MNIST images are 28x28 pixels, and have one color channel
    input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])
```

 **5、网络结构引入** 
     
      在网络结构中存在一些算子本身的输出就是动态的，例如tf.unique、tf.where、tf.dynamic_partition、argmax、reshape等。
      
       例如tf.reshape的修改 

```
       labels = tf.squeeze(labels, axis=3) 
       #tf.print(labels,[labels.shape]) 或者print（lables）打印出lables的shape信息，取代下面的-1值
       #logits_by_num_classes = tf.reshape(logits, [-1, params['num_classes']])  #修改之前
       logits_by_num_classes = tf.reshape(logits, [263169, params['num_classes']]) #修改之后
```
       实在不行export EXPERIMENTAL_DYNAMIC_PARTITION=1

   
https://support.huaweicloud.com/mprtg-A800_9000_9010/atlasprtg_13_0033.html
