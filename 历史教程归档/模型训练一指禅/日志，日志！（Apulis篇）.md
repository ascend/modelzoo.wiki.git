正所谓天有不测风云，人有旦夕祸福。哪有人跑训练脚本一次成功，不用调试的？所以，老谭准知道，你一定会来看我这一篇Wiki的:)

总的来讲，训练日志一共有三种，分别称为TensorFlow（或者叫前台）日志，Host日志，Device日志，他们之间的关系大概是这样的：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/145726_6ee996d9_5403319.png "屏幕截图.png")

### TensorFlow（前台）日志

我们启动训练之后，在日志打印窗口能直接看到的日志输出，就是前台日志，一般长这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/145910_5e65b41b_5403319.png "屏幕截图.png")
通常以“TensorFlow”开头，或者长这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/145952_e5753849_5403319.png "屏幕截图.png")
以tf_adapter开头，这种日志都是很容易获取到的，基本上在启动训练之后，都能直接看到，比如在训练启动后的控制台：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/141832_f9d5a9df_5403319.png "屏幕截图.png")
这部分日志能够说明一些问题，但信息量不是非常大，有些问题可以从这部分日志中看出端倪。

### Host日志

这部分日志在定位问题角度，作用是比较大的，如果你在提交了ISSUE之后，有人回复你让你取“Host日志”，那么取的就是这一部分日志。
由于一点技术原因，现在在Apulis平台上是无法在后台文件中打印Host日志的。开发人员可能让你去
`/var/log/npu/slog/host-0`
这个目录去获取Host日志，但很遗憾，在这个平台，这里并不能打印有效日志。

这里老谭给你提供一种办法，让你能够在屏幕上看到Host日志，并能够保存在自己定义的文件中。
1. 在你的训练脚本开头加上一行：
os.environ['SLOG_PRINT_TO_STDOUT'] = "1"
![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/152155_60d03afe_5403319.png "屏幕截图.png")
这样，Host日志就会混在前台日志中一起打印，就像这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/142620_baea9f06_5403319.png "屏幕截图.png")

2. 在训练开始之前，在控制台输入一条命令：
`script -f 你的文件名.log`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/142858_64e57315_5403319.png "屏幕截图.png")
然后正常启动训练，此时控制台，也就是屏幕上所有滚动的内容都会被记录到刚才那个文件中。
训练结束之后，按
`Ctrl+D`
快捷键，停止记录，保存文件：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/143017_ef0fb825_5403319.png "屏幕截图.png")
在我的场景下，这个train_host.log里边就保存了TF日志+Host日志，可以用来分析了。

### Device日志

这部分日志大家可以理解为具体执行任务的硬件侧打印的，某些时候这部分日志包含的信息很关键。
在讲解去哪找这部分日志之前，要简单交代一下Apulis平台对日志目录的处理。
通常情况下，昇腾软件栈的日志都打在
`/var/log/npu`
这个目录下。
但是出于Apulis平台的一些设置上的问题，裸机上的这个目录，被映射到了你的开发环境的
`/var/log/npulog`
这个目录下。
所以，在开发环境上，Device日志应该在：
`/var/log/npulog/slog`
目录下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/143719_0d90ce55_5403319.png "屏幕截图.png")
但是这个目录明显有多个Device子目录，在当前环境下取哪个呢？
在控制台执行命令：
`env | grep DEVICE_ID`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/143843_b7cec793_5403319.png "屏幕截图.png")
在我的场景下，DEVICE_ID=4，那我就去device-4/这个目录去找Device日志。
日志的获取方式就讲到这里。


喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！





`需要任何修改请联系老谭进行评估。`