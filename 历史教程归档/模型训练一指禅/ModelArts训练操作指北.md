如果你还没迁移你的训练脚本到NPU版本，或者根本不知道我这句话是在说什么，[ _请走这个传送门_ ](https://gitee.com/ascend/modelzoo/wikis/%E6%A8%A1%E5%9E%8B%E4%BC%97%E7%AD%B9%E8%AE%AD%E7%BB%83%E4%B8%80%E6%8C%87%E7%A6%85?sort_id=3147602)


ModelArts是华为云的众多云服务之一。之所以我们要用ModelArts平台来做训练，是因为当前只有这个平台后台集成了Ascend910处理器，能给我们做NPU训练使用。
### 开始之前
再提醒大家一下，要准备以下几个条件或材料：
1.迁移后的训练脚本
2.数据集
3.注册好的华为云账号，并且里边有充足的余额

下面我们开始。

总的来讲，在ModelArts上执行训练任务，有三种操作方式，我给它们分别称之为：
- 页面点点点方式（纯网页操作）
- Jupiter Notebook方式（网页上集成了控制台操作）
- Pycharm+插件方式（本地IDE操作）
你可以结合你的喜好来选择一种，或者三种都尝试一下，看哪种最喜欢:)
下面我来一一进行介绍。

### 页面点点点方式
[ _点击这里进入ModelArts主界面_ ](https://console.huaweicloud.com/modelarts/)（记得右键，在新标签页打开链接）

![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/152518_a375a92a_5403319.png "屏幕截图.png")
在正式开始介绍如何操作之前，要跟大家解释一下在ModelArts上训练该如何存放数据。ModelArts是个执行训练任务的云服务平台，那想要执行训练，肯定要先把数据集和训练脚本都传到云上。但是ModelArts本身是不带存储功能的，这就引出了在华为云上的一个存储服务——OBS。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/153120_5bc24567_5403319.png "屏幕截图.png")
（图画得丑，大家见谅）
OBS全称是 **OB**ject  **S**torage Service，对象存储服务，我们姑且把它理解为云上的一块大硬盘就是了，ModelArts作为训练平台，取训练脚本和数据集都要从这里取。结合上边的图，操作ModelArts创建训练任务的时候，跟OBS有一定关联的有这三个步骤：
1. 在页面上创建训练任务的时候，要选择训练脚本以及数据集在OBS上存储的位置
2. ModelArts下发训练任务的时候，是在后台创建了一个虚拟机来执行这个任务
3. 创建的执行任务用的虚拟机会从OBS的对应位置上下载训练脚本和数据集下来进行训练

至于如何把数据集和训练脚本传到OBS上，看这里：
https://support.huaweicloud.com/qs-obs/obs_qs_1000.html
注意，这个链接里边说的 **AK/SK** 很重要，一会儿要考。


如果你看到这里，我就默认你已经创建好了OBS桶，并把你的数据集和训练脚本分别放到了这个桶内不同的目录中。
这里强烈建议你在桶里再创建两个空目录，分别用于保存训练结果和训练产生的控制台日志。

要想让ModelArts认识你自己的OBS，光靠它们存在于同一个用户名下是不行的，ModelArts只认识AK/SK，所以这里我们要在ModelArts上配置一下OBS授权。（不记得什么是AK/SK的同学 去复习前边的内容并在墙角罚站十秒钟谢谢）
ModelArts的主界面，左侧菜单最下边有个“全局配置”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/154622_5ed98748_5403319.png "屏幕截图.png")
点开之后选择“访问授权”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/154705_d4657254_5403319.png "屏幕截图.png")
这里建议使用“使用访问秘钥”方式：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/154854_f91eabec_5403319.png "屏幕截图.png")
输入你的AK/SK，同意授权。
这样，ModelArts就认识你的OBS啦，下面我们正式开始配置训练任务。

同样在ModelArts主菜单，选择“算法管理”，之后选择右侧的“创建”按钮：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/155024_7e01bad4_5403319.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/155255_32c694dc_5403319.png "屏幕截图.png")
- 算法名称自己随便起一个，用默认的也行
- AI引擎用默认的“Ascen-Powered-Engine”（如果选不到这个，就在左上角“控制台”旁边的区域下拉菜单选择“北京四”）
- 后边的下拉菜单选择“TF-1.15-python3.7-aarch64”
- 代码目录选择你OBS上的训练脚本目录
- 启动文件选择你整体训练的入口脚本，即执行脚本
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/155809_190d8640_5403319.png "屏幕截图.png")
- 在①处填一个你喜欢的目录名
- 在②处也填一个
你会看到，底下的“启动命令”那一栏生成了一串启动命令，这个命令是将来在执行虚拟机上会运行的启动你的训练脚本的命令。也正是由于这个原因，在代码迁移的时候，我建议你通过入参方式来确定数据集和模型输出的路径，而非硬编码。
这里的原理是这样的，就以上边这个截图为例，当训练任务启动的时候，ModelArts会自动从OBS上数据集的位置将数据集拷贝到“/home/work/modelarts/inputs/logistic”这个目录，而传给你的训练脚本的就是这个目录，这样训练脚本就能够拿到数据集了。
那有的同学就问了，我这还没选数据集在哪呢，啥时候选数据集？
不要急，我们现在是在创建算法阶段，一会儿才会用这个算法来创建一个具体的训练任务，那个时候会选择数据集的。
同理，训练之执行完成之后，ModelArts会自动帮你把“/home/work/modelarts/outputs/output”这个路径下的东西拷贝到OBS上你指定的模型输出位置，这样就能够拿到输出了。
总之，通过页面操作，没法直接接触到执行虚拟机的控制台，也就只能靠这种方式完成文件的传递了。

ok，这一步做完之后，点右下角的“下一步”。
在这里，你的训练脚本需要什么超参，可以自己定义（记得定义每一个超参之后，后边的对号要点一下）：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/162445_400e64fb_5403319.png "屏幕截图.png")
当然，这里定义的只是有哪些参数，至于启动训练时具体的赋值，在创建训练任务的时候可以修改的，记住，我们现在在创建算法，而非训练任务。
超参创建好之后，点击“下一步”。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/160734_f9331359_5403319.png "屏幕截图.png")
不太建议大家设置任何约束，直接点“提交”。
任务提交成功之后，选择“返回算法管理”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/160846_7d9ddf6f_5403319.png "屏幕截图.png")
点击“创建训练作业”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/160912_219c64e5_5403319.png "屏幕截图.png")
这里，我们要用刚才创建好的算法来生成一个具体的训练作业了。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/161108_9e28c750_5403319.png "屏幕截图.png")
- 数据来源->数据存储位置，选择你的数据集在OBS上存储的位置
- 训练输出，选择想让模型输出保存在OBS的哪个路径下
- 作业日志路径虽然非必选，但我还是建议你选择一个OBS目录，这样才能把训练时的控制台日志保存下来，否则训练结束日志就丢掉了

配置完成后，直接选择下一步，提交。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/161345_dab5af08_5403319.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/161538_62a4b919_5403319.png "屏幕截图.png")
之后的工作，就是等待训练结束或训练报错了。祝你好运:)


### Jupiter Notebook方式
研究中………………………………………………

### Pycharm+插件方式
用这种方式就可以在本地开发代码后，直接操作Pycharm进行任务提交，省去了往OBS上上传训练代码，以及在页面上创建算法/训练任务这两个步骤。不过数据集还是要传到OBS上的。

首先，在ModelArts首页往下翻，会看到一个“Pycharm Toolkit”，下载一下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/103910_0acb6cdb_5403319.png "屏幕截图.png")
下载完成后，做如下几步操作：
1. 打开本地Pycharm工具
2. 选择左上角菜单栏的 **File->Settings** 
3. 在“Settings”对话框中，首先单击左侧导航栏中的“Plugins”，然后单击右侧的设置图标，选择“Install Plugin from Disk”，弹出文件选择对话框:
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/104349_9f1913a5_5403319.png "屏幕截图.png")
4. 选择刚才刚下载好的zip包，点击OK
5. 点击Restart IDE，重启一下Pycharm：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/104520_8ecf3444_5403319.png "屏幕截图.png")
菜单栏现在多了个“ModelArts”，说明安装成功了：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/104608_d92c2327_5403319.png "屏幕截图.png")
现在，单击这个ModelArts按钮，选择“Edit Credentials”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/104757_5374b8df_5403319.png "屏幕截图.png")
在弹出的对话框中选择你的ModelArts所在区域，并在下边填好AK/SK（忘记什么是AK/SK的，回前边复习一下谢谢）
点击OK之后，如果右下角出现这个提示，说明密钥是有效的：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/105420_034b93ec_5403319.png "屏幕截图.png")
接下来就是在Pycharm上创建训练任务了，同样点击菜单栏的ModelArts->Edit Training Job Configuration：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/144058_3cf30f62_5403319.png "屏幕截图.png")
     1. AI Engine的选择和在网页上选择一样，选“Ascend-Powered-Engine”
     2. 训练框架选择TF-1.15-python3.7-aarch64
     3. Boot File Path选择你本地的训练入口脚本
     4. Code Directory这里，在选完脚本之后，这个工具会自动帮你选择入口脚本所在目录，将来启动训练的时候，这个目录下的所有文件都会被拷贝到训练虚拟机上；如果这个目录获取的不对，就自己手动选择一下
     5. OBS Path：这里输入你想让训练结果和日志保存到OBS哪里，路径的获取方法：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/144526_b10c2eb0_5403319.png "屏幕截图.png")在OBS Browser+工具内进入你想保存日志的目录，单击一下地址栏空白处，这个地址栏会变成可编辑状态，复制这个路径，但注意不要复制最前边的“obs:/”:![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/144626_96c3b71a_5403319.png "屏幕截图.png")
     6. Data Path in OBS:你的数据集在OBS上存放的地方。路径获取方式同上。
配置完之后，点击右下角的Apply

实际执行的时候，ModelArts会自动帮你把数据集路径和训练结果输出路径以命令行参数的方式传递进训练脚本，在脚本中，这两个参数的获取方式参考如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/144913_d3ed2635_5403319.png "屏幕截图.png")
做完这一切之后，喝口水，深呼吸，点击菜单栏的ModelArts->Run Training Job，然后安静的等待训练结束。
当你看到日志打印：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/145838_342ebc6e_5403319.png "屏幕截图.png")
并且OBS上有了你想要的训练结果：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/145922_52a09248_5403319.png "屏幕截图.png")
证明训练顺利结束啦！

 _[训练报错了，想看看日志？](https://gitee.com/ascend/modelzoo/wikis/%E6%97%A5%E5%BF%97%EF%BC%8C%E6%97%A5%E5%BF%97%EF%BC%81?sort_id=3157963)_ 
[ _Loss是收敛了，精度不够怎么办？_ ](https://gitee.com/ascend/modelzoo/wikis/Loss%E6%98%AF%E6%94%B6%E6%95%9B%E4%BA%86%EF%BC%8C%E7%B2%BE%E5%BA%A6%E4%B8%8D%E5%A4%9F%E6%80%8E%E4%B9%88%E5%8A%9E%EF%BC%9F?sort_id=3148793)


喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！





`需要任何修改请联系老谭进行评估。`