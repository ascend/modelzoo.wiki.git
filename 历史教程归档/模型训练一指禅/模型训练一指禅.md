 （作者注：本文中所有的操作和资料基本都能在其他某文档中找到，只是比较零散，不成体系。我在此将其整理成能从头做下去的一套动作，仅供大家参考。如果存在任何的问题或诉求，可以给我这篇Wiki提ISSUE。）
### 准备工作
脚本方面：在CPU或GPU上调通的，已经能跑出结果的、确保loss能收敛到可接受水平的TensorFlow训练脚本
数据方面：整理好训练TF脚本用的全量数据集
环境方面：
- 用ModelArts的同学：准备好华为云账号，充值（用代金券或直接充值），没有华为云账号的，[ _点击这里注册一个_ ](https://reg.huaweicloud.com/registerui/register.html#/register)
- 用Apulis平台的同学（不知道什么是Apulis的请忽略）：准备好平台的账号密码，确定访问IP地址

### 万里长征第一步
将TensorFlow脚本做亿点点（大雾）修改，使其能够在昇腾的训练平台上进行训练（即训练脚本迁移）。
-  **如果你用的是Session.run执行训练：** 
这里有一个原始代码样例：
https://gitee.com/Riddle_Tan/modelzoo/blob/master/CodeSample/train_gpu_sessrun/train.py
网络结构很简单，选用的是MNIST数据集，注意我是通过命令行参数的方式（--data_url=xxxxxxx）来获取数据集路径的，强烈建议你在你的脚本中也这样获取数据，不要像自己在笔记本上训练一样，把数据集路径硬编码到代码中。
这里再给出一个迁移后的代码样例：
https://gitee.com/Riddle_Tan/modelzoo/blob/master/CodeSample/train_npu_sessrun/train.py
你可以用诸如BeyondCompare等工具来比较一下这两个代码的区别，你会发现这两个代码的差异很小，主要有以下几个：
 **1.--------------->** ![GPU与NPU代码差异1](https://images.gitee.com/uploads/images/2020/1126/194049_c3d3afeb_5403319.png "屏幕截图.png")第一个差异是NPU训练代码多了两行：
     **import npu_bridge
    from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig** 
    在你调用Session.run的脚本顶部也复制这两行进去，动作要快，姿势要帅！
 **2.--------------->** ![GPU与NPU代码差异2](https://images.gitee.com/uploads/images/2020/1126/194256_94a8ce19_5403319.png "屏幕截图.png")第二个差异是NPU训练代码多了这5行：
 **config = tf.ConfigProto()
    custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
    custom_op.name = "NpuOptimizer"
    custom_op.parameter_map["use_off_line"].b = True
    config.graph_options.rewrite_options.remapping = RewriterConfig.OFF** 
，创建了一个新的Session Config对象并赋了一些值。当然，如果你训练时已经用到了config对象，就基于这几行来扩展就可以。
 **3.--------------->** ![GPU与NPU代码差异2](https://images.gitee.com/uploads/images/2020/1126/194433_44054ab5_5403319.png "屏幕截图.png")创建session对象的时候，记得把刚才创建好的新的config对象当做参数传进去。
    齐活！

-  **如果你用的是Estimator执行训练：** 
这里有一个原始代码样例：
https://gitee.com/Riddle_Tan/modelzoo/blob/master/CodeSample/train_gpu_estimator/train.py
网络结构也很简单，选用的还是MNIST数据集，获取数据集路径的方式还是依靠命令行参数。
这里再给出一个迁移后的代码：
https://gitee.com/Riddle_Tan/modelzoo/blob/master/CodeSample/train_npu_estimator/train_npu.py
同样，我们来比较一下两者的差异：
1.![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/195528_b0e6bb07_5403319.png "屏幕截图.png")首先，NPU代码在引用包的时候多了如下两行：
 **from npu_bridge.estimator.npu.npu_config import NPURunConfig
from npu_bridge.estimator.npu.npu_estimator import NPUEstimator** 
2.![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/195627_96ebcb8a_5403319.png "屏幕截图.png")其次，有如下两个直接替换的改动：
 **tf.estimator.RunConfig----->NPURunConfig
tf.estimator.Estimator----->NPUEstimator** 
搞定！
但是有几个老谭以前在Estimator迁移中踩过的坑，这里想跟大家分享一下：
1.创建Estimator对象的时候，一定要用Estimator类，不要尝试用它的任何子类，诸如DNNClassifier等，这些子类是无法迁移至NPU进行训练的
2.创建数据集的时候，尽量用TensorFlow原生的tf.data.Dataset类，因为这个类在创建batch的时候，可以带drop_remainder参数，防止数据集产生动态shape，我们当前的软件栈对动态shape支持的还不太完善，如果用其他的封装好的数据集类创建数据集，可能无法带入drop_remainder参数，导致训练失败，切记切记。

到此，代码迁移工作就结束了，那怎样让你的训练代码在NPU上跑起来呢？
[ _如果你用的是ModelArts环境，点我传送_ ](https://gitee.com/ascend/modelzoo/wikis/ModelArts%E8%AE%AD%E7%BB%83%E6%93%8D%E4%BD%9C%E6%8C%87%E5%8C%97?sort_id=3148776)

[ _如果你用的是Apulis平台，点我传送_ ](https://gitee.com/ascend/modelzoo/wikis/Apulis%E5%B9%B3%E5%8F%B0%E8%AE%AD%E7%BB%83%E6%93%8D%E4%BD%9C%E6%8C%87%E5%8C%97?sort_id=3148777)


喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！

`需要任何修改请联系老谭进行评估。`