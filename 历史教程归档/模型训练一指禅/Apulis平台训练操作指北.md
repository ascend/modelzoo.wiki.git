Apulis是依瞳科技开发的一个人工智能开放平台，其涵盖的功能有很多，但我们如果只是想要在这个平台上进行基本的模型训练，那么除了创建开发环境的动作以外，其他的操作和我们平时用终端工具连接服务器进行训练差不太多。

总得来说，在Apulis平台做训练，分三个大步骤：
- 创建开发环境
- 往开发环境上传训练脚本代码、数据集
- 启动训练

### 创建开发环境

如果你会看我这篇Wiki，你手上肯定有某处、某活动发放给你的Apulis平台地址，用户名，密码，所以，浏览器登录这种操作就不在这里展示了。在首次登录进平台之后，页面应该是这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104205_b03976bc_5403319.png "屏幕截图.png")
我们点击左侧的“代码开发”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104240_149b9d8d_5403319.png "屏幕截图.png")
点击“创建开发环境”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104718_aee9363a_5403319.png "屏幕截图.png")
- “开发环境名称”自己随便起一个
- 代码存储路径这写一个你喜欢的文件夹名称
- 引擎类型记得选“TensorFlow”
- 设备数量选“1”
 **这里要注意，设备数量是有限的，当且仅当你要进行训练的时候再来创建开发环境，训练完保存好结果后赶紧删除开发环境。如果开发环境长时间占用且空闲，会被管理员销毁，切记。** 
填好上边的表单之后，点击右下角的“立即创建”按钮，然后等一会儿，可能会排队。

当你的开发环境状态显示为“运行中”，证明开发环境创建完毕了：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104839_e7f05749_5403319.png "屏幕截图.png")
这个时候点击右边的“Jupyter”：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/104912_f0d7e775_5403319.png "屏幕截图.png")
单击箭头所指的“Terminal”：
这里就是你的开发环境了。如果你不会Ubuntu基本操作的话，老谭是真教不了……

### 往开发环境上传训练脚本代码、数据集
这里建议你 _[下载一个Winscp工具](https://winscp.net/eng/download.php)_ ，下载安装很简单，就不截图了。安装好之后，打开工具：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110125_d85d3c54_5403319.png "屏幕截图.png")
这里跟我们要IP，端口，用户名，这些东西我们回到Apulis平台的开发环境管理页面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110437_5f6bb3b7_5403319.png "屏幕截图.png")
点击你的开发环境那一行，右边的“SSH”，弹出的“使用SSH连接”小气泡里边有IP，端口，用户名，密码信息，把这些信息填写到Winscp的对话框中：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110629_6c678bb5_5403319.png "屏幕截图.png")
点击下方的登录，就能进入到你的开发环境的文件管理界面了：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/110856_00975b72_5403319.png "屏幕截图.png")
其中左侧是你的主机，右侧是Apulis开发环境，想往开发环境上传东西，从左侧拖到右侧就好了。
在我的场景下，我把代码传到了“code”目录，把数据集传到了“dataset”目录，然后我创建了一个“output”目录来保存训练结果。你也可以这么做。

### 启动训练
数据上传完毕之后，回到开发环境那个Jupyter，在我的场景下，直接用python3.7后边接训练脚本和参数就能启动训练了：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/111254_12f6ccbc_5403319.png "屏幕截图.png")
跟你在一台普通的Ubuntu机器上做训练几乎没有任何区别。

Apulis平台上的训练操作就这么简单，创建环境，传数据，启动训练。
但其实平台的操作方式，以及连接开发环境的方式有很多，如果大家给老谭的点赞够多，咱就加更~

 _[训练报错了，想看看日志？](https://gitee.com/ascend/modelzoo/wikis/%E6%97%A5%E5%BF%97%EF%BC%8C%E6%97%A5%E5%BF%97%EF%BC%81%EF%BC%88Apulis%E7%AF%87%EF%BC%89?sort_id=3221603)_ 
[ _Loss是收敛了，精度不够怎么办？_ ](https://gitee.com/ascend/modelzoo/wikis/Loss%E6%98%AF%E6%94%B6%E6%95%9B%E4%BA%86%EF%BC%8C%E7%B2%BE%E5%BA%A6%E4%B8%8D%E5%A4%9F%E6%80%8E%E4%B9%88%E5%8A%9E%EF%BC%9F?sort_id=3148793)
 [_训练性能太差了，有什么好办法吗？_](https://gitee.com/ascend/modelzoo/wikis/%E8%AE%AD%E7%BB%83%E6%80%A7%E8%83%BD%E5%A4%AA%E5%B7%AE%E4%BA%86%EF%BC%8C%E6%9C%89%E4%BB%80%E4%B9%88%E5%A5%BD%E5%8A%9E%E6%B3%95%E5%90%97%EF%BC%9F%EF%BC%88Apulis%E7%89%88%EF%BC%89?sort_id=3148795)

喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！

`需要任何修改请联系老谭进行评估。`