（既然点开这个链接了，说明你的训练脚本在NPU上已经跑通了，咱就不在这介绍训练的基操了）
俗话说得好：
> 不带前提就聊精度的，都是耍流氓。       ————反正不是鲁迅说的

这里我们先给精度不够的问题分两个大类：
- 没有比较尺度，就是“绝对”的低
- 同样的脚本，在NPU上训练出来的精度比在GPU上低，也就是“相对”的低

### 如果你现在遇到的问题，是精度“绝对”的低
那可以先看看这两篇民间大神出品的Wiki：
 _[InceptionV4训练精度调优方法](https://gitee.com/ascend/modelzoo/wikis/InceptionV4%E8%AE%AD%E7%BB%83%E7%B2%BE%E5%BA%A6%E8%B0%83%E4%BC%98%E6%96%B9%E6%B3%95?sort_id=3154910)_ 
 _[YoloV4网络训练精度调优（Mindspore）](https://gitee.com/ascend/modelzoo/wikis/YoloV4%E7%BD%91%E7%BB%9C%E8%AE%AD%E7%BB%83%E7%B2%BE%E5%BA%A6%E8%B0%83%E4%BC%98%EF%BC%88Mindspore%EF%BC%89?sort_id=3155021)_  （这里提了一些跟训练框架无关的精度优化思路）

### 如果你现在遇到的问题，是精度“相对”的低
那咱可就有话题可聊了。

这个“相对”，指的是在NPU上训练出来的模型精度，跟同样的脚本在CPU/GPU上训练出来的精度相比，较低一些。所以，这里我们要使用一个“精度比对工具”，来比较在NPU上训练出来的参数，和在GPU上训练出来的参数的精度差异。

“精度比对工具”的图形化版本，是集成在MindStudio这个工具中的，所以需要你先安装一个 _**[ubuntu-18.04-desktop-amd64](http://releases.ubuntu.com/18.04/ubuntu-18.04.5-desktop-amd64.iso)**_ 环境，装在物理机或虚拟机上都行，这个东西怎么装咱就不在这里给大家介绍了，网上一搜一大把，要是GPU的训练能在这个系统上完成就更好了；然后在这个操作系统中 _**[装一个MindStudio](https://support.huaweicloud.com/ug-mindstudioc75/atlasms_02_0186.html)**_ ，后边我们要用到。

上边的环境准备好之后，我们要来获取精度比对相关的数据了。

把大象装进冰箱，总共分三步：
1. 在GPU上dump出参数文件来
2. 在NPU上dump出参数文件和计算图来
3. 放到“精度比对工具”中比一比，看结果

现在咱们分别说说这三步要怎么操作。

### 在GPU上DUMP出参数文件
为了能让训练脚本在执行过程中输出特定格式的参数文件，我们要对训练脚本做一点点改动。
不论你用的是哪种方式（Estimator或Session.run），要进行精度比对的话，首先要把脚本中所有的随机全都关掉，包括但不限于你对数据集的shuffle，参数的随机初始化，以及某些算子的隐形随机初始化（比如dense算子）。这一步很关键，一定要查查tf的接口文档，确认自己脚本内所有的随机都关掉了，否则参数是随机初始化的，那比较还有什么意义呢？

去除随机之后，我们要把DUMP参数用到的代码加到训练脚本中去：
-  **如果你用的是Estimator来启动训练：** 
在引用包的地方加这么一行：
 **from tensorflow.python import debug as tf_debug** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/155319_e6cfe44d_5403319.png "屏幕截图.png")
然后在生成EstimatorSpec对象实例的时候，也就是构造网络结构的时候，加这么一行：
 **training_hooks=[tf_debug.LocalCLIDebugHook()]** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/155428_f777e989_5403319.png "屏幕截图.png")
-  **如果你用的是Session.run来启动训练：** 
在引用包的地方加这么一行：
 **from tensorflow.python import debug as tf_debug** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/155955_02622895_5403319.png "屏幕截图.png")
然后在session初始化结束后加这么一行：
 **sess = tf_debug.LocalCLIDebugWrapperSession(sess, ui_type="readline")** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/160522_4f3206ff_5403319.png "屏幕截图.png")

修改完之后，正常启动训练，训练任务会在某个时间点停下来，让你能够在控制台进行交互：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/160835_8927e6f1_5403319.png "屏幕截图.png")
这时，我们输入一个“run”并回车，训练会往下执行一个step，并停下来等待进一步命令：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/160913_d1d94b51_5403319.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/160949_31cbb474_5403319.png "屏幕截图.png")
这里，我们就尝试获取这第一个step的训练结果参数。
在这个控制台输入下边这个命令：
lt >  _随便起个文件名_ 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/161708_ae463819_5403319.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/161752_61e6645b_5403319.png "屏幕截图.png")
这时候，在你训练脚本所在目录下，就会多出一个叫“gpu_dump”的文件：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/161847_2d9d68fc_5403319.png "屏幕截图.png")
把这个“gpu_dump”文件拷贝到前边安装的Ubuntu系统中，比如放到
 **/home/_ascend_/Downloads/dump/gpu** 
这个目录下（注意这里的“ascend”是我在Ubuntu系统上的用户名，你的可能不一样）
然后打开终端工具：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/164223_1246bcfe_5403319.png "屏幕截图.png")
输入：

```
cd /home/ascend/Downloads/dump/gpu
timestamp=$[$(date +%s%N)/1000] ; cat gpu_dump | awk '{print "pt",$4,$4}' | awk '{gsub("/", "_", $3);gsub(":", ".", $3);print($1,$2,"-n 0 -w "$3".""'$timestamp'"".npy")}'
```

把屏幕上所有以“pt”开头的内容全部复制下来，一定要复制全，别遗漏：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/164725_9729cf60_5403319.png "屏幕截图.png")
复制好之后，回到刚才执行训练脚本的控制台，把复制好的这些内容直接粘上：
一切正常的话，效果应该是这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/170948_c1c37b8d_5403319.png "屏幕截图.png")
并且训练脚本所在目录突然多了很多以“.npy”为后缀的文件：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1202/171039_7eff121b_5403319.png "屏幕截图.png")
把这些npy文件都拷贝到Ubuntu环境上，就还放在
 **/home/_ascend_/Downloads/dump/gpu** 
就可以，到此为止，在GPU上DUMP第0个step数据的步骤就结束了。（想要比对第n个step的参数，多跑几个run就行）

### 在NPU上dump出参数文件和计算图
注意我这里的措辞，dump出来的结果除了“参数”，还有“计算图”，要注意这两者的区别，计算图是你搭建起来的神经网络的骨架，这里边是没有参数的，只有各种网络结构和算子的元数据。用建筑来做类比的话，计算图就是一栋建筑的钢结构，而参数则是每根钢梁周边的砖瓦和混凝土（可能不太恰当，理解意思就行）。

在启动dump之前，当然还是要对代码做一点修改的。
而在开始修改代码之前，一定要确保你的代码在网络结构上、算子、优化器的选择上，以及参数的初始化策略等方面跟GPU上训练的代码完全一致，否则比较起来也是没有意义的。
注意：
 **不要在一个训练脚本中既做训练又做验证** 
 **不要在一个训练脚本中既做训练又做验证** 
 **不要在一个训练脚本中既做训练又做验证** 
也就是不要把train和evaluate放到同一个脚本中，否则会生成两组dump数据，导致分不清用哪个

修改的第1步，跟GPU上的修改一样，一定要去除随机，所有的随机都要去掉，切记。

第2步，我们要分平台讨论了。
-  **如果你用的是ModelArts：** 
在训练机上生成的任何东西，我们都是无法直接拿到的，要传到OBS上我们才能访问。为了在训练结束后自动传输，我们要在训练脚本的包引用区域加这么两行：
 **import moxing as mox
import os** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/114254_cfe21724_5403319.png "屏幕截图.png")
为了让训练脚本能够dump出计算图，我们在训练脚本中，构建模型前加这样一行：
 **os.environ['DUMP_GE_GRAPH'] = '2'** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/165845_7f73f3a2_5403319.png "屏幕截图.png")
这样，在训练过程中，计算图文件会保存在训练脚本所在目录中。为了拿回这些计算图文件，在训练结束后添加这么一行：
 **mox.file.copy_parallel("./", FLAGS.train_url)** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/114428_81c0e9db_5403319.png "屏幕截图.png")
其中，“FLAGS.train_url”是你启动训练任务时配置的模型输出路径，应该是OBS上某个目录。
为了让训练脚本能够dump出我们想要的参数文件，在引用包的区域添加(只有Estimator场景需要添加，sessionrun场景不需要)：
 **from npu_bridge.estimator.npu.npu_config import DumpConfig** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/114544_31cefd60_5403319.png "屏幕截图.png")
我们在训练机上创建一个“/cache/data”目录：
 **TMP_CACHE_DIR = '/cache/data'
os.makedirs(TMP_CACHE_DIR)** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/114711_80ddbff9_5403319.png "屏幕截图.png")
下边的操作，要按前端区分：
- 如果你用的是Estimator
创建一个DumpConfig实例并把它加入到NPURunConfig中：
 **dump_config = DumpConfig(enable_dump=True, dump_path=TMP_CACHE_DIR, dump_step="0", dump_mode="all")** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/114908_3e7af445_5403319.png "屏幕截图.png")
- 如果你用的是Sessionrun
在创建session config的时候多加几行：

```
custom_op.parameter_map["enable_dump"].b = True
custom_op.parameter_map["dump_path"].s = tf.compat.as_bytes(TMP_CACHE_DIR)
custom_op.parameter_map["dump_step"].s = tf.compat.as_bytes("0|5|10")
custom_op.parameter_map["dump_mode"].s = tf.compat.as_bytes("all")
```


注意这里的“dump_path”是指dump出来的参数文件保存位置，这里我们就保存在刚才创建的目录中；
“dump_step”指的是想要dump出第几个step的结果，多个step之间用“|”隔开，连续多个step可以用诸如“5-10”来表达，这里我们为了举例，只dump第0个step的结果，跟GPU上保持一致。
记得把dump出来的结果传回OBS，这样我们才能拿到：
 **mox.file.copy_parallel(TMP_CACHE_DIR, FLAGS.train_url)** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/115157_29cb3c7e_5403319.png "屏幕截图.png")
修改完之后，正常启动训练，训练结束后去FLAGS.train_url指向的OBS路径上找dump结果：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/193509_a343f8f9_5403319.png "屏幕截图.png")
在上边这张图中，以“ge”开头的文件，是我们设置“DUMP_GE_GRAPH=2”的结果，而下边那个以时间戳为名称的文件夹，则是我们设置DumpConfig后，执行训练的产出。

-  **如果你用的是Apulis：** 
为了让训练脚本打印计算图，我们在训练脚本的包引用区域加一行：
 **import os** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/145257_37c185cf_5403319.png "屏幕截图.png")
然后在训练启动之前加一行：
**os.environ['DUMP_GE_GRAPH'] = '2'** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/145335_ea74e3d1_5403319.png "屏幕截图.png")
这样，在训练过程中，计算图文件会保存在训练脚本所在目录中。
为了让训练脚本打印参数文件，我们在训练脚本的包引用区域加一行(只有Estimator场景需要添加，sessionrun场景不需要)：
 **from npu_bridge.estimator.npu.npu_config import DumpConfig** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/145604_94bf7b39_5403319.png "屏幕截图.png")
下边的操作，要按前端区分：
- 如果你用的是Estimator
创建一个DumpConfig实例并把它加入到NPURunConfig中：
 **dump_config = DumpConfig(enable_dump=True, dump_path=TMP_CACHE_DIR, dump_step="0", dump_mode="all")** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/145721_d2eb9ada_5403319.png "屏幕截图.png")
- 如果你用的是Sessionrun
在创建session config的时候多加几行：

```
custom_op.parameter_map["enable_dump"].b = True
custom_op.parameter_map["dump_path"].s = tf.compat.as_bytes(TMP_CACHE_DIR)
custom_op.parameter_map["dump_step"].s = tf.compat.as_bytes("0|5|10")
custom_op.parameter_map["dump_mode"].s = tf.compat.as_bytes("all")
```

注意化参数中的“dump_path”写一个你自己创建的，确定具备写入权限的空目录。
“dump_step”指的是想要dump出第几个step的结果，多个step之间用“|”隔开，连续多个step可以用诸如“5-10”来表达，这里我们为了举例，只dump第0个step的结果，跟GPU上保持一致。

修改完之后，正常启动训练，在训练脚本所在目录找以“ge”开头的文件；在“dump_path”指向的目录中，找一个以时间戳命名的目录。

把上边说的这两样东西，都拷贝到安装了MindStudio的Ubuntu环境上，下边的操作我们要在这个Ubuntu环境上进行了。
敲黑板，划重点！！！！下边是老谭要教给大家的小秘诀了，操作有点多，希望大家认真听认真记:)

在我的Ubuntu机器上，这个目录大概像这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/222123_5c9bd80d_5403319.png "屏幕截图.png")
文件非常多是吧，不要慌。
首先，在命令行输入这么个命令：
 **`grep Iterator *_Build.txt`** 
也就是在所有以“_Build.txt”为结尾的文件中，查找“Iterator”这个关键词。（至于为什么这么做，话题比较大，我们先不在这里讨论）
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/221753_7da64842_5403319.png "屏幕截图.png")
这时我们能够看到，有这个关键字的文件名都是相同的，在我这个场景下就是“ge_proto_00292_Build.txt”，记住这个文件名，这个是计算图文件，一会儿比对的时候要用。
接下来，我们看一下这个文件的头10行：
`head -n 10 ge_proto_00292_Build.txt`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/221930_3c2dcd2d_5403319.png "屏幕截图.png")
记住这个“name”字段的值，最好复制下来。
下面，我们进入那个以时间戳命名的文件夹，在我这个场景下，目录名是20201209083219：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/222346_639e97bc_5403319.png "屏幕截图.png")
继续深入：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/222405_3e3497f8_5403319.png "屏幕截图.png")
这里出现了三个目录，还记得刚才复制的那个“name”字段的值吗？进入以这个值命名的目录：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/222527_b8891c79_5403319.png "屏幕截图.png")
继续向下深入，直到找到很多文件为止。这个目录里存放的，就是我们想要的参数dump结果了。

### 放到“精度比对工具”中比一比，看结果
打开前边让你安装的“MindStudio”，随便新建一个训练工程：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1209/222834_8b38c4db_5403319.png "屏幕截图.png")
菜单栏Ascend->Model Accuracy Analyzer是我们需要的模型精度比对工具。
在左边的MyOutput这里选择在NPU上dump出来的数据，也就是上一步的参数dump结果目录；在右边的Ground Truth这里选择第一大步中，GPU Dump出来的结果所在目录;在下一行的Compare Rule Configuration处选择刚才那个“ge_proto_00292_Build.txt”文件：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/100052_87be0fd6_5403319.png "屏幕截图.png")
然后点击下方的“Compare”按钮，之后，就是见证奇迹的时刻---------------------------------------->
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/100324_ce489da8_5403319.png "屏幕截图.png")
下方的表格中，出现了网络中每个算子的横向比对情况，其中每一列的含义是这样的：

- LeftOp：表示基于昇腾AI处理器运行生成的dump数据的算子名。
- RightOp：表示基于GPU/CPU运行生成的npy或dump数据的算子名。
- TensorIndex：表示基于昇腾AI处理器运行生成的dump数据的算子的input ID和output ID。
- CosineSimilarity：进行余弦相似度算法比对出来的结果，范围是[-1,1]，比对的结果如果越接近1，表示两者的值越相近，越接近-1意味着两者的值越相反。
- MaxAbsoluteError：进行最大绝对误差算法比对出来的结果，值越接近于0，表明越相近，值越大，表明差距越大。
- AccumulatedRelativeError：进行累积相对误差算法比对出来的结果，值越接近于0，表明越相近，值越大，表明差距越大。
- RelativeEuclideanDistance：进行欧氏相对距离算法比对出来的结果，值越接近于0，表明越相近，值越大，表明差距越大。
- KullbackLeiblerDivergence：进行KLD散度算法比对出来的结果，取值范围是 0 到无穷大。KL 散度越小，真实分布与近似分布之间的匹配越好。
- StandardDeviation：进行标准差算法比对出来的结果，取值范围：0到无穷大。标准差越小，离散度越小，表明越接近均值。该列显示两组数据的均值和标准差，第一组展示基于昇腾AI处理器运行生成的dump数据的数值(均值;标准差)，第二组展示基于GPU/CPU运行生成的dump数据的数值(均值;标准差)。
- 显示“*”，表示新增的算子、无对应的原始算子；“NaN”表示无比对结果。
- 余弦相似度和KLD散度比较结果为NaN，其他算法有比较数据，则表明左侧或右侧数据为0；KLD散度比较结果为Inf，表明右侧数据有一个为0。
在我的比对中，是GPU和NPU上训练的第0个step，还没有产生任何差异，所以呈现的是处女座最喜欢的整齐样貌，如果训练step多一些，可能就不会这样完全相同了。

再告诉大家一个秘密：双击表格中任何一个算子，有惊喜哦！
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/100726_0b1e5732_5403319.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/100753_d599520e_5403319.png "屏幕截图.png")

如果发现某个算子上的参数差异过大，可能是整网精度较低的原因，可以先自行分析一下这个算子的特性，看有无修改方法，调调参，炼炼金，然后再比比。如果还不行的话，就痛痛快快的来提ISSUE吧，我们一起来解决。
OK，精度问题分析的话题就聊到这里。

喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！





`需要任何修改请联系老谭进行评估。`