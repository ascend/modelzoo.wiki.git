### 2020/1/20:平台升级了20.2版本，Profiling工具已重构，使用上可能和本文有出入，请酌情使用
原谅老谭在这里标题党了一下，其实就训练性能优化这个话题来说，在座各位都比老谭有经验多了，我也就不在这里班门弄斧了。但是想要优化性能，至少得先有优化方向，或者说至少得先有性能数据，才能有针对性的优化。老谭在这篇Wiki中想跟大家分享的，就是昇腾软件栈自带的性能数据分析工具——profiling。

在当前场景下，想要使用profiling工具来进行性能分析，总共分三个步骤：
- 在 **运行环境** 上进行 **性能数据采集** 
- 把采集到的的数据发送到开发环境上
- 在 **开发环境** 上调用脚本 **对性能数据进行解析** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/115210_cdad3315_5403319.png "屏幕截图.png")

训练时生成的性能数据是用二进制格式保存的，没法直接用于阅读，所以需要用专用工具来解析这部分数据以供阅读。
很幸运，在Apulis平台上，你拿到的环境都是安装了Toolkit的，也就是开发/运行合一的环境，所以操作会简化许多。下面开始跟老谭一步一步操作。

### 在运行环境上进行性能数据采集
采集性能数据需要我们对训练脚本做一点改动，和前边几篇Wiki一样，我们分情况讨论。
 **对脚本做profiling改造之前，记得把test和evaluate去掉。训练脚本只训练，验证脚本只验证，否则会打印多组profiling数据，就找不到你想要的了。** 
1. 用Estimator启动训练的
首先，在引用包的区域新增一行：
`from npu_bridge.estimator.npu.npu_config import ProfilingConfig`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/120141_d2aa61fd_5403319.png "屏幕截图.png")
其次，在创建NPURunConfig对象之前，添加这样两行：

```
profiling_options = ['task_trace'] 
profiling_config = ProfilingConfig(enable_profiling=True, enable_options = profiling_options, fp_point="dense/MatMul", bp_point="gradients/dense/MatMul_grad/MatMul_1")
```

并将profiling_options加入NPURunConfig
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/120659_5ad083a9_5403319.png "屏幕截图.png")
上边的“fp_point”和“bp_point”获取起来比较麻烦，要根据你具体的网络来分析，下边我会跟大家分享获取方法。

2. 用Session.run启动训练的
在创建custom_op的时候添加下边几行：

```
custom_op.parameter_map["profiling_mode"].b = True
custom_op.parameter_map["profiling_options"].s = tf.compat.as_bytes("task_trace") 
custom_op.parameter_map["fp_point"].s = tf.compat.as_bytes("resnet_v1_50_1/conv1/Conv2D")
custom_op.parameter_map["bp_point"].s = tf.compat.as_bytes("add_1")
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/121031_a118d7b9_5403319.png "屏幕截图.png")
上边的“fp_point”和“bp_point”获取起来比较麻烦，要根据你具体的网络来分析，下边我会跟大家分享获取方法。

-  **从网络结构文件中获取fp_point和bp_point的方法** 
fp_point指的是forward propagation，bp_point指的是backward propagation，在profiling中特指网络计算图的第一个前向传播算子和第一个反向传播算子。
在训练过程中，通常我们保存的结果是checkpoint和meta文件，但在分析前向和反向算子的时候，需要网络结构文件（graph.pbtxt），一般用tf.io.write_graph来保存计算图（不会用的童鞋去Google一下），如果用Estimator启动训练的话，在RunConfig里边配置个model_dir参数也能打印计算图文件。总之，训练结束后，去model_dir找graph.pbtxt就行。

打开这个文件，里边大概是这样：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/131021_fc2456a4_5403319.png "屏幕截图.png")
每一个“node”指的是一个算子，我们要观察的是“op”这个属性：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/131128_d29487bf_5403319.png "屏幕截图.png")
找fp_point的时候，从上往下找，找到第一个“计算”类算子，所谓“计算类”算子，可以排除“数据类型”和“存储类”算子，也就是排除诸如Const， VariableV2，IteratorV2，Identity，Reshape，Case等算子，或者说“name”这个属性中带有“step”、“Dataset”“seed”“kernel”的算子都可以排除。
在我的场景下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/131545_daaacf70_5403319.png "屏幕截图.png")
到1732行才找到第一个计算类算子，op字段也很直观，“MatMul”，矩阵乘，一看就是计算类的了。我们想要的“fp_point”值取这个算子的“name”，也就是“dense/MatMul”

找bp_point的时候，从下往上找，搜索“gradients”，也就是梯度，同样，排除“op”字段值是Assign、Const、NoOp等。
在我的场景下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/132109_c40391ae_5403319.png "屏幕截图.png")
找到4457行才找到第一个反向传播算子，op是MatMul，name中带有gradients，bp_point取“gradients/dense/MatMul_grad/MatMul_1”。

找bp/fp是个手艺活，多找找就有感觉了:)
（说个小技巧，在Estimator场景下，保存的graph.pbtxt里边带了参数，会使得这个文件特别大，一般的文本编辑器根本打不开，包括vim。这个时候，尝试一下Ultra Edit，有惊喜）

训练脚本修改完成之后，启动训练之前，要先启动一个小工具。
在控制台输入如下几个命令：

```
sudo su
cd /usr/local/Ascend/driver/tools
./ada --docker
```

然后用 **root权限** 启动训练，训练结束之后，profiling性能数据就生成了。

### 把采集到的的数据发送到开发环境上
因为Apulis平台上安装了Toolkit，可以理解为开发和运行环境是合一的，所以我们还在这台机器上进行数据分析就可以，但是要把生成的数据拷贝到另一个地方。
在root用户下执行如下几个命令：

```
mkdir -p /home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/profiler_data/result_dir/127.0.0.1
cd /var/log/npulog/profiling/container
```

在/var/log/npulog/profiling/container目录下进入一个以时间戳为名称命名的目录，应该就是你刚才训练的时间，在我的场景下是这个：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/134249_9bb3e9bb_5403319.png "屏幕截图.png")
进来之后，找到一个以“JOB”开头的目录：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/134357_7430b52d_5403319.png "屏幕截图.png")
执行下边这条命令把这个JOB开头的目录复制到刚才创建的“127.0.0.1”目录：
`cp -r JOBBJJEBCIJHAACJCADAAFAAAAAAAAAA/ /home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/profiler_data/result_dir/127.0.0.1`
这样，复制profiling数据的操作就结束了，可以开始分析了。

### 在开发环境上调用脚本对profiling数据进行解析
开发环境上的profiling数据分析工具叫“hiprof.sh”，在这里：
`/home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/bin`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/162312_b5d9d66f_5403319.png "屏幕截图.png")
所以，在root用户下执行：
`cd /home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/bin`
首先，要启动profiling服务来解析我们的数据：
`./start.sh`
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/162439_9a46ee19_5403319.png "屏幕截图.png")
稍等一会儿，数据就能够解析完毕。至于要等多久呢？这里有个经验之谈：
cd /home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/profiler_data/sql
观察这里边有没有数据：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/162609_468dd7b0_5403319.png "屏幕截图.png")
如果有，基本上可以说明数据解析完毕了。回到刚才的bin目录，我们要用hiprof.sh脚本进行数据提取了。我们生成的profiling数据，在分析时分为两类：
- Training Trace：采集训练任务及AI软件栈的软件信息，实现对训练任务的性能分析，重点关注数据增强、前后向计算、梯度聚合更新等相关数据。
- Task Trace：采集昇腾910处理器HWTS/AICore的硬件信息，分析任务开始、结束等信息。

其中，每一类数据中又有如下几种具体数据：
- Training Trace：
1. [获取指定训练任务的Job Profiling迭代数目](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0018.html)
2. [获取指定训练任务的迭代时长/数据增强拖尾/FPBP运算/梯度更新拖尾时间](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0019.html)
3. [导出指定训练任务指定AI Server上指定Device的迭代轨迹数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0020.html)
4. [导出指定训练任务的所有迭代数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0021.html)

- Task Trace
1. [导出指定训练任务指定AI Server上指定Device的某轮迭代HWTS（Chrome trace）数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0023.html)
2. [导出指定训练任务指定AI Server上指定Device的某轮迭代HWTS Task数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0024.html)
3. [导出指定训练任务指定AI Server上指定Device的AI Core数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0025.html)
4. [导出指定训练任务指定AI Server上指定Device的HCCL数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0026.html)
5. [导出指定训练任务指定AI Server上指定Device的算子基本信息](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0027.html)
6. [导出指定训练任务指定AI Server上指定Device的L2 Cache数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0030.html)
7. [导出指定训练任务指定AI Server上指定Device的某轮迭代的AI Core、AI CPU、All Reduce并行度分析数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0031.html)
8. [导出指定训练任务指定AI Server上指定Device的某轮迭代的AI Core算子计数表](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0032.html)
9. [导出指定训练任务指定AI Server上指定Device的某轮迭代的AI Core OP Summary数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0033.html)
10. [获取指定训练任务的Job Profiling任务原始数据目录路径](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0034.html)
11. [查询指定训练任务指定AI Server上指定Device的数据增强数据](https://support.huaweicloud.com/tg-training-cann/atlasprofilingtrain_16_0035.html)

这里我以一个典型的分析命令来给大家举例：
导出指定训练任务指定AI Server上指定Device的某轮迭代HWTS Task数据：
命令模板：
bash hiprof.sh --save_file --type=get_task_total --job_id=395013 --ip_address=127.0.0.1 --device_id=0 --iteration_id=0
其中
- job_id:在环境上用`env | grep JOB_ID`来获取
- ip_address：填127.0.0.1就行
- device_id：在Apulis场景下填0就行
- iteration_id：你想查看第几个迭代的数据
结果：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/171956_8326b1ec_5403319.png "屏幕截图.png")
可以看到，是生成了一个.csv文件，我们可以把这个文件下载到自己的Windows上打开：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/172211_48c27949_5403319.png "屏幕截图.png")
具体每一列的含义参考我上边给出的链接，打开之后最底下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1211/172247_174b4016_5403319.png "屏幕截图.png")
其他的数据获取方法跟这个也是类似的，可以每个都尝试尝试看看。

以上就是profiling工具的使用方式，用完这个工具，性能数据是提供给大家了，具体怎么优化，就得靠大家自己发挥聪明才智了。:)


喜欢我的Wiki的，还不快[ _去论坛上夸夸我_ ](https://bbs.huaweicloud.com/forum/forum-726-1.html)！动作要快，姿势要帅！
觉得我这篇Wiki写的不够好，或者有错误？[ _点这里向我开火_ ](https://gitee.com/ascend/modelzoo/issues)，谢谢！


`需要任何修改请联系老谭进行评估。`