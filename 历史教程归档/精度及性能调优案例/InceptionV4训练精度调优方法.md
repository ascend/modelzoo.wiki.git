**网络介绍**：InceptionV4网络是Google Inception系列网络的迭代更新，其将Inception和Residual结构结合起来组成没有残差链接的Inception变种。更复杂、精巧的Inception v4网络，在没有residual connections的情况下达到了与Inception-Resnet v2近似的精度。

Inception：一层block包含卷积和池化，增加网络宽度，同时学习稀疏（3x3， 5x5）和不稀疏（1x1）特征；Stem结构使用了不对称卷积核，在保证信息损失足够小的情况下，使得计算量降低，1x1的卷积核用来降维。非对称卷积核拆分：1x7 然后7x1 视野域和7x7一样，降低参数量


**精度调优历程：**

通过调整学习率、loss函数、混合精度、预处理，不断完成性能提点，最终达到论文精度，完成验收目标

![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/191108_d9c1e3cf_8267113.png "截图.PNG")

**预处理优化：**

![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/191029_b739fc1b_8267113.png "截图1.PNG")

随机裁剪不是完全随机：scale=(0.08, 1.0), ratio=(0.75, 1.333)

色调没有必要调整

谷歌大脑提出了自动数据增强方法(AutoAugment)

优化器： https://zhuanlan.zhihu.com/p/32626442

意味着参数更新方向不仅由当前的梯度决定，也与此前累积的下降方向有关。这使得参数中那些梯度方向变化不大的维度可以加速更新，并减少梯度方向变化较大的维度上的更新幅度。由此产生了加速收敛和减小震荡的效果。



**余弦退火学习率：**

![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/190956_b5df9358_8267113.png "截图2.PNG")

**RMSProp优化器：**

![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/191006_8c898b5a_8267113.png "截图3.PNG")

**总结：**

1. 合适的数据增强（预处理）有利于提升模型效果

2. 学习率更新策略配合优化器使训练收敛更好

3. 多卡并行、混合精度（1616fps-->2455fps）训练，参数并行更新（2455fps-->2708fps）能加快模型训练速度

4. Auto-tune可优化卷积算子执行时间，提升模型性能（遗留）

