## 问题背景 
经过finetune，PWCNet模型在MPI-Sintel-Clean数据集上取得精度EPE为1.76，论文EPE为1.70。Finetune精度与论文有所差距，我们希望能进一步优化模型训练，消除与论文的差距。  
根据[PWCNet](https://arxiv.org/pdf/1709.02371.pdf)中的描述，首先PWCNet需要在[FlyingThings3D](https://lmb.informatik.uni-freiburg.de/resources/datasets/SceneFlowDatasets.en.html)以及[FlyingChairs](https://lmb.informatik.uni-freiburg.de/resources/datasets/SceneFlowDatasets.en.html)两个数据集上进行预训练，再在[MPI Sintel](http://sintel.is.tue.mpg.de/)或者[KITTI](http://www.cvlibs.net/datasets/kitti/)上进行Finetune。在进行预训练时，作者使用的损失函数是
![L2 Loss](https://images.gitee.com/uploads/images/2021/0108/140610_9b1f2be3_8333118.png "1610085682(1).png")。
在进行Finetune时，作者使用的损失函数是
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/140754_a4c704a5_8333118.png "1610086057(1).png")。

## 原因分析 
一般来说，相同任务的网络模型在预训练和Finetune情况下使用的损失函数是相同的，在这里作者为了给离群点较小的惩罚，作者特意修改了Finetune阶段的损失函数。
但是我们发现，使用这个损失函数在Finetune时，网络收敛的并不好，训练过程如下所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/143752_9d25e644_8333136.png "Screenshot from 2021-01-08 14-37-16.png")

进一步，我们分析了Finetune前和Finetune后的模型的表现，我们选出了EPE值最大的Top-10样本，我们认为这些点是离群点，绘制箱型图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/143506_c7df490e_8333118.png "1610087650(1).png")
从图中可以看出，即便一阶范数的损失函数给离群点更小的惩罚，Finetune之后的离群点的EPE也能够显著减小，并且这些离群点之间的方差也变得更小。

更进一步，我们分析了MPI Sintel数据集和FlyingThings3D以及FlyingChairs数据集的特点，我们发现MPI Sintel数据集的平均光流场流速是13.5，而FlyingThings3D与FlyingChairs数据集的平均流速分别是38和11.1. 我们认为数据之间的这种不一致性对结果是有负面影响的。

## 改进方法 
基于设想一，我们在之前finetune的基础上，继续使用预训练时的二阶损失进行训练，此时离群点对训练的影响较小，也能被较好地训练。
基于设想二，我们希望尽量保持预训练数据和Finetune数据集的一致，所以我们对FlyingThings3D数据集进行了两倍降采样，从而使其平均流速变为19，这样数据之间更加接近。
 **基于这两个设想，最终精度达到1.25，优于最开始的1.76，甚至比paper里的1.70高出26%。** 
