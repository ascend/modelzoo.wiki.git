- 为什么需要在Tesla V100的GPU环境上跑性能数据？
因为我们希望迁移在Ascend 910上的网络，它的训练性能是Tesla V100的1.2倍。因此，我们需要拿到该网络在Ascend 910和Tesla V100上的训练性能数据。如果您已经将网络在Ascend910上迁移成功了，那么它在Ascend910上的性能数据很容易可以拿到。但还缺少Tesla V100上的性能数据，本文可以快速帮你解决环境问题。

本文主要讲解如何在弹性云服务器ECS上快速的 基于已有的共享镜像 创建Tesla V100的GPU训练环境，用于获取网络模型的训练性能标杆数据。镜像中已经安装的主要软件版本如下表格。  :exclamation: :exclamation: :exclamation:需要注意的是，ECS服务器是消耗Modelarts代金券的，即使[弹性云服务器ECS关机后仍会计费](https://support.huaweicloud.com/ecs_faq/zh-cn_topic_0018124776.html?utm_campaign=ua&utm_content=ecs&utm_term=buyecs)，如果长时间不用了记得 **删除它** **删除它** **删除它**。

| 软件 | 版本信息 |
|---------|---------|
|  Nvidia-Driver | 418.67 |
|  Cuda | 10.0 |
|  Cudnn | 7.6.0 |
|  Tensorflow-gpu | 1.15.0 |
|  Python | 3.7.5 |
|  Numpy | 1.20.0 |

### 目录
- [获取训练共享镜像](#获取训练共享镜像)
- [创建GPU训练服务器](#创建GPU训练服务器)
- [环境测试](#登入并测试)
- [其他](#其他)

### <span id="获取训练共享镜像">获取训练共享镜像</span>
为什么标题说是快速创建训练环境呢？因为我们已经给大家制作好了一套镜像，里面安装好了Nvidia-Driver,Cuda,Cudnn以及Python等，不需要每位开发者在弹性云服务器ECS上重新再搭建一套，直接使用即可；同时，大家公用一套环境，其中的软件包版本也是固定的，在对比性能数据时有可比较性。当然，感兴趣的同学仍然可以基于弹性云服务器ECS动手试试，看是否能够自己搭建一套训练环境，整个过程还是需要花点时间的。

问题来了，如何才能获取到共享的训练镜像呢？分两种情况：
 **第一种情况**，如果项目组同学都没有，则向华为方获取，大致步骤如下：
- 首先，向华为的项目经理或者接口人提供项目ID（在控制台我的凭证获取），说明希望获取Tesla V100的GPU训练镜像
![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095122_8122b600_8208106.png "屏幕截图.png")
- 其次，华为方会有对应的同事将该镜像通过共享的方式共享给你
![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095350_ee0a9544_8208106.png "屏幕截图.png")
- 然后，共享给你之后，你还需要 [**打开链接**](https://console.huaweicloud.com/ecm/?agencyId=0703e2ce96800ff21f35c01d3901ff44&region=cn-north-4&locale=zh-cn#/ims/manager/imageList/selfImage) 接收共享邀请
    ![接受邀请](https://images.gitee.com/uploads/images/2020/1218/113910_2f01ecec_1482256.png "屏幕截图.png")
- 最后，在后面创建ECS训练服务器时，你可以在 **镜像-共享镜像** 中看到已经给你共享的镜像
    ![共享镜像](https://images.gitee.com/uploads/images/2021/0203/174332_d95eb8d3_1482256.png "共享镜像.png")

 **第二种情况**，如果项目组有同学已经获得过共享镜像，可以通过他分享获取，大致步骤如下：
- 复制共享镜像，并且取名为 copy_gpu_training，名字自己随意取。
![复制镜像1](https://images.gitee.com/uploads/images/2021/0201/161318_3b6993e1_1482256.png "复制镜像1.png")
![复制镜像2](https://images.gitee.com/uploads/images/2021/0203/174225_86d3c4e4_1482256.png "复制镜像2.png")
- 点击 **确定** 后稍等几分钟完成后，在旁边的私有镜像中可以选择右侧的**更多->共享**，输入华为云账号，分享给项目组的其他同学。

:exclamation:注意，当前共享的GPU训练镜像是在**华北-北京4**，在其他区域上创建ECS服务器时该镜像是不可见的。

### <span id="创建GPU训练服务器">创建GPU训练服务器</span>
- 首先，登入[ **云服务器控制台** ](https://console.huaweicloud.com/ecm/?agencyId=0703e2ce96800ff21f35c01d3901ff44&region=cn-north-4&locale=zh-cn#/dashboard) 购买弹性云服务器。

    ![购买ecs入口](https://images.gitee.com/uploads/images/2020/1218/101313_42d8853b_1482256.png "购买ecs入口.png")

    计费模式选择 **按需计费** ，区域选择 **华北-北京4** ，CPU架构选择 **X86** ，规格选择 **GPU加速型** 中的 **g5.8xlarge.4 或者 p2v.2xlarge.8**，这两款V100的显存都是16G。

    ![购买配置1](https://images.gitee.com/uploads/images/2021/0201/150556_6b9391f3_1482256.png "购买配置1.png")

    镜像选择**共享镜像** ，就是刚开始第一节**获取训练共享镜像**拿到的。系统盘旁边的数据盘根据自己需要是否增加数据盘。当前提供的训练镜像系统盘有100G，根据各自模型需求看是否可放下训练数据。另外，由于只是简单的获取性能数据，最多也就跑1~2小时，建议只传部分数据集，不用将全量训练数据集上传。
    ![共享镜像和系统盘](https://images.gitee.com/uploads/images/2021/0203/174332_d95eb8d3_1482256.png "共享镜像和系统盘.png")

- 进入到下一步，可进行网络，弹性公网IP( **按流量计费** )，带宽大小的申请，没有特别要注意的地方，可参照如下截图。

    ![弹性公网IP](https://images.gitee.com/uploads/images/2021/0201/162625_a31854eb_1482256.png "弹性公网IP.png")

- 再进入到下一步，可设置 **云服务器名字** 和 **服务器的root密码** 。此处名字我取的是 **ecs-gpu-training** ，此处特别要注意，该密码必须记住，后面登入服务器时需要使用。

    ![云服务器名字和root密码](https://images.gitee.com/uploads/images/2021/0201/151104_0c2914cd_1482256.png "云服务器名字和root密码.png")

- 最后进行配置的确认和最终的购买，并返回查看刚刚在ECS上购买的GPU训练服务器。Here，我们可以看到分配的公网IP，后面我们可以使用这个IP进行SSH登入使用了。

    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1218/104136_d62a6446_1482256.jpeg "6.jpg")
### <span id="登入并测试">登入并测试</span>
当前共享的镜像中，我们内置了一个AlexNet网络当作Demo，供大家验证测试购买的训练服务器是否可用。AlexNet的训练代码路径为/root/alexnet，需要用到的数据集在路径为/root/Flowers-Data-Set。
启动内置的AlexNet网络样例测试的步骤如下：
1. Step1:切换到Alexnet网络工程目录
    `cd /root/alexnet/`
2. Step2:执行训练脚本
    `sh run_1p.sh`

训练脚本起来后，可以看到如下截图的性能耗时数据打印。
![Alexnet性能数](https://images.gitee.com/uploads/images/2021/0201/170709_0f41cc3c_1482256.png "Alexnet性能数.png")
同时，如果你在另外一个Terminal中输出`watch -n 1 nvidia-smi`，可以看到当前网络在GPU上的利用率，类似如下截图情况。
![GPU利用率](https://images.gitee.com/uploads/images/2021/0201/170634_1faeacf1_1482256.png "GPU利用率.png")
### <span id="其他">其他</span>
- apt-get源的更新和软件包安装
    默认提供的ubuntu源在安装软件时可能比较慢，可以使用如下命令进行替换国内的源，再进行软件包的安装。
    ```
    rm -rf /etc/apt/sources.list
    cp /root/sources.list.ubuntu16 /etc/apt/sources.list
    apt-get update
    ```
- pip3安装python包
    当前安装的python packages可能不能满足所有人的需求，如有需求请自行通过pip3安装。
- 性能优化
    在昇腾平台上的Tensorflow网络性能优化，可以使用官方发布的[**Auto Tune工具**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasautotune_16_0001.html)和[**Profiling工具**](https://support.huaweicloud.com/Development-tg-cann202training1/atlasprofilingtrain_16_0003.html)进行优化。由于涉及的内容比较多，在此篇wiki就不进行阐述，详细参看这两篇wiki——[**网络性能优化指引**](https://gitee.com/ascend/modelzoo/wikis/%E7%BD%91%E7%BB%9C%E8%AE%AD%E7%BB%83%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96%E6%8C%87%E5%BC%95(20.2)?sort_id=3652440)和[**Auto Tune性能调优工具**](https://gitee.com/ascend/modelzoo/wikis/Modelarts%E4%BD%BF%E7%94%A8AutoTune%E6%80%A7%E8%83%BD%E8%B0%83%E4%BC%98%E5%B7%A5%E5%85%B7?sort_id=3647780)