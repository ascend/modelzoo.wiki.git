**问题背景：**

YOLOV4网络训练MAP到40.4%后，精度提升遇到“瓶颈”，尝试调试多组参数，包括延长训练epo、初始lr等，mAP指标均在40.4%附近波动，难以超过40.4%，对比当前实验结果与论文各项精度指标差距，发现主要差距体现在当前模型对中小物体检测能力弱于论文，对大物体的检测能力强于论文，由此尝试调整loss配比，加强对中小物体的检测能力训练

**思路**：和论文精度指标比对，分析弱势指标，调整loss配比

**TOP问题**：

**最后“一公里”，精度提升遇到“瓶颈”**

MAP训练到40.4%后，精度提升遇到“瓶颈”，尝试调试多组参数，包括延长训练epo、初始lr等，mAP指标均在40.4%附近波动，难以超过40.4%

**分析**：对比当前实验结果与论文各项精度指标差距，发现主要差距体现在当前模型对中小物体检测能力弱于论文，对大物体的检测能力强于论文，由此尝试调整loss配比**，**加强对中小物体的检测能力训练

**方法**：和论文精度指标比对，分析弱势指标，调整loss配比

YOLOv4 loss计算包含三部分：总loss = 定位loss + 置信度loss + 分类loss

**效果**：对中小物体检测能力提升，补齐弱项，精度达到并超过论文公开精度



**多尺度训练loss跳变、发散**

算子嵌套叠加运算时，精度不够，导致随着训练的进行，误差累计越来越大，误差达到上限时，nan踩内存，导致算子溢出，loss发散，单模块校验时，me上mish与pytorch上的mish前向结果与反向梯度一致（误差控制在1e-5以内），接入整网训练时，在训练一定epoch后，误差累计导致发散。P.Exp()+P.Log()算子叠加嵌套，精度不够

解决方法：P.Softplus()集成算子，解决算子精度不够的问题，采用P.Softplus()算子后，mish接入整网中，多尺度训练能正常收敛，精度正常

![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/190627_88e23b39_8267113.png "截图1.PNG")![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/190636_f7d73b1c_8267113.png "截图2.PNG")

**涨点组件**：

结构：CSPDarknet53、SPP、PAN

激活函数：Mish

tricks：Eliminate grid sensitivity, multiple anchors for a GT, Cosine annealing scheduler, Optimized Anchors

loss：GIoU, CIoU, DIoU

后处理：DIoU-NMS



**尝试过但无明显涨点的方法：**

Mosaic, GridMask, FocalLoss



**想要尝试但是当前不支持的方法**

*Exponential Moving Average* compute the moving averages of trained parameters using exponential decay



**精度提升效果**：608分辨率下，map超越论文基线1.2%