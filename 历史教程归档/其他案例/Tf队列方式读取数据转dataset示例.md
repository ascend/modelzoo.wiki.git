1、排查脚本中是否是以队列的方式读取数据：
关键api：tf.WholeFileReader()、tf.train.string_input_producer、tf.train.batch以及使用slim的dataset api等。
反向进行排查：搜索脚本中是否包含tf.data或者tf.contrib.data相关的api，没有找到说明使用的是队列的方式读取数据，需要整改。

2、队列方式转dataset方式的要点
首先找到队列方式读取的原始数据集，查看原始数据集的类型，比如图片，文本等。根据数据集的类型选择dataset的对应api进行原始数据的读取。
读取到数据之后，下一步就是对读取到的数据进行预处理操作，这部分两种方式处理基本类似，都是使用的tf.image的相关api

--数据读取的方式
队列读取数据的方式：tf.train.string_input_producer

dataset读取数据的方式：
tfrecord：tf.data.TFRecordDataset
从原始路径读取文本：tf.data.TextLineDataset
从原始路径读取图像：tf.data.Dataset.from_tensor_slices
更多方式参考：https://www.tensorflow.org/tutorials/load_data/images?hl=zh-cn

--数据预处理的方式
读取到数据之后，两种方式对于数据的处理方式以及使用的api都是类似的，可以直接借用。
例如tf.image.random_flip_left_right、tf.image.crop_to_bounding_box、tf.image.resize_images等


以下是一个网络从input_producer转成dataset的示例：
该网络的原始数据集是图片，可以采用dataset的tfrecord和直接读取图片路径的方式来读取原始数据，本例子使用的是tfrecord

input_producer的数据读取加数据预处理：
    def read_input(self):
        with tf.device('/cpu:0'):
            reader = tf.WholeFileReader()
            filename_queue = tf.train.string_input_producer(self.data)
            data_num = len(self.data)
            key, value = reader.read(filename_queue)
            image = tf.image.decode_jpeg(value, channels=self.c_dim, name="dataset_image")

            image = tf.image.random_flip_left_right(image)
            image = tf.image.crop_to_bounding_box(image, (218 - self.input_height) // 2, (178 - self.input_width) // 2,
                                                  self.input_height, self.input_width)
            if self.crop:
                image = tf.image.resize_images(image, [self.output_height, self.output_width],
                                               method=tf.image.ResizeMethod.BICUBIC)

            num_preprocess_threads = 4
            num_examples_per_epoch = 800
            min_queue_examples = int(0.1 * num_examples_per_epoch)
            img_batch = tf.train.batch([image],
                                       batch_size=self.batch_size,
                                       num_threads=4,
                                       capacity=min_queue_examples + 2 * self.batch_size)
            img_batch = 2 * ((tf.cast(img_batch, tf.float32) / 255.) - 0.5)

            return img_batch, data_num


dataset的读取数据：
tfrecord的方式需要先制作tfrecord：
import tensorflow as tf
import os
import cv2

def int64_list_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def image2tfrecords(data_dir, tf_record_path):
    tf_write = tf.python_io.TFRecordWriter(tf_record_path)
    for image_name in os.listdir(data_dir):
        image_dir = os.path.join(data_dir,image_name)
        org_image = cv2.imread(image_dir)
        image = org_image.tobytes()
                   # tfrecord中需要存储哪些数据，需要根据网络的输入进行选择，比如存image，label。
                    # 本用例中存储的img_shape是由于预处理需要对原始图片进行裁剪，所以需要保存固定shape方便解析时恢复原始大小。
                    # 如果预处理对固定shape的图片进行预处理，则不需要保存img_shape。
        example = tf.train.Example(
            features=tf.train.Features(feature={
                'image': bytes_feature(image),
                'img_shape': int64_list_feature(org_image.shape)}))
        tf_serialized = example.SerializeToString()
        tf_write.write(tf_serialized)
    tf_write.close()
    print('already!!!!!!')

image2tfrecords("./img_align_celeba", "./train-tfrecords")


对应的解析tfrecord的函数
    def parse_record(self, example):
                  # tfrecord中存储的map
        features = {
            'image':
                tf.FixedLenFeature((), tf.string, default_value=''),
            'img_shape':
                tf.FixedLenFeature(shape=(3,), dtype=tf.int64)}
        parsed = tf.parse_single_example(example,features=features)
        
        # 数据预处理方式与input_producer方式保持一致
        image = tf.decode_raw(parsed['image'], out_type=tf.uint8)
        image = tf.reshape(image, shape=parsed['img_shape'])
        image = tf.image.random_flip_left_right(image)
        image = tf.image.crop_to_bounding_box(image, (218 - self.input_height) // 2, (178 - self.input_width) // 2,
                                              self.input_height, self.input_width)
        if self.crop:
            image = tf.image.resize_images(image, [self.output_height, self.output_width],
                                           method=tf.image.ResizeMethod.BICUBIC)
        image = 2 * ((tf.cast(image, tf.float32) / 255.) - 0.5)
        image.set_shape([self.output_height, self.output_width, 3])
        return image

读取tfrecord并做数据预处理：
    def read_input(self):
        with tf.device('/cpu:0'):
            data_num = len(self.data)
            dataset = tf.data.TFRecordDataset(["./data/train-tfrecords"]).repeat() # 数据读取
            dataset = dataset.map(lambda value: self.parse_record(value), num_parallel_calls=64) #数据预处理
            dataset = dataset.batch(self.batch_size, drop_remainder=True).prefetch(64)  #取batch并进行预取操作
            self.iterator = dataset.make_initializable_iterator()  #初始化迭代器
            train_batch_element = self.iterator.get_next() #从迭代器中取出数据，getnext输出tensor的顺序取决于map函数的输出
            return train_batch_element, data_num
替换成dataset之后，需要打印get_next的输出tensor，需要保证和input_producer的输出tensor的shape和type一致。
