参考链接：https://support.huaweicloud.com/tg-modelarts/modelarts_15_0001.html

由于AI开发者会使用PyCharm工具开发算法或模型，为方便快速将本地代码提交到公有云的训练环境，ModelArts提供了一个PyCharm插件工具PyCharm ToolKit，协助用户完成代码上传、提交训练作业、将训练日志获取到本地展示等，用户只需要专注于本地的代码开发即可。
大家可以参考上卖弄链接搞定即可；但是这里有几个问题跟大家分享一下，以免大家走了弯路。

问题一：由于配置中默认数据输入参数为data_url,而且会从配置的参数中读取，所以你的模型训练脚本中的参数如果不是从命令行读取需要做一下处理，保证在参数中加上该参数，并且要将该参数传递给你的真实的训练参数，否则，你的训练任务中会报找不到该参数。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0913/101209_a69b8060_5395865.png "屏幕截图.png")
例如：
我的resnet50训练脚本中，原来的输入输入是从一个配置文件中读取的，所以我做了如下处理：
1. 在训练脚本中的输入列表中加上一行，保证data_url能够读入：
    cmdline.add_argument('--data_url', default="./model_dir",
                         help="""config file used.""")
2. 在训练脚本中将该值传递到配置文件中：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0913/102019_1de6553d_5395865.png "屏幕截图.png")

问题二：训练过程中总是打印很多无关的信息，淹没了重要的打印信息，可以通过如下链接屏蔽：
https://support.huaweicloud.com/trouble-modelarts/modelarts_13_0019.html
例如：一直疯狂打印的，就可以通过本方法屏蔽：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0913/102215_05ed7a1a_5395865.png "屏幕截图.png")





