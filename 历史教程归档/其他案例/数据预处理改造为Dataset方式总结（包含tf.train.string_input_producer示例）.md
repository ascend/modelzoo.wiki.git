 **使用Dataset大体分为三个步骤：创建Dataset对象=>转换为正确的数据格式=>在网络计算过程中使用Dataset** 

### 1. 创建tf.data.Dataset
#### 简单点的
（1）使用数字作为数据
``` python
tf.data.Dataset.range(start, stop, step)
```

（2）获取文件名作为数据
``` python
tf.data.Dataset.list_files(file_pattern)
```

#### 通过读取文件获取数据
（3）数据集包含在一个或多个文本文件中
读取文本文件，一行字符串即为一组数据
``` python
tf.data.TextLineDataset(filenames)
```

（4）数据集包含在一个或多个TFRecord文件中
TFRecord文件是二进制格式，包含大小不同的二进制record序列，每个record即为一组数据
``` python
tf.data.TFRecordDataset(filenames)
```

（5）数据集包含在一个或多个自定义二进制文件（每组数据字节数相同）中
``` python
tf.data.FixedLengthRecordDataset(filenames, record_bytes)
```
record_bytes是tf.int64类型，表示每组数据占用字节数

#### 如果没有符合以上条件的文件，则需要将数据读入内存，再通过以下方式创建
（6）将传入的整个数据作为一组数据
``` python
tf.data.Dataset.from_tensors(tensors)
```

（7）将传入的迭代数据中的每个元素作为一组数据
``` python
tf.data.Dataset.from_tensor_slices(tensors)
```

（8）将生成器生成的一个结果作为一组数据（大型数据集使用该方法）
``` python
tf.data.Dataset.from_tensor_generator(generator, output_types, output_shapes=None, args=None)
```
generator为生成器，output_types为一组数据的类型列表，output_shapes为一组数据的形状列表，args为给生成器的参数列表

### 2. 转换数据格式，得到新的Dataset
#### Dataset的转换函数
（1）重复此数据集count次数
``` python
dataset.repeat(count=None)
```
参数count（可选）表示数据集应重复的次数，默认行为（如果count是None或-1）是无限期重复的数据集

（2）随机混洗数据集的元素
``` python
dataset.shuffle(buffer_size,seed=None,reshuffle_each_iteration=None)
```
参数buffer_size表示新数据集将从中采样的数据集中的元素数
参数seed（可选）表示将用于创建分布的随机种子
参数reshuffle_each_iteration（可选）一个布尔值，如果为true，则表示每次迭代时都应对数据集进行伪随机重组（默认为True）

（3）生成一个跳过count元素的数据集
``` python
dataset.skip(count)
```
参数count表示应跳过以形成新数据集的此数据集的元素数，如果count大于此数据集的大小，则新数据集将不包含任何元素，如果count 为-1，则跳过整个数据集

（4）提取前count个元素形成性数据集
``` python
dataset.take(count)
```
参数count表示应该用于形成新数据集的此数据集的元素数，如果count为-1，或者count大于此数据集的大小，则新数据集将包含此数据集的所有元素

（5）map可以将map_func函数映射到数据集
``` python
dataset.map(map_func，num_parallel_calls=None)
```
参数map_func是映射函数
参数num_parallel_calls表示要并行处理的数字元素，如果未指定，将按顺序处理元素

（6）flat_map可以将map_func函数映射到数据集，之后铺平为一维
``` python
dataset.flat_map(map_func)
```
参数map_func是映射函数

（7）filter可以对传入的dataset数据进行条件过滤
``` python
dataset.filter(predicate)
```
参数predicate是条件过滤函数

（8）batch可以将数据集的连续元素合成批次
``` python
dataset.batch(batch_size,drop_remainder=False)
```
参数batch_size表示要在单个批次中合并的此数据集的连续元素个数
参数drop_remainder表示在少于batch_size元素的情况下是否应删除最后一批，默认是不删除

（9）concatenate可以将两个Dataset对象进行合并或连接
``` python
dataset.concatenate(dataset)
```
参数dataset表示需要传入的dataset对象

（10）将给定数据集压缩在一起
``` python
dataset.zip(datasets)
```
参数datesets表示数据集的嵌套结构

#### 比较常用的转换函数
（1）转换csv格式文件
``` python
filepaths = ['data.csv']
# 跳过标题行
raw_dataset = tf.data.TextLineDataset(filepaths).skip(1)
# 解析csv内容行
def parse_line(line):
    CSV_COLUMN_NAMES = ['a', 'b', 'c']
    CSV_DEFAULT_VALUES = [1, 2, 3]
    fields = tf.decode_csv(line, record_defaults=CSV_DEFAULT_VALUES)
    feature_dict = dict(zip(CSV_COLUMN_NAMES, fields))
    return feature_dict
dataset = raw_dataset.map(parse_line)
```

（2）转换tf.train.Example组成的TFRecord格式文件
``` python
filepaths = ['data.tfrecord']
raw_dataset = tf.data.TFRecordDataset(filepaths)
# 定义Features结构，用于解析
feature_description = {
    'a': tf.FixedLenFeature((2, 2), tf.int64),
    'b': tf.FixedLenFeature((1, 2), tf.float32),
    'c': tf.FixedLenFeature((1,), tf.string)
}
# 解析tf.train.Example
def parse_example(example_string):
    feature_dict = tf.parse_single_example(example_string, feature_description)
    return feature_dict
dataset = raw_dataset.map(parse_example)
```

（3）转换raw格式图像文件
``` python
filepaths = ['image.raw']
IMAGE_WIDTH = 28
IMAGE_HEIGHT = 28
raw_dataset = tf.data.Dataset.list_files(filepaths)
# 读取图像内容并转换为浮点数矩阵
def parse_image_filepath(filepath):
    raw_image = tf.read_file(filepath)
    image = tf.decode_raw(raw_image, tf.float32)
    return tf.reshape(image, (IMAGE_WIDTH, IMAGE_HEIGHT))
dataset = raw_dataset.map(parse_image_filepath)
```

### 3. 在网络计算过程中使用Dataset
在网络计算过程中，需要提取出一个batch的数据，通常使用迭代器获取
（1）单次迭代器，自动初始化，不支持重新初始化
``` python
tf.data.make_one_shot_iterator(dataset)
```

（2）手动初始化迭代器，创建后或需要重新初始化时，使用sess.run(iter.initializer)进行初始化
``` python
tf.data.make_initializable_iterator(dataset)
```

获得迭代器iterator后，可以通过iterator.get_next()函数获取一个batch的数据
在tf.Session()中运行时，迭代器到达数据集结束处会产生一个异常：
``` python
with tf.Session() as sess:
    while True:
        try:
            sess.run(iterator.get_next())
        except tf.errors.OutOfRangeError as e:
            break
```
### 4. 管道队列tf.train.string_input_producer转换
``` python
filepaths = ['a.txt', 'b.txt']
epochs = 10

# 以下两种方式读取文件内容，value变量为文件内容
# 1.管道队列方式
# ----------------------------------------------------------------------
with tf.Session() as sess:
    file_queue = tf.train.string_input_producer(filepaths, shuffle=True, num_epochs=epochs)
    reader = tf.WholeFileReader()
    key, value = reader.read(file_queue)
    tf.local_variables_initializer().run()
    threads = tf.train.start_queue_runners(sess=sess)
    while True:
        try:            
            print(sess.run(value))
        except tf.errors.OutOfRangeError as e:
            break
# ----------------------------------------------------------------------

# 2.Dataset方式
# ----------------------------------------------------------------------
with tf.Session() as sess:
    raw_dataset = tf.data.Dataset.list_files(filepaths)
    dataset = raw_dataset.map(tf.read_file).shuffle(buffer_size=10000)
    iterator = tf.data.make_initializable_iterator(dataset)
    for i in range(epochs):
        iterator.initializer.run()
        while True:
            try:
                value = iterator.get_next()
                print(sess.run(value))
            except tf.errors.OutOfRangeError as e:
                break
# ----------------------------------------------------------------------
```