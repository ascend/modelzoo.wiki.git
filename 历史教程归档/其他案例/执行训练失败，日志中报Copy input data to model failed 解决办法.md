转自 [王涛](https://gitee.com/wqtshg) 分享

【问题描述】
host日志中有如下ERROR导致训练失败
[ERROR] GE(65218,python3.7):2021-01-22-16:46:38.700.734 [../../../../../../graphengine/ge/graph/load/model_manager/davinci_model.cc:2128]67016 CopyInputData: ErrorNo: 1343225857(Parameter's invalid!) input data size(30081056) does not match model required size(6009472), op_name(_arg_Placeholder_0_0_0_arg) ret failed.
[ERROR] GE(65218,python3.7):2021-01-22-16:46:38.700.754 [../../../../../../graphengine/ge/graph/load/model_manager/davinci_model.cc:2703]67016 Run: ErrorNo: -1(failed) Copy input data to model failed.

【问题分析】
这个ERROR日志，直接报错的含义是，已编译好的模型需要的输入数据大小为6009472，然后执行模型时，传下来的实际输入数据大小为30081056，校验不匹配从而报错并中断流程。
模型需要的输入数据大小，可以根据日志中的节点名称_arg_Placeholder_0_0_0_arg去dump图看查看。
这类问题出现的原因，大多是因为网络是一个输入就是动态shape的网络，对于这类网络，执行时需要在脚本的config中添加如下参数可解决问题。

【解决办法】
1. session run模式需要设置
custom_op.parameter_map["dynamic_input"].b = True
custom_op.parameter_map["dynamic_graph_execute_mode"].s = tf.compat.as_bytes("lazy_recompile")

2. estimator模式
在NPURunconfig里面加dynamic_input和dynamic_graph_execute_mode