1、  流程介绍

当前Ascend 910训练生成的ckpt文件，需要转换为pb文件才能给Ascend 310推理使用，那么到底该如何转换呢？下面以Alexnet网络为例来给出转换的方法。

2、  转换需要注意的几个问题

1）  输入节点

对训练而言，输入节点为IteratorV2，而推理需要的输入为placeholder。

2）  输出节点

对训练而言，输出节点为loss值，而推理需要的输出为Argmax或BiasAdd等。

3）  BatchNorm的行为

在训练时Batchnorm算子的平均值和方差在训练时由训练样本进行计算得到，但在推理时，该算子的平均值和方差由样本的滑动平均来计算，因此batchnorm在训练和推理时需要不同的平均值计算方式，大多数网络通过is_training属性控制。

4）网络中一些NPU上特有的dropout或集合通信算子需要引入npu_bridge包

from npu_bridge.estimator import npu_ops

from npu_bridge.estimator.npu.npu_config import NPURunConfig

from npu_bridge.estimator.npu.npu_estimator import NPUEstimator

from npu_bridge.estimator.npu.npu_optimizer import allreduce

from npu_bridge.estimator.npu.npu_optimizer import NPUDistributedOptimizer

from npu_bridge.hccl import hccl_ops

 

3、  转换方式

ckpt转pb主要通过Tensorflow原生的freeze_graph方法：



关键步骤说明：

1）  导入网络模型文件

2）  指定ckpt文件路径

3）  定义输入节点

4）  调用网络模型生成推理图

5）  定义输出节点

6）  调用freeze_graph生成pb文件

 
4、  转换示例

以alexnet网络为例。

1）    alexnet.py

网络模型文件，从训练网络脚本中提取出来，可以在如下链接中下载源码并找到：https://www.huaweicloud.com/ascend/resources/modelzoo/Model%20Scripts/d02e5703352b469495d04afa6df3da18

2）    ckpt2pb_alexnet.py

转换的实现逻辑，包含了freeze_graph方法，源码如下：

import tensorflow as tf
from tensorflow.python.tools import freeze_graph
from tensorflow.python.framework import graph_util
# 导入网络模型文件
import alexnet
from npu_bridge.estimator import npu_ops
# 指导ckpt路径,需要换成你自己的
ckpt_path = "./model.ckpt-20000"

def main(): 
    tf.reset_default_graph()
    # 定义输入节点
    inputs = tf.placeholder(tf.float32, shape=[None, 224, 224, 3], name="input")
    # 调用网络模型生成推理图
    logits = alexnet.inference(inputs, version="he_uniform",
                                  num_classes=1000, is_training=False)
    # 定义输出节点
    predict_class = tf.argmax(logits, axis=1, output_type=tf.int32, name="output")

    with tf.Session() as sess:
        tf.train.write_graph(sess.graph_def, './pb_model', 'model.pb')    # 默认，不需要修改
        freeze_graph.freeze_graph(
		        input_graph='./pb_model/model.pb',   # 默认，不需要修改
		        input_saver='',
		        input_binary=False, 
		        input_checkpoint=ckpt_path, 
		        output_node_names='output',  # 与上面定义的输出节点一致
		        restore_op_name='save/restore_all',
		        filename_tensor_name='save/Const:0',
		        output_graph='./pb_model/alexnet.pb',   # 改为对应网络的名称
		        clear_devices=False,
		        initializer_nodes='')
    print("done")

if __name__ == '__main__': 
    main()



