1、登录华为云后，进入控制台，选择 北京四。然后点击 对象存储服务OBS
![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/145937_b67901e9_9029767.png "obs1.png")
2、进入需要公开读的obs桶 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/145948_00b0844b_9029767.png "obs2.png")
3、找到访问 权限控制 - 桶策略。然后点击 创建
![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/145955_3c1b0e74_9029767.png "obs3.png")
4、在公共读写行，点击使用模板创建。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/150002_408b9f39_9029767.png "obs4.png")
5、最后点击右下角的 配置确认 ，然后进入第二个页面点击 创建
